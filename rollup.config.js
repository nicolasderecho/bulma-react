import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import external from 'rollup-plugin-peer-deps-external'
import postcss from 'rollup-plugin-postcss'
import resolve from 'rollup-plugin-node-resolve'
import url from 'rollup-plugin-url'
import path from 'path';
import pkg from './package.json'

export default {
    input: path.resolve(__dirname, 'src/lib/index.js'),
    output: [
        {
            file: pkg.main,
            format: 'cjs',
            sourcemap: true
        },
        {
            file: pkg.module,
            format: 'es',
            sourcemap: true
        }
    ],
    external: [
        'react',
        'prop-types',
        'classnames'
    ],
    plugins: [
        external(),
        postcss({ extract: './dist/css/tenpines-bulma-react.css' }),
        url(),
        babel({
            exclude: 'node_modules/**'
        }),
        resolve(),
        commonjs()
    ]
}