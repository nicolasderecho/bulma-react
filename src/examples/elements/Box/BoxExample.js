import React from "react";
import {Title, Box } from "../../../lib";
import CodeBlock from '../../shared/CodeBlock';
import {snippet1} from "./Snippets";

export default class BoxExample extends React.Component {

    render(){
        return <div>
            <Title as={'h3'}>A white box to contain other elements</Title>
            <CodeBlock>{snippet1}</CodeBlock>
            <Box>a simple Box</Box>
        </div>;
    }
}