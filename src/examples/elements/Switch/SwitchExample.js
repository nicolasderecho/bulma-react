import React from "react";
import {Title, Switch} from "../../../lib";
import Slider from "../../../lib/custom/Slider/Slider";

export default class SwitchExample extends React.Component {
    state = {checked: false};

    render(){
        return <div>
            <Title as={'h3'}>A switch</Title>
            <Switch rounded thin labelClass={'labelCustom'} color={'success'} size={'large'} checked={this.state.checked} text={'PEPE'} onClick={() => this.setState((prevState) => ({checked: !prevState.checked}))} />
            <Switch rounded labelClass={'labelCustom'} color={'success'} size={'large'} checked={this.state.checked} text={'pepe'} onClick={() => this.setState((prevState) => ({checked: !prevState.checked}))} />

            <Switch thin labelClass={'labelCustom'} color={'success'} size={'small'} checked={this.state.checked} text={'pepe'} onClick={() => this.setState((prevState) => ({checked: !prevState.checked}))} />
            <Slider color={'success'} />
        </div>;
    }
}