import React from "react";
import {Title, Field, Tooltip, Button} from "../../../lib";

export default class TooltipExample extends React.Component {
    state = { active: true };

    render(){
        return <div>
            <Title as={'h3'}>A Tooltip</Title>
            <Field>
                <Tooltip text={'Look me! a tooltip'} color={'danger'} positionMobile={'left'} active={this.state.active} position={'bottom'}><Button>Click</Button></Tooltip>
                <Button onClick={() => this.setState((prev) => ({active: !prev.active}))}>Toogle</Button>
            </Field>
        </div>;
    }
}