import React from "react";
import {Tag, Tags, Title} from "../../lib";
import SyntaxHighlighter from 'react-syntax-highlighter/dist/cjs';
import { dark } from 'react-syntax-highlighter/dist/cjs/styles/prism';

export default class TagExample extends React.Component {
  render(){
    return <div>
      <Title as={'h3'}>A tag</Title>
      <SyntaxHighlighter language={'javascript'} style={dark}>{'import React from "React"\n<Tag>Pepe<Tag>'}</SyntaxHighlighter>
      <Tag >Pepe</Tag>

      <Title as={'h3'}>Addons</Title>
      <SyntaxHighlighter language={'javascript'} style={dark}>{'<Tags addons>\n  <Tag >Documentation</Tag>\n  <Tag isDelete />\n</Tags>'}</SyntaxHighlighter>
      <Tags addons>
        <Tag >Documentation</Tag>
        <Tag isDelete />
      </Tags>

    </div>
  }
}