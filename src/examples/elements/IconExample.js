import React from "react";
import '@fortawesome/fontawesome-free/css/all.css';
import {IconWrapper} from "../../lib";

export default class IconExample extends React.Component {
    render(){
        return <div>ACA VIENE EL ICON
            <IconWrapper size={'large'} hasText={'dark'}>
                <i className="fas fa-home" />
            </IconWrapper>
        </div>;
    }
}