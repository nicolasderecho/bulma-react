import React from "react";
import {Column, Columns, Notification} from "../../lib";

export default class ColumnsExample extends React.Component {
  render(){
    return <div>
      <Columns>
        <Column mobile={false} tablet mobileColumnSize={'one-fifth'} desktopOffset={'1/5'} desktopColumnSize={'4'} ><Notification color={'primary'}>1</Notification></Column>
        <Column narrowDesktop ><Notification color={'primary'}>2</Notification></Column>
        <Column narrowDesktop ><Notification color={'primary'}>3</Notification></Column>
      </Columns>
      <Columns from={'mobile'} vcentered gapTablet={3} gapMobile={2} gapDesktop={4}>
        <Column>Column 1</Column>
        <Column>Column 2</Column>
        <Column>Column 3</Column>
        <Column>Column 4</Column>
      </Columns>
    </div>;
  }
}