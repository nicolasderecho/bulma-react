import React from "react";
import {Content, Title} from "../../lib";
import SyntaxHighlighter from 'react-syntax-highlighter/dist/cjs';
import {dark} from "react-syntax-highlighter/dist/cjs/styles/prism";

export default class ContentExample extends React.Component {
    render(){
        return <div>
            <Title as={'h4'}>A single class to handle WYSIWYG generated content, where only HTML tags are available</Title>
            <SyntaxHighlighter language={'javascript'} style={dark}>{'import React from "React"\n<Content>Some content<Content>'}</SyntaxHighlighter>
            <Content>Some content</Content>
            <br></br>
            <Title as={'h4'}>Sizes</Title>
            <SyntaxHighlighter language={'javascript'} style={dark}>{'import React from "React"\n<Content size={\'small\'}>\n  <p>Lorem Ipsum Dolor Sit Amet</p>\n<Content>'}</SyntaxHighlighter>
            <Content size={'small'}>
                <p>Lorem Ipsum Dolor Sit Amet</p>
            </Content>
        </div>
    }
}