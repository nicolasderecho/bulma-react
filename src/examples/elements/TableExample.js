import React from "react";
import {Table} from "../../lib";

export default class TableExample extends React.Component {
  render(){
    return <Table bordered hoverable>
      <Table.Head>
        <Table.Row>
          <Table.Cell>Table Head</Table.Cell>
        </Table.Row>
      </Table.Head>
      <Table.Body>
        <Table.Row>
          <Table.CellHeader>Number</Table.CellHeader>
          <Table.CellHeader>Name</Table.CellHeader>
          <Table.CellHeader>Description</Table.CellHeader>
        </Table.Row>
        <Table.Row>
          <Table.Cell>1</Table.Cell>
          <Table.Cell>Lalo Landa</Table.Cell>
          <Table.Cell color={'success'}>The Simpsons</Table.Cell>
        </Table.Row>
        <Table.Row selected>
          <Table.Cell>2</Table.Cell>
          <Table.Cell>Jake Peralta</Table.Cell>
          <Table.Cell>Broklyn 99</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>3</Table.Cell>
          <Table.Cell>Sheldon</Table.Cell>
          <Table.Cell>The Big Bang Theory</Table.Cell>
        </Table.Row>
      </Table.Body>
      <Table.Footer>
        <Table.Row>
          <Table.Cell>Table Footer</Table.Cell>
        </Table.Row>
      </Table.Footer>
    </Table>
  }
}