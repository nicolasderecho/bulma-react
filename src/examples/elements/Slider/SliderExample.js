import React from "react";
import {Title} from "../../../lib";
import Slider from "../../../lib/custom/Slider/Slider";

export default class SwitchExample extends React.Component {
    state = {number: 20};

    render(){
        return <div>
            <Title as={'h3'}>A Slider</Title>
            <div>{this.state.number}</div>
            <Slider color={'success'} min={0} max={100} step={0.5} value={this.state.number} onChange={(newValue) => this.setState({number: newValue })} />
        </div>;
    }
}