import React from "react";
import {Title, Subtitle} from "../../lib";

export default class TitleExample extends React.Component {
  render(){
    return <div>
      <Title sizeNumber={1} spaced as={'div'}>Title</Title>
      <Subtitle sizeNumber={3} as={'div'}>Subtitle</Subtitle>
    </div>;
  }
}