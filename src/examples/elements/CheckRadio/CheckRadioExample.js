import React from "react";
import {Title, Field, CheckBox, Radio} from "../../../lib";

export default class CheckRadioExample extends React.Component {

    render(){
        return <div>
            <Title as={'h3'}>A CheckRadio</Title>
            <Field>
                <Radio id={'blue'} text={'Azul'}  name={'color'} rtl color={'info'} />
                <Radio id={'red'}  text={'Rojo'}  name={'color'} color={'danger'} hasBackgroundColor />
            </Field>
            <Field>
                <CheckBox id={'terms'} indeterminate={true} text={'Acepto términos y condiciones'} name={'accept'} color={'success'} />
            </Field>
        </div>;
    }
}