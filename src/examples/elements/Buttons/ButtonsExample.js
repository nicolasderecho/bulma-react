import React from "react";
import {Box, ButtonGroup, Buttons, Button, Title, Columns, Column} from "../../../lib";
import CodeBlock from "../../shared/CodeBlock";
import {example1, example2, example3, example4, example5, example6, example7, example8} from "./Snippet";

export default class ButtonsExample extends React.Component {
    render(){
        return <React.Fragment>
            <Title as={'h3'}>The classic button, in different colors, sizes, and states</Title>
            <Box style={{marginTop: '20px'}}>
                <CodeBlock>{example1}</CodeBlock>
                <Button>a button</Button>
            </Box>
            <Box>
                <Title as={'h4'}>To add some margin among buttons you can use 'Buttons' component</Title>
                <CodeBlock>{example2}</CodeBlock>
                <Buttons>
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </Buttons>
            </Box>
            <Box>
                <Title as={'h4'}>You can select the button's color</Title>
                <CodeBlock>{example5}</CodeBlock>
                <Buttons>
                    <Button color={'primary'} >primary</Button>
                    <Button color={'link'} >link</Button>
                    <Button color={'info'} >info</Button>
                    <Button color={'success'} >success</Button>
                    <Button color={'warning'} >warning</Button>
                    <Button color={'danger'} >danger</Button>
                    <Button color={'black'} >black</Button>
                    <Button color={'dark'} >dark</Button>
                    <Button color={'light'} >light</Button>
                    <Button color={'white'} >white</Button>
                </Buttons>
            </Box>
            <Box>
                <Title as={'h4'}>When using field controls, if you want to group buttons together on a single line, use 'ButtonGroup' component</Title>
                <CodeBlock>{example3}</CodeBlock>
                <ButtonGroup>
                    <p className="control"><Button>First Option</Button></p>
                    <p className="control"><Button>Secon Option</Button></p>
                    <p className="control"><Button>Third Option</Button></p>
                </ButtonGroup>
            </Box>
            <Box>
                <Title as={'h4'} >If you want to use buttons as addons, the ButtonGroup 'addons' property</Title>
                <CodeBlock>{example4}</CodeBlock>
                <ButtonGroup>
                    <Buttons addons>
                        <Button>First Option</Button>
                        <Button>Secon Option</Button>
                        <Button>Third Option</Button>
                    </Buttons>
                </ButtonGroup>
            </Box>
            <Box>
                <Title as={'h4'}>The button comes in 4 different sizes</Title>
                <CodeBlock>{example6}</CodeBlock>
                <Buttons>
                    <Button size="small">small</Button>
                    <Button size="normal">normal (default)</Button>
                    <Button size="medium">medium</Button>
                    <Button size="large">large</Button>
                </Buttons>
            </Box>
            <Box>
                <Title as={'h4'}>You can change the size of multiple buttons at once by wrapping them in a Buttons parent</Title>
                <CodeBlock>{example7}</CodeBlock>
                <Buttons size={'medium'}>
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </Buttons>
            </Box>
            <Box>
                <Title as={'h4'}>You can render a custom html element if you prefer</Title>
                <CodeBlock>{example8}</CodeBlock>
                <Buttons>
                    <Button as={'link'} color={'link'}>{'I am a <a>'}</Button>
                    <Button as={'a'} color={'primary'}>{'I am a <a>'}</Button>
                    <Button as={'button'} color={'success'}>{'I am a <button>'}</Button>
                    <Button as={'input'} color={'danger'} value={'I am an <input type="submit" and I don\'t accept children'}/>
                </Buttons>
            </Box>
            <Box>
                <Columns>
                    <Column mobileNarrow tabletNarrow desktopNarrow mobileColumnSize={4}>pepe</Column>
                </Columns>
            </Box>
        </React.Fragment>
    }
}