export const example1 =
`import React from "react"
import {Button} from "10pines-bulma-react"
<Button>a button</Button>`;

export const example2 =
`import {Button, Buttons} from "10pines-bulma-react"
<Buttons>
    <Button>One</Button>
    <Button>Two</Button>
    <Button>Three</Button>
</Buttons>`;

export const example3 =
`import {Button, ButtonGroup} from "10pines-bulma-react"
<ButtonGroup>
    <p className="control"><Button>First Option</Button></p>
    <p className="control"><Button>Secon Option</Button></p>
    <p className="control"><Button>Third Option</Button></p>
</ButtonGroup>`;

export const example4 =
`import {Button, Buttons, ButtonGroup} from "10pines-bulma-react"
<ButtonGroup>
    <Buttons addons>
        <Button>First Option</Button>
        <Button>Secon Option</Button>
        <Button>Third Option</Button>
    </Buttons>
</ButtonGroup>`;

export const example5 =
`<Buttons>
    <Button color={'primary'} >primary</Button>
    <Button color={'link'} >link</Button>
    <Button color={'info'} >info</Button>
    <Button color={'success'} >success</Button>
    <Button color={'warning'} >warning</Button>
    <Button color={'danger'} >danger</Button>
    <Button color={'black'} >black</Button>
    <Button color={'dark'} >dark</Button>
    <Button color={'light'} >light</Button>
    <Button color={'white'} >white</Button>
</Buttons>`;

export const example6=
`<Buttons>
    <Button size="small">small</Button>
    <Button size="normal">normal (default)</Button>
    <Button size="medium">medium</Button>
    <Button size="large">large</Button>
</Buttons>`;

export const example7=
`<Buttons size={'medium'}>
    <Button>One</Button>
    <Button>Two</Button>
    <Button>Three</Button>
</Buttons>`;

export const example8=
`<Buttons>
    <Button as={'link'} color={'link'}>{'I am a <a>'}</Button>
    <Button as={'a'} color={'primary'}>{'I am a <a>'}</Button>
    <Button as={'button'} color={'success'}>{'I am a <button>'}</Button>
    <Button as={'input'} color={'danger'} value={'I am an <input type="submit" and I don\\'t accept children'}/>
</Buttons>`;