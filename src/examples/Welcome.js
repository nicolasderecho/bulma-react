import React from "react";
import Content from "../lib/elements/Content/Content";

export default class Welcome extends React.Component {
    render(){
        return <Content>Welcome!</Content>;
    }
}