import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom"
import DocMenu from "./DocMenu";
import Welcome from "./Welcome";
import BoxExample from "./elements/Box/BoxExample";
import ButtonsExample from "./elements/Buttons/ButtonsExample";
import ContentExample from "./elements/ContentExample";
import IconExample from "./elements/IconExample";
import TagExample from "./elements/TagExample";
import TableExample from "./elements/TableExample";
import TitleExample from "./elements/TitleExample";
import ColumnsExample from "./elements/ColumnsExample";
import SwitchExample from "./elements/Switch/SwitchExample";
import CheckRadioExample from "./elements/CheckRadio/CheckRadioExample";
import TooltipExample from "./elements/Tooltip/TooltipExample";
import {Content} from "../lib";
import './Home.scss';
import SliderExample from "./elements/Slider/SliderExample";

export default class Home extends React.Component {
    render() {
        return <Router>
            <div style={{display: 'flex'}}>
                <DocMenu/>
                <Content style={{flex:6, padding:'1.5em'}}>
                    <Route exact path="/" component={Welcome} />
                    <Route exact path="/box" component={BoxExample} />
                    <Route exact path="/buttons" component={ButtonsExample} />
                    <Route exact path="/content" component={ContentExample} />
                    <Route exact path="/icon" component={IconExample} />
                    <Route exact path="/tag" component={TagExample} />
                    <Route exact path="/table" component={TableExample} />
                    <Route exact path="/title" component={TitleExample} />
                    <Route exact path="/columns" component={ColumnsExample} />
                    <Route exact path="/switch" component={SwitchExample} />
                    <Route exact path="/checkradio" component={CheckRadioExample} />
                    <Route exact path="/tooltip" component={TooltipExample} />
                    <Route exact path="/slider" component={SliderExample} />
                </Content>
            </div>
        </Router>;
    }
}