import React from "react";
import {ListItem, Menu} from "../lib";
import {NavLink} from "react-router-dom";

const Link = (props) => <NavLink to={'/box'} activeClassName={'is-active'} {...props} />;

export default class DocMenu extends React.Component {
    render() {
        return <div style={{padding: '1.5em'}}>
            <Menu>
                <Menu.Label>Elements</Menu.Label>
                <Menu.List>
                    <ListItem><Link to={'/box'} >Box</Link></ListItem>
                    <ListItem><Link to={'/buttons'} >Buttons</Link></ListItem>
                    <ListItem><Link to={'/content'} >Content</Link></ListItem>
                    <ListItem><Link to={'/icon'} >Icon</Link></ListItem>
                    <ListItem><Link to={'/tag'} >Tag</Link></ListItem>
                    <ListItem><Link to={'/table'} >Table</Link></ListItem>
                    <ListItem><Link to={'/title'} >Title</Link></ListItem>
                    <ListItem><Link to={'/columns'} >Columns</Link></ListItem>
                    <ListItem><Link to={'/switch'} >Switch</Link></ListItem>
                    <ListItem><Link to={'/checkradio'} >CheckRadio</Link></ListItem>
                    <ListItem><Link to={'/tooltip'} >Tooltip</Link></ListItem>
                    <ListItem><Link to={'/slider'} >Slider</Link></ListItem>
                </Menu.List>
            </Menu>
        </div>


    }
}