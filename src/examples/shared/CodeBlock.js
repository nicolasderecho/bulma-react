import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter/dist/cjs';

const REMOVE_SPACES_REGEXP = /^ +| +$/gm;

const removeSpacesFrom = (string) => string.replace(REMOVE_SPACES_REGEXP, "");

const CodeBlock = ({children, language = 'javascript', removeSpaces = false}) => {
  const code = removeSpaces ? removeSpacesFrom(children) : children;
  return <SyntaxHighlighter language={language}>
    {code.toString().trim()}
  </SyntaxHighlighter>
};

export default CodeBlock;