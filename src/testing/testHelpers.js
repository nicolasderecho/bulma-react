import React from 'react';
import {shallow} from 'enzyme';
import {camelCase, dashCase} from "../lib/helpers/util";

export const expectToRenderComponentWithSize = (Component, size, prefix) => {
    const selector = prefix ? `${prefix}.is-${size}` : `.is-${size}`;
    const element = shallow(<Component size={size}/>);
    expect(element.find(selector).exists()).toBeTruthy();
};

export const expectToRenderComponentWithPluralSize = (Component, size, prefix) => {
    const selector = prefix ? `${prefix}.are-${size}` : `.are-${size}`;
    const element = shallow(<Component size={size}/>);
    expect(element.find(selector).exists()).toBeTruthy();
};

export const expectToRenderComponentWithTextClass = (Component, textClass, prefix) => {
    const selector = prefix ? `${prefix}.has-text-${textClass}` : `.has-text-${textClass}`;
    const element = shallow(<Component hasText={textClass}/>);
    expect(element.find(selector).exists()).toBeTruthy();
};

export const expectToRenderComponentWithColor = (Element, color) => {
    const element = shallow(<Element color={color} />);
    expect(element.find(`.is-${color}`).exists()).toBeTruthy();
};

export const expectToRenderComponentWithProperty = (Component, property, { prefix = 'is', propertyValue = ''} = { prefix: 'is', propertyValue: '' }) => {
    const selectorValue = propertyValue || dashCase(property);
    const selector = !!prefix ? `${prefix}-${selectorValue}` : `${selectorValue}`;
    const matchedProps = { [camelCase(property)]: undefined};
    const notMatchedProps = { [camelCase(property)]: false};

    const matchedElement = shallow(<Component {...matchedProps} />);
    const notMatchedElement = shallow(<Component { ...notMatchedProps }/>);

    expect(matchedElement.props()).not.toHaveProperty(camelCase(property));
    expect(matchedElement.find(`.${selector}`).exists()).toBeTruthy();
    expect(notMatchedElement.find(`.${selector}`).exists()).toBeFalsy();
};

export const expectToRenderComponentWithHierarchy = (Element, hierarchy) => {
    const element = shallow(<Element hierarchy={hierarchy} />);
    expect(element.find(`.is-${hierarchy}`).exists()).toBeTruthy();
};

export const expectToRenderComponentWithSizeNumber = (Element, horizontalSize) => {
    const element = shallow(<Element horizontalSize={horizontalSize} />);
    expect(element.find(`.is-${horizontalSize}`).exists()).toBeTruthy();
};

export const expectToRenderComponentWithElement = (Component, expectedElement, expectedClass) => {
    const component = shallow(<Component/>);
    const selector = !!expectedClass ? `${expectedElement}.${expectedClass}` : expectedElement;
    expect(component.find(selector).exists()).toBeTruthy();
};

export const expectToRenderComponentWithChildren = (Component) => {
    const child = <span>Child Element</span>;
    const component = shallow(<Component>{child}</Component>);
    expect(component.contains(child)).toBeTruthy();
};

export const expectToRenderComponentWithRequestedClass = (Component, selector, className = 'example') => {
    const component = shallow(<Component className={className}/>);
    expect(component.find(`${selector}.${className}`).exists()).toBeTruthy();
};

export const expectToRenderComponentWithState = (Component, state, prefix) => {
    const selector = prefix ? `${prefix}.is-${state}` : `.is-${state}`;
    const element = shallow(<Component state={state}/>);
    expect(element.find(selector).exists()).toBeTruthy();
};

export const expectToRenderComponentWithAlignment = (Component, alignment, { prefix = 'is'} = { prefix: 'is' }) => {
    const selector = `.${prefix}-${alignment}`;
    const element = shallow(<Component alignment={alignment}/>);
    expect(element.props()).not.toHaveProperty('alignment');
    expect(element.find(selector).exists()).toBeTruthy();
};

export const expectToRenderComponentWithSeparator = (Component, separator, { prefix = 'has', suffix = 'separator'} = { prefix: 'has', suffix: 'separator' }) => {
    const selector = `.${prefix}-${separator}-${suffix}`;
    const element = shallow(<Component separator={separator}/>);
    expect(element.props()).not.toHaveProperty('separator');
    expect(element.find(selector).exists()).toBeTruthy();
};