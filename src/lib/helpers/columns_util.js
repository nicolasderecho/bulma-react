import {capitalize} from "./generics";

export const ONE_THIRD_ALIAS = '1/3';
export const TWO_THIRDS_ALIAS = '2/3';
export const ONE_QUARTER_ALIAS = '1/4';
export const HALF_ALIAS = '1/2';
export const THREE_QUARTERS_ALIAS = '3/4';
export const ONE_FIFTH_ALIAS = '1/5';
export const TWO_FIFTHS_ALIAS = '2/5';
export const THREE_FIFTHS_ALIAS = '3/5';
export const FOURT_FIFTHS_ALIAS = '4/5';

export const THREE_QUARTERS = 'three-quarters';
export const TWO_THIRDS = 'two-thirds';
export const HALF = 'half';
export const ONE_THIRD = 'one-third';
export const ONE_QUARTER = 'one-quarter';
export const FULL = 'full';
export const FOURT_FIFTHS = 'four-fifths';
export const THREE_FIFTHS = 'three-fifths';
export const TWO_FIFTHS = 'two-fifths';
export const ONE_FIFTH = 'one-fifth';

export const COLUMN_SIZE_NUMBERS = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
export const ORIGINAL_COLUMN_SIZES = COLUMN_SIZE_NUMBERS.concat([THREE_QUARTERS, TWO_THIRDS, HALF, ONE_THIRD, ONE_QUARTER, FULL, FOURT_FIFTHS, THREE_FIFTHS, TWO_FIFTHS, ONE_FIFTH]);
export const COLUMN_SIZES_ALIASES = [ONE_THIRD_ALIAS, TWO_THIRDS_ALIAS, ONE_QUARTER_ALIAS, HALF_ALIAS, THREE_QUARTERS_ALIAS, ONE_FIFTH_ALIAS, TWO_FIFTHS_ALIAS, THREE_FIFTHS_ALIAS, FOURT_FIFTHS_ALIAS];
export const COLUMN_SIZES = ORIGINAL_COLUMN_SIZES.concat(COLUMN_SIZES_ALIASES);
export const COLUMN_SIZES_ALIASES_HASH = {
  [ONE_THIRD_ALIAS]: ONE_THIRD,
  [TWO_THIRDS_ALIAS]: TWO_THIRDS,
  [ONE_QUARTER_ALIAS]: ONE_QUARTER,
  [HALF_ALIAS]: HALF,
  [THREE_QUARTERS_ALIAS]: THREE_QUARTERS,
  [TWO_FIFTHS_ALIAS]: TWO_FIFTHS,
  [THREE_FIFTHS_ALIAS]: THREE_FIFTHS,
  [FOURT_FIFTHS_ALIAS]: FOURT_FIFTHS,
  [ONE_FIFTH_ALIAS]: ONE_FIFTH
};
export const ORIGINAL_COLUMN_SIZES_HASH = ORIGINAL_COLUMN_SIZES.reduce((hash, columnSize) => Object.assign({}, hash, { [columnSize]: columnSize }), {});
export const COLUMN_SIZES_HASH = Object.assign({}, ORIGINAL_COLUMN_SIZES_HASH, COLUMN_SIZES_ALIASES_HASH);

export const MOBILE     = 'mobile';
export const TABLET     = 'tablet';
export const TOUCH      = 'touch';
export const DESKTOP    = 'desktop';
export const WIDESCREEN = 'widescreen';
export const FULLHD     = 'fullhd';
export const DEVICES    = [MOBILE, TABLET, TOUCH, DESKTOP, WIDESCREEN, FULLHD];

export const columnDimensionClassFor = (columnSize, prefix, suffix = '') => {
  const key = `${prefix}-${COLUMN_SIZES_HASH[columnSize]}`;
  const finalKey = !!suffix ? `${key}-${suffix}` : key;
  return { [finalKey]: !!COLUMN_SIZES_HASH[columnSize] };
};
export const columnSizeClassFor = (columnSize, suffix = '') => columnDimensionClassFor(columnSize, 'is', suffix);
export const columnOffsetlassFor = (columnSize, suffix = '') => columnDimensionClassFor(columnSize, 'is-offset', suffix);
export const isDevice = (possibleDevice) => DEVICES.indexOf(possibleDevice) !== -1;
export const deviceActiveClassFor = (fromDevice) => ({ [`is-${fromDevice}`]: isDevice(fromDevice) });
export const GAP_SIZES = [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ];
export const isValidGap = (possibleGap) => GAP_SIZES.indexOf(Number(possibleGap)) !== -1;
export const gapSizeFor = (gap) => ({ [`is-${gap}`]: isValidGap(gap)});
export const isVariableGap = (gap, props = {}) => {
  const deviceGapValidations = DEVICES.map((device) => isValidGap(props[`gap${capitalize(device)}`]) ).reduce((acum, value) => acum || value);
  return { [`is-variable`]: isValidGap(gap) || deviceGapValidations };
};
export const gapSizeForDevices = (props) => {
  return DEVICES.map((device) => {
    const gap = props[`gap${capitalize(device)}`];
    return { [`is-${gap}-${device}`]: isValidGap(gap) };
  });
};
export const combineKeyWithValues = (key, list) => list.map( (listElement) => `${key}${capitalize(listElement)}`);
export const combineValuesWithKey = (list, key) => list.map( (listElement) => `${listElement}${capitalize(key)}`);
export const isColumnSizeNumber = (sizeNumber, sizeNumbers = COLUMN_SIZE_NUMBERS) => !!sizeNumber && sizeNumbers.indexOf(sizeNumber.toString()) !== -1;
export const horizontalClassFor = (sizeNumber, sizeNumbers) => ({ [`is-${sizeNumber}`]: isColumnSizeNumber(sizeNumber, sizeNumbers) });