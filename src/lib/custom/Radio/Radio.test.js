import React from 'react';
import Radio from './Radio';
import { mount } from "enzyme/build";

it('renders a radio', () => {
    const id = 'blue-color';
    const radio = mount(<Radio id={id} text={'Blue'} name={'color'} />);
    expect(radio.find(`input#${id}.is-checkradio`).props()).toHaveProperty('type', 'radio');
});