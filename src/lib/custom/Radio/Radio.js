import React from 'react';
import PropTypes from 'prop-types';
import CheckRadio from "../CheckRadio/CheckRadio";
import { COLOR_TYPES, SIZES } from "../../helpers/util";

const Radio = (props) => {
    return <CheckRadio type={'radio'} {...props} />;
};

Radio.displayName = 'Radio';

Radio.propTypes = {
    inputClass: PropTypes.string,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES),
    size: PropTypes.oneOf(SIZES)
};

export default Radio;