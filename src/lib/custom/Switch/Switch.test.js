import React from 'react';
import Switch from './Switch';
import {
  expectToRenderComponentWithColor,
  expectToRenderComponentWithElement,
  expectToRenderComponentWithProperty,
  expectToRenderComponentWithSize
} from "../../../testing/testHelpers";
import { mount } from "enzyme/build";
import { COLOR_TYPES, SIZES } from "../../helpers/util";

it('renders a checkbox input with class switch', () => {
  expectToRenderComponentWithElement(Switch, 'input', 'switch');
});

it('renders a label with the switch message', () => {
  const component = mount(<Switch text={'Lights'}/>);
  const label = component.find('.field label');
  expect(label.exists()).toBeTruthy();
  expect(label.text()).toBe('Lights');
});

it('wraps the component on a field div by default', () => {
  const component = mount(<Switch />);
  expect(component.find('.field input.switch').exists()).toBeTruthy();
});

it('adds the rounded class when requested', () => {
  expectToRenderComponentWithProperty(Switch, 'rounded');
});

it('adds the rtl class when requested', () => {
  expectToRenderComponentWithProperty(Switch, 'rtl');
});

it('adds the outlined class when requested', () => {
  expectToRenderComponentWithProperty(Switch, 'outlined');
});

it('adds the thin class when requested', () => {
  expectToRenderComponentWithProperty(Switch, 'thin');
});

it('adds the requested size by props', () => {
  SIZES.forEach((size) => expectToRenderComponentWithSize(Switch, size) );
});

it('renders the requested color by props', () => {
  COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Switch, color) );
});

it('accepts the checked property', () => {
  const checkedComponent = mount(<Switch checked={true} />);
  const uncheckedComponent = mount(<Switch checked={false} />);
  expect(checkedComponent.find('input').getDOMNode().checked).toBeTruthy();
  expect(uncheckedComponent.find('input').getDOMNode().checked).toBeFalsy();
});

it('calls the onClick callback when received', () => {
  let clicked = false;
  const onClick = () => clicked = true;
  const component = mount(<Switch onClick={onClick} />);
  component.find('label').simulate('click');
  expect(clicked).toEqual(true);
});

it('adds a class to the label when requested', () => {
  const component = mount(<Switch labelClass={'custom-label'} />);
  expect(component.find('label.custom-label').exists()).toBeTruthy();
});

it('adds a class to the wrapper when requested', () => {
  const component = mount(<Switch wrapperClass={'custom-wrapper'} />);
  expect(component.find('div.field.custom-wrapper').exists()).toBeTruthy();
});

it('renders a disabled component when requested', () => {
  const disabledComponent = mount(<Switch disabled />);
  const enabledComponent = mount(<Switch disabled={false} />);
  expect(disabledComponent.find('input').getDOMNode().disabled).toBeTruthy();
  expect(enabledComponent.find('input').getDOMNode().disabled).toBeFalsy();
});