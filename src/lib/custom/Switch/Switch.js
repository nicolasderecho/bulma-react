import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    COLOR_TYPES,
    SIZES,
    colorClassFor,
    sizeClassFor,
    isEnabled,
    roundedClassFor,
    checkProperties
} from "../../helpers/util";

const Switch = ({ labelClass, wrapperClass, className, text, color, size, onClick = Function.prototype, ...props }) => {
  const classes = classNames(className, 'switch', colorClassFor(color), sizeClassFor(size), roundedClassFor(props), checkProperties(props, ['rtl', 'thin', 'outlined']));
  const isChecked = isEnabled(props, 'checked');
  const isDisabled = isEnabled(props, 'disabled');
  const wrapperClasses = classNames(wrapperClass, 'field');
  const labelClasses   = classNames(labelClass);
  const onClickSwitch  = isDisabled ? Function.prototype : onClick;
  return  <div className={wrapperClasses}>
            <input className={classes} type={'checkbox'} checked={isChecked} disabled={isDisabled} readOnly />
      <label className={labelClasses} onClick={onClickSwitch}><div className={'switch-text'}>{text}</div></label>
          </div>;
};

Switch.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  size: PropTypes.oneOf(SIZES),
  color: PropTypes.oneOf(COLOR_TYPES),
  onClick: PropTypes.func,
};

export default Switch;