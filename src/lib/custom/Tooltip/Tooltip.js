import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    checkProperties,
    positionClassFor,
    propsWithoutKeys,
    POSITIONS,
    COLOR_TYPES,
    isColorType
} from "../../helpers/util";

const Tooltip = ({className, text, color, position, positionMobile, positionTablet, positionTouch, positionDesktop, positionWidescreen, positionFullhd, positionMobileOnly, positionTabletOnly, positionTouchOnly, positionDesktopOnly, positionWidescreenOnly, positionFullhdOnly, children, ...props}) => {
    const classes = classNames(className, 'tooltip', positionClassFor(position), positionClassFor(positionMobile, {device: 'mobile'}), positionClassFor(positionTablet, {device: 'tablet'}), positionClassFor(positionTouch, {device: 'touch'}), positionClassFor(positionDesktop, {device: 'desktop'}), positionClassFor(positionWidescreen, {device: 'widescreen'}), positionClassFor(positionFullhd, {device: 'fullhd'}), positionClassFor(positionMobileOnly, {device: 'mobile', suffix: 'only'}), positionClassFor(positionTabletOnly, {device: 'tablet', suffix: 'only'}), positionClassFor(positionTouchOnly, {device: 'touch', suffix: 'only'}), positionClassFor(positionDesktopOnly, {device: 'desktop', suffix: 'only'}), positionClassFor(positionWidescreenOnly, {device: 'widescreen', suffix: 'only'}), positionClassFor(positionFullhdOnly, {device: 'fullhd', suffix: 'only'}) , checkProperties(props, ['active', 'multiline'], {prefix: 'is-tooltip'}), {[`is-tooltip-${color}`]: isColorType(color)});
    const finalProps = propsWithoutKeys(props, ['active', 'multiline']);
    return <div className={classes} {...finalProps} data-tooltip={text}>
        {children}
    </div>;
};

Tooltip.displayName = 'Tooltip';

Tooltip.propTypes = {
    className: PropTypes.string,
    text: PropTypes.string.isRequired,
    position: PropTypes.oneOf(POSITIONS),
    positionMobile: PropTypes.oneOf(POSITIONS),
    positionTablet: PropTypes.oneOf(POSITIONS),
    positionTouch: PropTypes.oneOf(POSITIONS),
    positionDesktop: PropTypes.oneOf(POSITIONS),
    positionWidescreen: PropTypes.oneOf(POSITIONS),
    positionFullhd: PropTypes.oneOf(POSITIONS),
    color: PropTypes.oneOf(COLOR_TYPES)
};

export default Tooltip;