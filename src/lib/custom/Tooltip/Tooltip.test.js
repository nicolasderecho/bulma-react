import React from 'react';
import Tooltip from './Tooltip';
import { mount } from "enzyme/build";
import {POSITIONS, capitalize, COLOR_TYPES} from "../../helpers/util";
import {DEVICES} from "../../helpers/columns_util";

it('renders a button with tooltip', () => {
    const element = mount(<Tooltip text={'some hint info'} />);
    expect(element.find('.tooltip').exists()).toBeTruthy();
});

it('assigns the received text as the tooltip text', () => {
    const text = 'some hint info';
    const element = mount(<Tooltip text={text} />);
    expect(element.find('.tooltip').prop('data-tooltip')).toBe(text);
});

it('adds the tooltip in the requested position', () => {
   const text = 'some text';
   POSITIONS.forEach((position) => {
       const element = mount(<Tooltip text={text} position={position}/>);
       expect(element.find(`.tooltip.is-tooltip-${position}`).exists()).toBeTruthy();
   });
});

it('adds the tooltip in the requested position for the specified device', () => {
    const text = 'some text';
    DEVICES.forEach(device => {
        POSITIONS.forEach(position => {
            const props = {text: text, [`position${capitalize(device)}`]: position };
            const element = mount(<Tooltip {...props} />);
            expect(element.find(`.tooltip.is-tooltip-${position}-${device}`).exists()).toBeTruthy();
        });
    });
});

it('adds the tooltip in the requested position only for the specified device', () => {
    const text = 'some text';
    DEVICES.forEach(device => {
        POSITIONS.forEach(position => {
            const props = {text: text, [`position${capitalize(device)}Only`]: position };
            const element = mount(<Tooltip {...props} />);
            expect(element.find(`.tooltip.is-tooltip-${position}-${device}-only`).exists()).toBeTruthy();
        });
    });
});

it('adds an active tooltip when requested', () => {
    const text = 'some text';
    const element = mount(<Tooltip text={text} active />);
    const secondElement = mount(<Tooltip text={text} active={false} />);
    expect(element.find('.tooltip.is-tooltip-active').exists()).toBeTruthy();
    expect(secondElement.find('.tooltip.is-tooltip-active').exists()).toBeFalsy();
});

it('adds an active tooltip when requested', () => {
    const text = 'some text';
    const element = mount(<Tooltip text={text} multiline />);
    const secondElement = mount(<Tooltip text={text} multiline={false} />);
    expect(element.find('.tooltip.is-tooltip-multiline').exists()).toBeTruthy();
    expect(secondElement.find('.tooltip.is-tooltip-multiline').exists()).toBeFalsy();
});

it('adds the requested color when requested', () => {
   COLOR_TYPES.forEach(color => {
       const text = 'some text';
       const element = mount(<Tooltip text={text} color={color} />);
       expect(element.find(`.tooltip.is-tooltip-${color}`).exists()).toBeTruthy();
   });
});

it('renders the children component', () => {
    const text = 'some text';
    const element = mount(<Tooltip text={text}><span className={'children'}>Tooltip trigger</span></Tooltip>);
    expect(element.find('.tooltip span.children').exists()).toBeTruthy();
});