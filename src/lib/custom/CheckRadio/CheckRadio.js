import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { checkProperties, colorClassFor, sizeClassFor, COLOR_TYPES, SIZES } from "../../helpers/util";

const ALLOWED_TYPES = ['radio', 'checkbox'];

const CheckRadio = (props) => {
    const { inputClass, text, id, name, type, color, size } = props;
    const inputRef = props.inputRef || React.createRef();
    const classes = classNames(inputClass, 'is-checkradio', colorClassFor(color), sizeClassFor(size), checkProperties(props, ['rtl', 'circle', 'block']), checkProperties(props, ['hasNoBorder', 'hasBackgroundColor'], { prefix: ''} ));
    return <React.Fragment>
        <input ref={inputRef} id={id} type={type} className={classes} name={name} />
        <label htmlFor={id}>{text}</label>
    </React.Fragment>;
};

CheckRadio.displayName = 'CheckRadio';

CheckRadio.propTypes = {
    inputClass: PropTypes.string,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    type: PropTypes.oneOf(ALLOWED_TYPES).isRequired,
    color: PropTypes.oneOf(COLOR_TYPES),
    size: PropTypes.oneOf(SIZES)
};

export default CheckRadio;