import React from 'react';
import CheckRadio from './CheckRadio';
import {
    expectToRenderComponentWithColor,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty,
    expectToRenderComponentWithSize
} from "../../../testing/testHelpers";
import {mount, shallow} from "enzyme/build";
import {camelCase, COLOR_TYPES, SIZES} from "../../helpers/util";

it('renders an input with its label', () => {
    const component = mount(<CheckRadio id={'someId'} text={'optionText'} type={'radio'}/>);
    expect(component.find('input.is-checkradio').exists()).toBeTruthy();
    expect(component.text()).toBe('optionText');
});

it('associates the label and the input by the id prop', () => {
    const id = 'blue-color';
    const component = mount(<CheckRadio id={id} text={'Blue'} type={'radio'}/>);
    expect(component.find(`input#${id}.is-checkradio`).exists()).toBeTruthy();
    expect(component.find('label').props()).toHaveProperty('htmlFor', id);
});

it('accepts a name prop', () => {
    const id = 'blue-color';
    const component = mount(<CheckRadio id={id} text={'Blue'} name={'color'} type={'radio'}/>);
    expect(component.find(`input#${id}.is-checkradio`).props()).toHaveProperty('name', 'color');
});

it('renders the required input type', () => {
    const id = 'blue-color';
    const radio = mount(<CheckRadio id={id} text={'Blue'} name={'color'} type={'radio'}/>);
    const secondId = 'terms';
    const checkbox = mount(<CheckRadio id={secondId} text={'Accept terms and conditions'} name={'acceptTerms'} type={'checkbox'} />);
    expect(radio.find(`input#${id}.is-checkradio`).props()).toHaveProperty('type', 'radio');
    expect(checkbox.find(`input#${secondId}.is-checkradio`).props()).toHaveProperty('type', 'checkbox');
});

it('adds the rtl class when requested', () => {
    const rtlComponent = mount(<CheckRadio id={'pepe'} type={'radio'} rtl />);
    const notRtlComponent = mount(<CheckRadio id={'pepo'} type={'radio'} rtl={false} />);
    expect(rtlComponent.find('input.is-rtl').exists()).toBeTruthy();
    expect(notRtlComponent.find('input.is-rtl').exists()).toBeFalsy();
});

it('adds the circle class when requested', () => {
    const circleComponent = mount(<CheckRadio id={'pepe'} type={'checkbox'} circle />);
    const notCircleComponent = mount(<CheckRadio id={'pepo'} type={'checkbox'} circle={false} />);
    expect(circleComponent.find('input.is-circle').exists()).toBeTruthy();
    expect(notCircleComponent.find('input.is-circle').exists()).toBeFalsy();
});

it('adds the block class when requested', () => {
    const circleComponent = mount(<CheckRadio id={'pepe'} type={'checkbox'} block />);
    const notCircleComponent = mount(<CheckRadio id={'pepo'} type={'checkbox'} block={false} />);
    expect(circleComponent.find('input.is-block').exists()).toBeTruthy();
    expect(notCircleComponent.find('input.is-block').exists()).toBeFalsy();
});

it('adds the has no border class when requested', () => {
    const circleComponent = mount(<CheckRadio id={'pepe'} type={'checkbox'} hasNoBorder />);
    const notCircleComponent = mount(<CheckRadio id={'pepo'} type={'checkbox'} hasNoBorder={false} />);
    expect(circleComponent.find('input.has-no-border').exists()).toBeTruthy();
    expect(notCircleComponent.find('input.has-no-border').exists()).toBeFalsy();
});

it('adds the has background color class when requested', () => {
    const element = mount(<CheckRadio id={'pepe'} type={'checkbox'} hasBackgroundColor />);
    const secondElement = mount(<CheckRadio id={'pepo'} type={'checkbox'} hasBackgroundColor={false} />);
    expect(element.find('input.has-background-color').exists()).toBeTruthy();
    expect(secondElement.find('input.has-background-color').exists()).toBeFalsy();
});

it('adds the requested color', () => {
    COLOR_TYPES.forEach((color) => {
        const element = mount(<CheckRadio id={'pepe'} type={'radio'} color={color} />);
        expect(element.find(`.is-${color}`).exists()).toBeTruthy();
    });
});

it('adds the requested size', () => {
    SIZES.forEach((size) => {
        const element = mount(<CheckRadio id={'pepe'} type={'radio'} size={size} />);
        expect(element.find(`.is-${size}`).exists()).toBeTruthy();
    });
});