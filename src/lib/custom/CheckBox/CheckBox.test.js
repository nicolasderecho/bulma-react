import React from 'react';
import CheckBox from './CheckBox';
import { mount } from "enzyme/build";

it('renders a checkbox', () => {
    const id = 'terms';
    const checkbox = mount(<CheckBox id={id} text={'Accept terms and conditions'} name={'acceptTerms'} />);
    expect(checkbox.find(`input#${id}.is-checkradio`).props()).toHaveProperty('type', 'checkbox');
});

it('adds the indeterminate class when requested', () => {
    const element = mount(<CheckBox id={'pepe'} indeterminate />);
    const secondElement = mount(<CheckBox id={'pepo'} indeterminate={false} />);
    expect(element.find('input').getDOMNode().indeterminate).toBeTruthy();
    expect(secondElement.find('input').getDOMNode().indeterminate).toBeFalsy();
});