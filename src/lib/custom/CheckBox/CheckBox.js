import React from 'react';
import PropTypes from 'prop-types';
import { COLOR_TYPES, SIZES, isEnabled } from "../../helpers/util";
import CheckRadio from "../CheckRadio/CheckRadio";

class CheckBox extends React.Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef();
    }

    setIndeterminateValue() {
        const input = this.inputRef.current;
        input.indeterminate = isEnabled(this.props, 'indeterminate');
    }

    componentDidMount() {
        this.setIndeterminateValue();
    }

    componentDidUpdate() {
        this.setIndeterminateValue();
    }

    render(){
        return <CheckRadio type={'checkbox'} inputRef={this.inputRef} {...this.props} />;
    }

}

CheckBox.displayName = 'CheckBox';

CheckBox.propTypes = {
    inputClass: PropTypes.string,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES),
    size: PropTypes.oneOf(SIZES)
};

export default CheckBox;