import React from 'react';
import Slider from './Slider';
import { mount } from "enzyme/build";
import {COLOR_TYPES, ORIENTATIONS, SIZES} from "../../helpers/util";

it('renders a slider input', () => {
    const slider = mount(<Slider />);
    expect(slider.find('input.slider').exists()).toBeTruthy();
});

it('renders the input in the requested orientation', () => {
    ORIENTATIONS.forEach(orientation => {
        const slider = mount(<Slider orientation={orientation} />);
        expect(slider.find('input.slider').getDOMNode().orient).toBe(orientation);
    });
});

it('renders a disabled input when requested', () => {
    const slider = mount(<Slider disabled />);
    expect(slider.find('input.slider').getDOMNode().disabled).toBeTruthy();
});

it('renders a full width slider when requested', () => {
    const slider = mount(<Slider fullwidth />);
    expect(slider.find('input.slider.is-fullwidth').exists()).toBeTruthy();
});

it('renders a circle slider when requested', () => {
    const slider = mount(<Slider circle />);
    expect(slider.find('input.slider.is-circle').exists()).toBeTruthy();
});

it('renders the slider in the requested color', () => {
   COLOR_TYPES.forEach(color => {
       const slider = mount(<Slider color={color} />);
       expect(slider.find(`input.slider.is-${color}`).exists()).toBeTruthy();
   });
});

it('renders the slider in the requested size', () => {
    SIZES.forEach(size => {
        const slider = mount(<Slider size={size} />);
        expect(slider.find(`input.slider.is-${size}`).exists()).toBeTruthy();
    });
});

it('calls the onChange prop when the slider value changes', () => {
    let changed = false;
    const onChange = () => changed = true;
    const component = mount(<Slider onChange={onChange} />);
    component.find('input').simulate('change');
    expect(changed).toEqual(true);
});