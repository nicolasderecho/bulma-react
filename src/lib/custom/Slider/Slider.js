import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    COLOR_TYPES,
    ORIENTATIONS,
    SIZES,
    isEnabled,
    checkProperty,
    colorClassFor,
    sizeClassFor
} from "../../helpers/util";

class Slider extends React.Component {
    constructor(props) {
        super(props);
        this.inputRef = this.props.inputRef || React.createRef();
        this.onSliderChange = this.onSliderChange.bind(this);
    }

    componentDidMount() {
        const input = this.inputRef.current;
        input.orient = this.props.orientation;
    }

    onSliderChange(event) {
        const target = event.target;
        const newValue = Number(target.value);
        const onChange = this.props.onChange || Function.prototype;
        onChange(newValue, event);
    }

    render() {
        const {className, step, min, max, value, color, size, onChange, ...props } = this.props;
        const disabled = isEnabled(props, 'disabled');
        const classes = classNames(className, 'slider', checkProperty(props, 'fullwidth'), checkProperty(props, 'circle'), colorClassFor(color), sizeClassFor(size));
        return <input ref={this.inputRef} className={classes} type={'range'} step={step} min={min} max={max} value={value} disabled={disabled} onChange={this.onSliderChange} />;
    }
}

Slider.displayName = 'Slider';

Slider.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(SIZES),
    color: PropTypes.oneOf(COLOR_TYPES),
    orientation: PropTypes.oneOf(ORIENTATIONS)
};

export default Slider;