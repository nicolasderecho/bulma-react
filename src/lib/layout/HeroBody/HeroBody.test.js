import React from 'react';
import { shallow } from 'enzyme';
import HeroBody from './HeroBody';

it('renders an element with class hero-body', () => {
    const heroBody = shallow(<HeroBody/>);
    expect(heroBody.find('div.hero-body').exists()).toBeTruthy();
});

it('renders a specific DOM element when requested', () => {
    const heroBody = shallow(<HeroBody as={'span'}/>);
    expect(heroBody.find(`span.hero-body`).exists()).toBeTruthy();
});

it('passes classes to the hero body', () => {
    const className = 'example';
    const heroBody = shallow(<HeroBody className={className}/>);
    expect(heroBody.find(`div.hero-body.${className}`).exists()).toBeTruthy();
});

it('renders the hero body\'s children', () => {
    const child = <span>child element</span>;
    const heroBody = shallow(<HeroBody>{child}</HeroBody>);
    expect(heroBody.contains(child)).toBeTruthy();
});