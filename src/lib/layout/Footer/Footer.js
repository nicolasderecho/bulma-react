import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const Footer = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'footer');
    const classes = classNames(className, 'footer');
    return <Element className={classes} {...props} />;
};

Footer.displayName = 'Footer';

Footer.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default Footer;