import React from 'react';
import { shallow } from 'enzyme';
import Footer from './Footer';

it('renders an element with class footer', () => {
    const footer = shallow(<Footer/>);
    expect(footer.find('footer.footer').exists()).toBeTruthy();
});

it('renders a specific DOM element when requested', () => {
    const footer = shallow(<Footer as={'div'}/>);
    expect(footer.find(`div.footer`).exists()).toBeTruthy();
});

it('passes classes to the footer', () => {
    const className = 'example';
    const footer = shallow(<Footer className={className}/>);
    expect(footer.find(`footer.footer.${className}`).exists()).toBeTruthy();
});

it('renders the footer\'s children', () => {
    const child = <span>child element</span>;
    const footer = shallow(<Footer>{child}</Footer>);
    expect(footer.contains(child)).toBeTruthy();
});