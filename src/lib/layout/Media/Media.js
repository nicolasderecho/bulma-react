import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";
import MediaLeft from "../MediaLeft/MediaLeft";
import MediaContent from "../MediaContent/MediaContent";
import MediaRight from "../MediaRight/MediaRight";

const Media = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'article');
    const classes = classNames(className, 'media');
    return <Element className={classes} {...props} />;
};

Media.displayName = 'Media';

Media.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

Media.Left    = MediaLeft;
Media.Content = MediaContent;
Media.Right   = MediaRight;

export default Media;