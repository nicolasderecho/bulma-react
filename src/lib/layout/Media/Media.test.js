import React from 'react';
import { shallow } from 'enzyme';
import Media from './Media';

it('renders an element with class media', () => {
    const media = shallow(<Media/>);
    expect(media.find('article.media').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const media = shallow(<Media as={'div'}/>);
    expect(media.find(`div.media`).exists()).toBeTruthy();
});

it('passes classes to the media', () => {
    const className = 'example';
    const media = shallow(<Media className={className}/>);
    expect(media.find(`article.media.${className}`).exists()).toBeTruthy();
});

it('renders the media\'s children', () => {
    const child = <span>child element</span>;
    const media = shallow(<Media>{child}</Media>);
    expect(media.contains(child)).toBeTruthy();
});