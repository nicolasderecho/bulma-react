import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {SIZES, htmlElementFor, sizeClassFor} from "../../helpers/util";

const Section = ({ className, as, size, ...props }) => {
    const Element    = htmlElementFor(as, 'section');
    const classes = classNames(className, 'section', sizeClassFor(size));
    return <Element className={classes} {...props} />;
};

Section.displayName = 'Section';

Section.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string,
    size: PropTypes.oneOf(SIZES)
};

export default Section;