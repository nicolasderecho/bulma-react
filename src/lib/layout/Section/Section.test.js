import React from 'react';
import { shallow } from 'enzyme';
import Section from './Section';
import {SIZES} from "../../helpers/util";
import {expectToRenderComponentWithSize} from "../../../testing/testHelpers";

it('renders an element with class section', () => {
    const section = shallow(<Section/>);
    expect(section.find('section.section').exists()).toBeTruthy();
});

it('renders a specific DOM element when requested', () => {
    const section = shallow(<Section as={'div'}/>);
    expect(section.find(`div.section`).exists()).toBeTruthy();
});

it('passes classes to the section', () => {
    const className = 'example';
    const section = shallow(<Section className={className}/>);
    expect(section.find(`section.section.${className}`).exists()).toBeTruthy();
});

it('renders the section\'s children', () => {
    const child = <span>child element</span>;
    const section = shallow(<Section>{child}</Section>);
    expect(section.contains(child)).toBeTruthy();
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Section, size) );
});