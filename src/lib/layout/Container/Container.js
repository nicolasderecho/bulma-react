import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperties, htmlElementFor, propsWithoutKeys} from "../../helpers/util";

const Container = ({ className, as, ...props }) => {
    const Element = htmlElementFor(as, 'div');
    const classes = classNames(className, 'container', checkProperties(props, ['fluid', 'fullhd', 'widescreen']));
    const finalProps = propsWithoutKeys(props, ['fluid', 'fullhd', 'widescreen']);
    return <Element className={classes} {...finalProps} />;
};

Container.displayName = 'Container';

Container.propTypes = {
    className: PropTypes.string
};

export default Container;