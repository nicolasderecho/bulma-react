import React from 'react';
import { shallow } from 'enzyme';
import Container from './Container';
import {expectToRenderComponentWithProperty} from "../../../testing/testHelpers";

it('renders a div with class container', () => {
    const element = shallow(<Container/>);
    expect(element.find('div.container').exists()).toBeTruthy();
});

it('passes classes to the container', () => {
    const className = 'example';
    const element = shallow(<Container className={className}/>);
    expect(element.find(`div.container.${className}`).exists()).toBeTruthy();
});

it('renders the container\'s children', () => {
    const child = <span>child element</span>;
    const element = shallow(<Container>{child}</Container>);
    expect(element.contains(child)).toBeTruthy();
});

it('renders the element requested by props', () => {
    const element = shallow(<Container as={'span'}/>);
    expect(element.find('span.container').exists()).toBeTruthy();
});

it('renders a fluid container when requested', () => {
    expectToRenderComponentWithProperty(Container, 'fluid');
});

it('renders a widescreen container when requested', () => {
    expectToRenderComponentWithProperty(Container, 'widescreen');
});

it('renders a fullhd container when requested', () => {
    expectToRenderComponentWithProperty(Container, 'fullhd');
});