import React from 'react';
import { shallow } from 'enzyme';
import MediaContent from './MediaContent';

it('renders an element with class media-content', () => {
    const mediaContent = shallow(<MediaContent/>);
    expect(mediaContent.find('div.media-content').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const mediaContent = shallow(<MediaContent as={'span'}/>);
    expect(mediaContent.find(`span.media-content`).exists()).toBeTruthy();
});

it('passes classes to the media', () => {
    const className = 'example';
    const mediaContent = shallow(<MediaContent className={className}/>);
    expect(mediaContent.find(`div.media-content.${className}`).exists()).toBeTruthy();
});

it('renders the media\'s children', () => {
    const child = <span>child element</span>;
    const mediaContent = shallow(<MediaContent>{child}</MediaContent>);
    expect(mediaContent.contains(child)).toBeTruthy();
});