import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const MediaContent = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'div');
    const classes = classNames(className, 'media-content');
    return <Element className={classes} {...props} />;
};

MediaContent.displayName = 'MediaContent';

MediaContent.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default MediaContent;