import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor, isEnabled, propsWithoutKeys, hierarchyClassFor, HIERARCHIES} from "../../helpers/util";
import {horizontalClassFor} from "../../helpers/columns_util";

const Tile = ({ className, as, hierarchy, horizontalSize, ...props }) => {
    const Element    = htmlElementFor(as, 'div');
    const classes = classNames(className, 'tile', {'is-vertical': isEnabled(props, 'vertical')}, hierarchyClassFor(hierarchy), horizontalClassFor(horizontalSize));
    const finalProps = propsWithoutKeys(props, ['vertical']);
    return <Element className={classes} {...finalProps} />;
};

Tile.displayName = 'Tile';

Tile.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string,
    hierarchy: PropTypes.oneOf(HIERARCHIES)
};

export default Tile;