import React from 'react';
import { shallow } from 'enzyme';
import Tile from './Tile';
import {expectToRenderComponentWithProperty, expectToRenderComponentWithHierarchy, expectToRenderComponentWithSizeNumber} from "../../../testing/testHelpers";
import {HIERARCHIES} from "../../helpers/util";
import {COLUMN_SIZE_NUMBERS} from "../../helpers/columns_util";

it('renders an element with class tile', () => {
    const tile = shallow(<Tile/>);
    expect(tile.find('div.tile').exists()).toBeTruthy();
});

it('renders a specific DOM element when requested', () => {
    const tile = shallow(<Tile as={'span'}/>);
    expect(tile.find(`span.tile`).exists()).toBeTruthy();
});

it('passes classes to the tile', () => {
    const className = 'example';
    const tile = shallow(<Tile className={className}/>);
    expect(tile.find(`div.tile.${className}`).exists()).toBeTruthy();
});

it('renders the tile\'s children', () => {
    const child = <span>child element</span>;
    const tile = shallow(<Tile>{child}</Tile>);
    expect(tile.contains(child)).toBeTruthy();
});

it('renders a vertical tile when requested', () => {
    expectToRenderComponentWithProperty(Tile, 'vertical');
});

it('renders the requested hierarchy', () => {
    HIERARCHIES.forEach((hierarchy) => expectToRenderComponentWithHierarchy(Tile, hierarchy));
});

it('renders the requested size number', () => {
   COLUMN_SIZE_NUMBERS.forEach((sizeNumber) => expectToRenderComponentWithSizeNumber(Tile, sizeNumber));
});