import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const HeroFoot = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'div');
    const classes = classNames(className, 'hero-foot');
    return <Element className={classes} {...props} />;
};

HeroFoot.displayName = 'HeroFoot';

HeroFoot.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default HeroFoot;