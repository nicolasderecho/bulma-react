import React from 'react';
import { shallow } from 'enzyme';
import HeroFoot from './HeroFoot';

it('renders an element with class hero-foot', () => {
    const heroFoot = shallow(<HeroFoot/>);
    expect(heroFoot.find('div.hero-foot').exists()).toBeTruthy();
});

it('renders a specific DOM element when requested', () => {
    const heroFoot = shallow(<HeroFoot as={'span'}/>);
    expect(heroFoot.find(`span.hero-foot`).exists()).toBeTruthy();
});

it('passes classes to the hero foot', () => {
    const className = 'example';
    const heroFoot = shallow(<HeroFoot className={className}/>);
    expect(heroFoot.find(`div.hero-foot.${className}`).exists()).toBeTruthy();
});

it('renders the hero foot\'s children', () => {
    const child = <span>child element</span>;
    const heroFoot = shallow(<HeroFoot>{child}</HeroFoot>);
    expect(heroFoot.contains(child)).toBeTruthy();
});