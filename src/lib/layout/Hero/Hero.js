import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    COLOR_TYPES,
    colorClassFor,
    htmlElementFor,
    sizeClassFor,
    HERO_SIZES,
    propsWithoutKeys, isEnabled
} from "../../helpers/util";
import HeroHead from "../HeroHead/HeroHead";
import HeroFoot from "../HeroFoot/HeroFoot";
import HeroBody from "../HeroBody/HeroBody";

const Hero = ({ className, as, color, size, ...props }) => {
    const Element    = htmlElementFor(as, 'section');
    const classes = classNames(className, 'hero', colorClassFor(color), sizeClassFor(size, HERO_SIZES), {'is-bold': isEnabled(props, 'bold')}, {'is-fullheight-with-navbar': isEnabled(props, 'fullheightWithNavbar')});
    const finalProps = propsWithoutKeys(props, ['bold', 'fullheightWithNavbar']);
    return <Element className={classes} {...finalProps} />;
};

Hero.displayName = 'Hero';

Hero.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES),
    size: PropTypes.oneOf(HERO_SIZES)
};

Hero.Head = HeroHead;
Hero.Body = HeroBody;
Hero.Foot = HeroFoot;

export default Hero;