import React from 'react';
import { shallow } from 'enzyme';
import Hero from './Hero';
import {camelCase, COLOR_TYPES, dashCase, HERO_SIZES} from "../../helpers/util";
import {
    expectToRenderComponentWithColor,
    expectToRenderComponentWithProperty,
    expectToRenderComponentWithSize
} from "../../../testing/testHelpers";

it('renders an element with class hero', () => {
    const hero = shallow(<Hero/>);
    expect(hero.find('section.hero').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const hero = shallow(<Hero as={'div'}/>);
    expect(hero.find(`div.hero`).exists()).toBeTruthy();
});

it('passes classes to the hero', () => {
    const className = 'example';
    const hero = shallow(<Hero className={className}/>);
    expect(hero.find(`section.hero.${className}`).exists()).toBeTruthy();
});

it('renders the hero\'s children', () => {
    const child = <span>child element</span>;
    const hero = shallow(<Hero>{child}</Hero>);
    expect(hero.contains(child)).toBeTruthy();
});

it('renders the hero with the proper styles', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Hero, color) );
});

it('adds the requested size by props', () => {
    HERO_SIZES.forEach((size) => expectToRenderComponentWithSize(Hero, size) );
});

it('renders a Hero with class is-bold when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Hero, 'bold');
});

it('renders a Hero with class is-fullheight-with-navbar when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Hero, 'fullheight-with-navbar');

});