import React from 'react';
import { shallow } from 'enzyme';
import MediaRight from './MediaRight';

it('renders an element with class media-right', () => {
    const mediaRight = shallow(<MediaRight/>);
    expect(mediaRight.find('div.media-right').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const mediaRight = shallow(<MediaRight as={'span'}/>);
    expect(mediaRight.find(`span.media-right`).exists()).toBeTruthy();
});

it('passes classes to the media', () => {
    const className = 'example';
    const mediaRight = shallow(<MediaRight className={className}/>);
    expect(mediaRight.find(`div.media-right.${className}`).exists()).toBeTruthy();
});

it('renders the media\'s children', () => {
    const child = <span>child element</span>;
    const mediaRight = shallow(<MediaRight>{child}</MediaRight>);
    expect(mediaRight.contains(child)).toBeTruthy();
});