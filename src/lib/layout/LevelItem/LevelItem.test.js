import React from 'react';
import { shallow } from 'enzyme';
import LevelItem from './LevelItem';

it('renders an element with class level-item', () => {
    const levelItem = shallow(<LevelItem/>);
    expect(levelItem.find('div.level-item').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const levelItem = shallow(<LevelItem as={'span'}/>);
    expect(levelItem.find(`span.level-item`).exists()).toBeTruthy();
});

it('passes classes to the level', () => {
    const className = 'example';
    const levelItem = shallow(<LevelItem className={className}/>);
    expect(levelItem.find(`div.level-item.${className}`).exists()).toBeTruthy();
});

it('renders the level\'s children', () => {
    const child = <span>child element</span>;
    const levelItem = shallow(<LevelItem>{child}</LevelItem>);
    expect(levelItem.contains(child)).toBeTruthy();
});