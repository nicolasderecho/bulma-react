import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const LevelItem = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'div');
    const classes = classNames(className, 'level-item');
    return <Element className={classes} {...props} />;
};

LevelItem.displayName = 'LevelItem';

LevelItem.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default LevelItem;