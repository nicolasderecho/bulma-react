import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const LevelLeft = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'div');
    const classes = classNames(className, 'level-left');
    return <Element className={classes} {...props} />;
};

LevelLeft.displayName = 'Level';

LevelLeft.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default LevelLeft;