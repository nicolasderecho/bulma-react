import React from 'react';
import { shallow } from 'enzyme';
import LevelLeft from './LevelLeft';

it('renders an element with class level-left', () => {
    const levelLeft = shallow(<LevelLeft/>);
    expect(levelLeft.find('div.level-left').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const levelLeft = shallow(<LevelLeft as={'span'}/>);
    expect(levelLeft.find(`span.level-left`).exists()).toBeTruthy();
});

it('passes classes to the level left', () => {
    const className = 'example';
    const levelLeft = shallow(<LevelLeft className={className}/>);
    expect(levelLeft.find(`div.level-left.${className}`).exists()).toBeTruthy();
});

it('renders the level left\'s children', () => {
    const child = <span>child element</span>;
    const levelLeft = shallow(<LevelLeft>{child}</LevelLeft>);
    expect(levelLeft.contains(child)).toBeTruthy();
});