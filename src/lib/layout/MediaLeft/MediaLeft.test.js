import React from 'react';
import { shallow } from 'enzyme';
import MediaLeft from './MediaLeft';

it('renders an element with class media-left', () => {
    const mediaLeft = shallow(<MediaLeft/>);
    expect(mediaLeft.find('div.media-left').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const mediaLeft = shallow(<MediaLeft as={'figure'}/>);
    expect(mediaLeft.find(`figure.media-left`).exists()).toBeTruthy();
});

it('passes classes to the media', () => {
    const className = 'example';
    const mediaLeft = shallow(<MediaLeft className={className}/>);
    expect(mediaLeft.find(`div.media-left.${className}`).exists()).toBeTruthy();
});

it('renders the media\'s children', () => {
    const child = <span>child element</span>;
    const mediaLeft = shallow(<MediaLeft>{child}</MediaLeft>);
    expect(mediaLeft.contains(child)).toBeTruthy();
});