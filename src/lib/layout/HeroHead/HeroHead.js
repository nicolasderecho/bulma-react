import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const HeroHead = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'div');
    const classes = classNames(className, 'hero-head');
    return <Element className={classes} {...props} />;
};

HeroHead.displayName = 'HeroHead';

HeroHead.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default HeroHead;