import React from 'react';
import { shallow } from 'enzyme';
import HeroHead from './HeroHead';

it('renders an element with class hero-head', () => {
    const heroHead = shallow(<HeroHead/>);
    expect(heroHead.find('div.hero-head').exists()).toBeTruthy();
});

it('renders a specific DOM element when requested', () => {
    const heroHead = shallow(<HeroHead as={'span'}/>);
    expect(heroHead.find(`span.hero-head`).exists()).toBeTruthy();
});

it('passes classes to the hero-head', () => {
    const className = 'example';
    const heroHead = shallow(<HeroHead className={className}/>);
    expect(heroHead.find(`div.hero-head.${className}`).exists()).toBeTruthy();
});

it('renders the hero head\'s children', () => {
    const child = <span>child element</span>;
    const heroHead = shallow(<HeroHead>{child}</HeroHead>);
    expect(heroHead.contains(child)).toBeTruthy();
});