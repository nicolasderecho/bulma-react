import React from 'react';
import { shallow } from 'enzyme';
import LevelRight from './LevelRight';

it('renders an element with class level-right', () => {
    const levelRight = shallow(<LevelRight/>);
    expect(levelRight.find('div.level-right').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const levelRight = shallow(<LevelRight as={'span'}/>);
    expect(levelRight.find(`span.level-right`).exists()).toBeTruthy();
});

it('passes classes to the level left', () => {
    const className = 'example';
    const levelRight = shallow(<LevelRight className={className}/>);
    expect(levelRight.find(`div.level-right.${className}`).exists()).toBeTruthy();
});

it('renders the level left\'s children', () => {
    const child = <span>child element</span>;
    const levelRight = shallow(<LevelRight>{child}</LevelRight>);
    expect(levelRight.contains(child)).toBeTruthy();
});