import React from 'react';
import { shallow } from 'enzyme';
import Level from './Level';

it('renders an element with class level', () => {
    const level = shallow(<Level/>);
    expect(level.find('nav.level').exists()).toBeTruthy();
});

it('renders a div element when requested', () => {
    const level = shallow(<Level as={'div'}/>);
    expect(level.find(`div.level`).exists()).toBeTruthy();
});

it('passes classes to the level', () => {
    const className = 'example';
    const level = shallow(<Level className={className}/>);
    expect(level.find(`nav.level.${className}`).exists()).toBeTruthy();
});

it('renders the level\'s children', () => {
    const child = <span>child element</span>;
    const level = shallow(<Level>{child}</Level>);
    expect(level.contains(child)).toBeTruthy();
});