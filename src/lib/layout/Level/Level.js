import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";
import LevelLeft from "../LevelLeft/LevelLeft";
import LevelRight from "../LevelRight/LevelRight";
import LevelItem from "../LevelItem/LevelItem";

const Level = ({ className, as, ...props }) => {
    const Element    = htmlElementFor(as, 'nav');
    const classes = classNames(className, 'level');
    return <Element className={classes} {...props} />;
};

Level.displayName = 'Level';

Level.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

Level.Left  = LevelLeft;
Level.Right = LevelRight;
Level.Item  = LevelItem;

export default Level;