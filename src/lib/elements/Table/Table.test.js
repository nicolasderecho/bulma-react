import React from 'react';
import { shallow } from 'enzyme';
import Table from './Table';

it('renders a table with class table', () => {
  const table = shallow(<Table/>);
  expect(table.find('table.table').exists()).toBeTruthy();
});

it('passes classes to the table', () => {
  const className = 'example';
  const table = shallow(<Table className={className}/>);
  expect(table.find(`table.table.${className}`).exists()).toBeTruthy();
});

it('renders the table\'s children', () => {
  const child = <span>child element</span>;
  const table = shallow(<Table>{child}</Table>);
  expect(table.contains(child)).toBeTruthy();
});

it('renders a hoverable table when requested', () => {
  const matchedTable = shallow(<Table hoverable />);
  const notMatchedTable = shallow(<Table hoverable={false} />);
  expect(matchedTable.props()).not.toHaveProperty('hoverable');
  expect(matchedTable.find('.is-hoverable').exists()).toBeTruthy();
  expect(notMatchedTable.find('.is-hoverable').exists()).toBeFalsy();
});

it('renders a bordered table when requested', () => {
  const matchedTable = shallow(<Table bordered />);
  const notMatchedTable = shallow(<Table bordered={false} />);
  expect(matchedTable.props()).not.toHaveProperty('bordered');
  expect(matchedTable.find('.is-bordered').exists()).toBeTruthy();
  expect(notMatchedTable.find('.is-bordered').exists()).toBeFalsy();
});

it('renders a striped table when requested', () => {
  const matchedTable = shallow(<Table striped />);
  const notMatchedTable = shallow(<Table striped={false} />);
  expect(matchedTable.props()).not.toHaveProperty('striped');
  expect(matchedTable.find('.is-striped').exists()).toBeTruthy();
  expect(notMatchedTable.find('.is-striped').exists()).toBeFalsy();
});

it('renders a narrow table when requested', () => {
  const matchedTable = shallow(<Table narrow />);
  const notMatchedTable = shallow(<Table narrow={false} />);
  expect(matchedTable.props()).not.toHaveProperty('narrow');
  expect(matchedTable.find('.is-narrow').exists()).toBeTruthy();
  expect(notMatchedTable.find('.is-narrow').exists()).toBeFalsy();
});

it('renders a fullwidth table when requested', () => {
  const matchedTable = shallow(<Table fullwidth />);
  const notMatchedTable = shallow(<Table fullwidth={false} />);
  expect(matchedTable.props()).not.toHaveProperty('fullwidth');
  expect(matchedTable.find('.is-fullwidth').exists()).toBeTruthy();
  expect(notMatchedTable.find('.is-fullwidth').exists()).toBeFalsy();
});