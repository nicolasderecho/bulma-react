import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {isEnabled, propsWithoutKeys} from "../../helpers/util";
import TableRow from "../TableRow/TableRow";
import TableHead from "../TableHead/TableHead";
import TableFooter from "../TableFooter/TableFooter";
import TableBody from "../TableBody/TableBody";
import TableCell from "../TableCell/TableCell";
import TableCellHeader from "../TableCellHeader/TableCellHeader";
import TableContainer from "../TableContainer/TableContainer";

const TABLE_OPTIONS = ['hoverable', 'bordered', 'striped', 'narrow', 'fullwidth'];

const Table = ({ className, ...props }) => {
  const tableOptionsClasses = TABLE_OPTIONS.map( (tableOption) => ( { [`is-${tableOption}`]: isEnabled(props, tableOption) } ) );
  const classes = classNames(className, 'table', ...tableOptionsClasses);
  const finalProps = propsWithoutKeys(props, ['hoverable', 'bordered', 'striped', 'narrow', 'fullwidth']);
  return <table className={classes} {...finalProps} />;
};

Table.displayName = 'Table';

Table.propTypes = {
  className: PropTypes.string
};

Table.Row         = TableRow;
Table.Head        = TableHead;
Table.Footer      = TableFooter;
Table.Body        = TableBody;
Table.Cell        = TableCell;
Table.CellHeader  = TableCellHeader;
Table.Container   = TableContainer;

export default Table;