import React from 'react';
import { shallow } from 'enzyme';
import TableFooter from './TableFooter';

it('renders a tfoot', () => {
  const tfoot = shallow(<TableFooter/>);
  expect(tfoot.find('tfoot').exists()).toBeTruthy();
});

it('passes classes to the tfoot', () => {
  const className = 'example';
  const tfoot = shallow(<TableFooter className={className}/>);
  expect(tfoot.find(`tfoot.${className}`).exists()).toBeTruthy();
});

it('renders the tfoot\'s children', () => {
  const child = <span>child element</span>;
  const tfoot = shallow(<TableFooter>{child}</TableFooter>);
  expect(tfoot.contains(child)).toBeTruthy();
});