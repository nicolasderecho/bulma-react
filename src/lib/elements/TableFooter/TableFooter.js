import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableFooter = ({ className, ...props }) => {
  return <tfoot className={classNames(className)} {...props} />;
};

TableFooter.displayName = 'TableFooter';

TableFooter.propTypes = {
  className: PropTypes.string
};

export default TableFooter;