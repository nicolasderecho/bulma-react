import React from 'react';
import { shallow } from 'enzyme';
import Tags from './Tags';
import {SIZES} from "../../helpers/util";
import {expectToRenderComponentWithPluralSize, expectToRenderComponentWithProperty} from "../../../testing/testHelpers";

it('renders a div with class tags', () => {
  const tags = shallow(<Tags/>);
  expect(tags.find('div.tags').exists()).toBeTruthy();
});

it('passes classes to the tags', () => {
  const className = 'example';
  const tags = shallow(<Tags className={className}/>);
  expect(tags.find(`div.tags.${className}`).exists()).toBeTruthy();
});

it('renders the tags\'s children', () => {
  const child = <span>child element</span>;
  const tags = shallow(<Tags>{child}</Tags>);
  expect(tags.contains(child)).toBeTruthy();
});

it('renders the element requested by props', () => {
  const tag = shallow(<Tags as={'span'}/>);
  expect(tag.find('span.tags').exists()).toBeTruthy();
});

it('renders a tag with class has-addons when the flag is enabled', () => {
  expectToRenderComponentWithProperty(Tags, 'addons', {prefix: 'has'});
});

it('renders a tags group with the proper size', () => {
  SIZES.forEach( (size) => expectToRenderComponentWithPluralSize(Tags, size) );
});