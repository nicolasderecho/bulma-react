import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {addonsClassFor, htmlElementFor, pluralSizeClassFor, propsWithoutKeys, SIZES} from "../../helpers/util";

const Tags = ({ className, as, size, ...props }) => {
  const Element = htmlElementFor(as, 'div');
  const classes = classNames(className, 'tags', addonsClassFor(props), pluralSizeClassFor(size));
  const finalProps = propsWithoutKeys(props, ['addons']);
  return <Element className={classes} {...finalProps} />;
};

Tags.displayName = 'Tags';

Tags.propTypes = {
  className: PropTypes.string,
  as: PropTypes.string,
  size: PropTypes.oneOf(SIZES)
};

export default Tags;