import React from 'react';
import { shallow } from 'enzyme';
import Tag from './Tag';
import {COLOR_TYPES, SIZES} from "../../helpers/util";
import {expectToRenderComponentWithColor, expectToRenderComponentWithSize} from "../../../testing/testHelpers";
import Button from "../Button/Button";

it('renders a span with class tag', () => {
    const tag = shallow(<Tag/>);
    expect(tag.find('span.tag').exists()).toBeTruthy();
});

it('passes classes to the tag', () => {
    const className = 'example';
    const tag = shallow(<Tag className={className}/>);
    expect(tag.find(`span.tag.${className}`).exists()).toBeTruthy();
});

it('renders the tag\'s children', () => {
    const child = <span>child element</span>;
    const tag = shallow(<Tag>{child}</Tag>);
    expect(tag.contains(child)).toBeTruthy();
});

it('renders the element requested by props', () => {
    const tag = shallow(<Tag as={'div'}/>);
    expect(tag.find('div.tag').exists()).toBeTruthy();
});

it('adds the requested color by props', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Tag, color) );
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Tag, size) );
});

it('adds the requested link class by props', () => {
    const linkButton = shallow(<Button link />);
    const notLinkButton = shallow(<Button link={false} />);
    expect(linkButton.find('.is-link').exists()).toBeTruthy();
    expect(notLinkButton.find('.is-link').exists()).toBeFalsy();
});

it('adds the requested delete class by props', () => {
    const deleteTag = shallow(<Tag isDelete />);
    const notDeleteTag = shallow(<Tag isDelete={false} />);
    expect(deleteTag.find('.is-delete').exists()).toBeTruthy();
    expect(notDeleteTag.find('.is-delete').exists()).toBeFalsy();
});

it('renders a rounded tag', () => {
    const roundedTag = shallow(<Tag rounded />);
    const notRoundedTag = shallow(<Tag rounded={false} />);
    expect(roundedTag.find('.is-rounded').exists()).toBeTruthy();
    expect(notRoundedTag.find('.is-rounded').exists()).not.toBeTruthy();
});