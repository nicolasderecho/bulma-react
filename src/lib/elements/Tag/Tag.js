import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {colorClassFor, htmlElementFor, isEnabled, roundedClassFor, sizeClassFor, SIZES, propsWithoutKeys} from "../../helpers/util";

const Tag = ({ className, as, size, color, ...props }) => {
    const Element = htmlElementFor(as, 'span');
    const classes = classNames(className, 'tag', sizeClassFor(size), colorClassFor(color), {'is-delete': isEnabled(props, 'isDelete')}, roundedClassFor(props));
    const finalProps = propsWithoutKeys(props, ['grouped', 'addons', 'isDelete', 'rounded']);
    return <Element className={classes} {...finalProps} />;
};

Tag.displayName = 'Tag';

Tag.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(SIZES)
};

export default Tag;