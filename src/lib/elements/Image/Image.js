import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor, isEnabled, propsWithoutKeys} from "../../helpers/util";

const IMAGE_DIMENSIONS = ['16x16','24x24','32x32','48x48','64x64','96x96','128x128','square','1by1','5by4','4by3','3by2','5by3','16by9','2by1','3by1','4by5','3by4','2by3','3by5','9by16','1by2','1by3'];

const Image = ({ className, wrapperClassName, is, wrapper, children, src, alt,...props }) => {
    const imgAlt = alt || '';
    const Wrapper = htmlElementFor(wrapper, 'figure');
    const wrapperClasses = classNames(wrapperClassName, 'image', {[`is-${is}`]: !!is });
    const classes = classNames(className, {'is-rounded': isEnabled(props, 'rounded')});
    const finalProps = propsWithoutKeys(props, ['rounded']);
    return <Wrapper className={wrapperClasses} {...finalProps} >
        { !!src
            ? <img src={src} alt={imgAlt} className={classes} />
            : children
        }
    </Wrapper>;
};

Image.displayName = 'Image';

Image.propTypes = {
    className: PropTypes.string,
    is: PropTypes.oneOf(IMAGE_DIMENSIONS),
    as: PropTypes.string,
    src: PropTypes.string,
    children: (props, propName) => {
        if ( !!props['src'] && !!props[propName]) {
            return new Error('Image Can receive the src props or a children to render the img element but It shouldn\'t receive both at the same time. Children will be ignored.');
        }
    },
};

export default Image;