import React from 'react';
import { shallow } from 'enzyme';
import Image from './Image';
import {expectToRenderComponentWithProperty} from "../../../testing/testHelpers";

const expectToRenderComponentWithDimension = (Component, dimension) => {
    const selector = `figure.is-${dimension}`;
    const element = shallow(<Component is={dimension}/>);
    expect(element.find(selector).exists()).toBeTruthy();
};

const IMAGE_SRC = 'https://bulma.io/images/placeholders/128x128.png';

it('renders a figure with class image', () => {
    const imageWrapper = shallow(<Image/>);
    expect(imageWrapper.find('figure.image').exists()).toBeTruthy();
});

it('passes classes to the element', () => {
    const className = 'example';
    const imageWrapper = shallow(<Image className={className} src={IMAGE_SRC}/>);
    expect(imageWrapper.find(`figure.image img.${className}`).exists()).toBeTruthy();
});

it('passes classes to the wrapper element', () => {
    const className = 'example';
    const imageWrapper = shallow(<Image wrapperClassName={className}/>);
    expect(imageWrapper.find(`figure.${className}`).exists()).toBeTruthy();
});

it('renders the element\'s children', () => {
    const child = <span>child element</span>;
    const imageWrapper = shallow(<Image>{child}</Image>);
    expect(imageWrapper.contains(child)).toBeTruthy();
});

it('renders the requested wrapper by props', () => {
    const iconWrapper = shallow(<Image wrapper={'div'}/>);
    expect(iconWrapper.find('div.image').exists()).toBeTruthy();
});

it('adds the requested dimension by props', () => {
    expectToRenderComponentWithDimension(Image, '16x16');
    expectToRenderComponentWithDimension(Image, '24x24');
    expectToRenderComponentWithDimension(Image, '32x32');
    expectToRenderComponentWithDimension(Image, '48x48');
    expectToRenderComponentWithDimension(Image, '64x64');
    expectToRenderComponentWithDimension(Image, '96x96');
    expectToRenderComponentWithDimension(Image, '128x128');
    expectToRenderComponentWithDimension(Image, 'square');
    expectToRenderComponentWithDimension(Image, '1by1');
    expectToRenderComponentWithDimension(Image, '5by4');
    expectToRenderComponentWithDimension(Image, '4by3');
    expectToRenderComponentWithDimension(Image, '3by2');
    expectToRenderComponentWithDimension(Image, '5by3');
    expectToRenderComponentWithDimension(Image, '16by9');
    expectToRenderComponentWithDimension(Image, '2by1');
    expectToRenderComponentWithDimension(Image, '3by1');
    expectToRenderComponentWithDimension(Image, '4by5');
    expectToRenderComponentWithDimension(Image, '3by4');
    expectToRenderComponentWithDimension(Image, '2by3');
    expectToRenderComponentWithDimension(Image, '3by5');
    expectToRenderComponentWithDimension(Image, '9by16');
    expectToRenderComponentWithDimension(Image, '1by2');
    expectToRenderComponentWithDimension(Image, '1by3');
});

it('doesn\'t allow an invalid dimension', () => {
    const wrapper = () => shallow(<Image is={'0x0'}/>);
    expect(wrapper).toThrow();
});

it('renders a child img element with the received src', () => {
    const imageWrapper = shallow(<Image src={IMAGE_SRC}/>);
    const image = imageWrapper.find('figure.image img');
    expect(image.exists()).toBeTruthy();
    expect(image.prop('src')).toEqual(IMAGE_SRC);
});

it('renders a custom element received by children', () => {
    const imageWrapper = shallow(<Image><img alt={'custom'}/></Image>);
    const image = imageWrapper.find('figure.image img');
    expect(image.exists()).toBeTruthy();
    expect(image.prop('alt')).toEqual('custom');
});

it('raises and error when it receives both children and img src', () => {
    const renderInvalidComponent = () => shallow(<Image src={IMAGE_SRC}><img alt={'custom'}/></Image>);
    expect(renderInvalidComponent).toThrow();
});

it('renders a rounded image', () => {
    const roundedImage = shallow(<Image rounded src={IMAGE_SRC}/>);
    const anotherRoundedImage = shallow(<Image rounded={true} src={IMAGE_SRC}/>);
    const notRoundedImage = shallow(<Image rounded={false} src={IMAGE_SRC}/>);
    expect(roundedImage.find('img.is-rounded').exists()).toBeTruthy();
    expect(anotherRoundedImage.find('figure.image img.is-rounded').exists()).toBeTruthy();
    expect(notRoundedImage.find('figure.image img.is-rounded').exists()).not.toBeTruthy();
});

it('adds custom classes to the img element', () => {
    const className = 'example';
    const imageWrapper = shallow(<Image className={className} src={IMAGE_SRC}/>);
    expect(imageWrapper.find(`figure.image img.${className}`).exists()).toBeTruthy();
});