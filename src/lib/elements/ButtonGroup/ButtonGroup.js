import React from 'react';
import PropTypes from 'prop-types';
import Field from "../../form/Field/Field";

const ButtonGroup = (props) => {
  return <Field grouped={true} {...props} />;
};

ButtonGroup.displayName = 'ButtonGroup';

ButtonGroup.propTypes = {
  className: PropTypes.string
};

export default ButtonGroup;