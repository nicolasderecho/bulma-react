import React from 'react';
import { shallow, mount } from 'enzyme';
import ButtonGroup from './ButtonGroup';

it('renders a div with class field and is-grouped', () => {
  const buttonGroup = mount(<ButtonGroup/>);
  expect(buttonGroup.find('div.field.is-grouped').exists()).toBeTruthy();
});

it('renders the button group\'s children', () => {
  const child = <span>Child Element</span>;
  const buttonGroup = shallow(<ButtonGroup>{child}</ButtonGroup>);
  expect(buttonGroup.contains(child)).toBeTruthy();
});