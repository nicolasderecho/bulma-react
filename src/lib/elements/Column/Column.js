import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {camelCase, htmlElementFor, isEnabled, propsWithoutKeys} from "../../helpers/util";
import { COLUMN_SIZES, columnOffsetlassFor, columnSizeClassFor, combineValuesWithKey, DEVICES } from "../../helpers/columns_util";

const KEYS_TO_FILTER = [ ...['narrow'], ...combineValuesWithKey(DEVICES, 'narrow'), ...combineValuesWithKey(DEVICES, 'Offset'), ...combineValuesWithKey(DEVICES, 'ColumnSize'), ...DEVICES ];

const checkDerivedClasses = (props, object, device) => Object.assign({}, {
  narrow: object.narrow.concat({[`is-narrow-${device}`]: isEnabled(props, `${device}Narrow`)}),
  device: object.device.concat({[`is-${device}`]: isEnabled(props, `${camelCase(device)}`)}),
  deviceColumnSizes: object.deviceColumnSizes.concat(columnSizeClassFor(props[`${device}ColumnSize`], device)),
  deviceOffsets: object.deviceOffsets.concat(columnOffsetlassFor(props[`${device}Offset`], device))
});
const initialDerivedValues = () => ({ narrow: [], device: [], deviceColumnSizes: [], deviceOffsets: []});

const Column = ({ className, as, columnSize, offset, ...props }) => {
  const Element = htmlElementFor(as, 'div');
  const derivedClasses = DEVICES.reduce((object, device) => checkDerivedClasses(props, object, device), initialDerivedValues());
  const classes = classNames(className, 'column', columnSizeClassFor(columnSize), columnOffsetlassFor(offset), {'is-narrow': isEnabled(props, 'narrow')}, ...derivedClasses.narrow, ...derivedClasses.device, ...derivedClasses.deviceColumnSizes, ...derivedClasses.deviceOffsets);
  const finalProps = propsWithoutKeys(props, KEYS_TO_FILTER);
  return <Element className={classes} {...finalProps} />;
};

Column.displayName = 'Column';

Column.propTypes = {
  className: PropTypes.string,
  as: PropTypes.string,
  columnSize: PropTypes.oneOf(COLUMN_SIZES),
  mobileColumnSize: PropTypes.oneOf(COLUMN_SIZES),
  tabletColumnSize: PropTypes.oneOf(COLUMN_SIZES),
  touchColumnSize: PropTypes.oneOf(COLUMN_SIZES),
  desktopColumnSize: PropTypes.oneOf(COLUMN_SIZES),
  widescreenColumnSize: PropTypes.oneOf(COLUMN_SIZES),
  fullhdColumnSize: PropTypes.oneOf(COLUMN_SIZES),
  mobileOffset: PropTypes.oneOf(COLUMN_SIZES),
  tabletOffset: PropTypes.oneOf(COLUMN_SIZES),
  touchOffset: PropTypes.oneOf(COLUMN_SIZES),
  desktopOffset: PropTypes.oneOf(COLUMN_SIZES),
  widescreenOffset: PropTypes.oneOf(COLUMN_SIZES),
  fullhdOffset: PropTypes.oneOf(COLUMN_SIZES),
  offset: PropTypes.oneOf(COLUMN_SIZES),
};

export default Column;