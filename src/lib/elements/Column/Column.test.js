import React from 'react';
import { shallow } from 'enzyme';
import Column from './Column';
import {COLUMN_SIZES, COLUMN_SIZES_HASH, DEVICES} from "../../helpers/columns_util";
import {camelCase} from "../../helpers/util";
import {expectToRenderComponentWithProperty} from "../../../testing/testHelpers";

it('renders a div with class column', () => {
  const column = shallow(<Column/>);
  expect(column.find('div.column').exists()).toBeTruthy();
});

it('renders the requested element by props', () => {
  const column = shallow(<Column as={'span'}/>);
  expect(column.find('span.column').exists()).toBeTruthy();
});

it('passes classes to the element', () => {
  const className = 'example';
  const column = shallow(<Column className={className}/>);
  expect(column.find(`div.column.${className}`).exists()).toBeTruthy();
});

it('renders the element\'s children', () => {
  const child = <span>child element</span>;
  const column = shallow(<Column>{child}</Column>);
  expect(column.contains(child)).toBeTruthy();
});

const expectToRendedColumnWithDimension = (propName, propValue, prefix, suffix = '') => {
  const selectorName = COLUMN_SIZES_HASH[propValue];
  const selector = !!suffix ? `.${prefix}-${selectorName}-${suffix}` : `.${prefix}-${selectorName}`;
  const props = { [propName]: propValue };
  const element = shallow(<Column {...props} />);
  expect(element.find(selector).exists()).toBeTruthy();
};

it('renders the element with the proper size', () => {
  COLUMN_SIZES.forEach( (columnSize) => expectToRendedColumnWithDimension('columnSize', columnSize, 'is'));
});

it('renders the element with the proper size for the requested device', () => {
  DEVICES.forEach((device) => {
    COLUMN_SIZES.forEach( (columnSize) => expectToRendedColumnWithDimension(`${device}ColumnSize`, columnSize, 'is', device));
  });
});

it('renders the element with the proper offset', () => {
  COLUMN_SIZES.forEach( (columnSize) => expectToRendedColumnWithDimension('offset', columnSize, 'is-offset'));
});

it('renders the element with the proper offset for the requested device', () => {
  DEVICES.forEach((device) => {
    COLUMN_SIZES.forEach( (columnSize) => expectToRendedColumnWithDimension(`${device}Offset`, columnSize, 'is-offset', device));
  });
});

it('doesn\'t allow an invalid column size', () => {
  const invalidSizeNumber = '10/4';
  const wrapper = () => shallow(<Column columnSize={invalidSizeNumber}/>);
  expect(wrapper).toThrow();
});

it('doesn\'t allow an invalid offset', () => {
  const invalidOffsetNumber = '10/4';
  const wrapper = () => shallow(<Column offset={invalidOffsetNumber}/>);
  expect(wrapper).toThrow();
});

it('renders a narrow column when requested', () => {
  expectToRenderComponentWithProperty(Column, 'narrow');
});

it('renders a narrow column for the specified device', () => {
  DEVICES.forEach((device) => expectToRenderComponentWithProperty(Column, `${device}Narrow`, { propertyValue: `narrow-${device}` }) );
});

it('renders a column with the specified device', () => {
  DEVICES.forEach((device) => expectToRenderComponentWithProperty(Column, device));
});