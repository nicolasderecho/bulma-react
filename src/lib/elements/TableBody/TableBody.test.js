import React from 'react';
import { shallow } from 'enzyme';
import TableBody from './TableBody';

it('renders a tbody', () => {
  const tbody = shallow(<TableBody/>);
  expect(tbody.find('tbody').exists()).toBeTruthy();
});

it('passes classes to the tbody', () => {
  const className = 'example';
  const tbody = shallow(<TableBody className={className}/>);
  expect(tbody.find(`tbody.${className}`).exists()).toBeTruthy();
});

it('renders the tbody\'s children', () => {
  const child = <span>child element</span>;
  const tbody = shallow(<TableBody>{child}</TableBody>);
  expect(tbody.contains(child)).toBeTruthy();
});