import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableBody = ({ className, ...props }) => {
  return <tbody className={classNames(className)} {...props} />;
};

TableBody.displayName = 'TableBody';

TableBody.propTypes = {
  className: PropTypes.string
};

export default TableBody;