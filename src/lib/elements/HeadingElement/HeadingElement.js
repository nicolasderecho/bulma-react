import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor, isEnabled, propsWithoutKeys, SIZE_NUMBERS, sizeNumberClassFor} from "../../helpers/util";

const HeadingElement = ({ className, as, sizeNumber, ...props }) => {
  const Element = htmlElementFor(as, 'h1');
  const classes = classNames(className, {'is-spaced': isEnabled(props, 'spaced')}, sizeNumberClassFor(sizeNumber));
  const finalProps = propsWithoutKeys(props, ['spaced']);
  return <Element className={classes} {...finalProps} />;
};

HeadingElement.displayName = 'HeadingElement';

HeadingElement.propTypes = {
  className: PropTypes.string,
  as: PropTypes.string,
  sizeNumber: (props, propName) => {
    const value = props[propName];
    const isInvalidSizeNumber = !!value && SIZE_NUMBERS.indexOf(value.toString()) === -1;
    if(isInvalidSizeNumber) {
      return new Error(`${value} is an invalid size number. must be one of ${SIZE_NUMBERS.join(', ')}`);
    }
  }
};

export default HeadingElement;