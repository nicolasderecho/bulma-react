import React from 'react';
import { shallow } from 'enzyme';
import HeadingElement from './HeadingElement';
import {SIZE_NUMBERS} from "../../helpers/util";
import Button from "../Button/Button.test";

it('renders a h1 element by default', () => {
  const element = shallow(<HeadingElement/>);
  expect(element.find('h1').exists()).toBeTruthy();
});

it('renders the requested element by props', () => {
  const content = shallow(<HeadingElement as={'h2'} className={'i-am-h2'}/>);
  expect(content.find('h2.i-am-h2').exists()).toBeTruthy();
});

it('passes classes to the element', () => {
  const className = 'title';
  const element = shallow(<HeadingElement className={className}/>);
  expect(element.find(`h1.title.${className}`).exists()).toBeTruthy();
});

it('renders the element\'s children', () => {
  const child = <span>child element</span>;
  const element = shallow(<HeadingElement>{child}</HeadingElement>);
  expect(element.contains(child)).toBeTruthy();
});

it('renders a spaced element when requested', () => {
  const matchedTable = shallow(<HeadingElement spaced />);
  const notMatchedTable = shallow(<HeadingElement spaced={false} />);
  expect(matchedTable.props()).not.toHaveProperty('spaced');
  expect(matchedTable.find('.is-spaced').exists()).toBeTruthy();
  expect(notMatchedTable.find('.is-spaced').exists()).toBeFalsy();
});

it('renders an element with the requested size', () => {
  SIZE_NUMBERS.forEach((sizeNumber) => {
    const element = shallow(<HeadingElement sizeNumber={parseInt(sizeNumber)}/>);
    expect(element.find(`.is-${sizeNumber}`).exists()).toBeTruthy();
  });
});

it('doesn\'t allow invalid size numbers', () => {
  const wrapper = () => shallow(<HeadingElement sizeNumber={invalidSizeNumber}/>);
  expect(wrapper).toThrow();
});