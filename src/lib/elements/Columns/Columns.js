import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {capitalize, checkProperties, htmlElementFor, propsWithoutKeys} from "../../helpers/util";
import Column from "../Column/Column";
import {deviceActiveClassFor, gapSizeFor, isVariableGap, gapSizeForDevices, DEVICES} from "../../helpers/columns_util";

const PROP_KEYS_TO_FILTER = ['gapless', 'multiline', 'centered', 'vcentered'].concat(DEVICES.map((device) => `gap${capitalize(device)}`));

const Columns = ({ className, as, from, gap, ...props }) => {
  const Element = htmlElementFor(as, 'div');
  const classes = classNames(className, 'columns', checkProperties(props, PROP_KEYS_TO_FILTER), deviceActiveClassFor(from), gapSizeFor(gap), isVariableGap(gap, props), ...gapSizeForDevices(props));
  const finalProps = propsWithoutKeys(props, PROP_KEYS_TO_FILTER);
  return <Element className={classes} {...finalProps} />;
};

Columns.displayName = 'Columns';

Columns.propTypes = {
  className: PropTypes.string,
  as: PropTypes.string,
  from: PropTypes.oneOf(DEVICES),
};

Columns.Column = Column;

export default Columns;