import React from 'react';
import { shallow } from 'enzyme';
import Columns from './Columns';
import {expectToRenderComponentWithProperty} from "../../../testing/testHelpers";
import {DEVICES, GAP_SIZES} from "../../helpers/columns_util";
import {capitalize} from "../../helpers/util";

it('renders a div with class columns', () => {
  const columns = shallow(<Columns/>);
  expect(columns.find('div.columns').exists()).toBeTruthy();
});

it('renders the requested element by props', () => {
  const columns = shallow(<Columns as={'span'}/>);
  expect(columns.find('span.columns').exists()).toBeTruthy();
});

it('passes classes to the element', () => {
  const className = 'example';
  const columns = shallow(<Columns className={className}/>);
  expect(columns.find(`div.columns.${className}`).exists()).toBeTruthy();
});

it('renders the element\'s children', () => {
  const child = <span>child element</span>;
  const columns = shallow(<Columns>{child}</Columns>);
  expect(columns.contains(child)).toBeTruthy();
});

it('renders gapless columns when requested', () => {
  expectToRenderComponentWithProperty(Columns, 'gapless');
});

it('renders multiline columns when requested', () => {
  expectToRenderComponentWithProperty(Columns, 'multiline');
});

it('renders vertical centered columns when requested', () => {
  expectToRenderComponentWithProperty(Columns, 'vcentered');
});

it('renders centered columns when requested', () => {
  expectToRenderComponentWithProperty(Columns, 'centered');
});

it('renders columns active from the specified device', () => {
  DEVICES.forEach((device) => {
    const columns = shallow(<Columns from={device}/>);
    expect(columns.find(`.columns.is-${device}`).exists()).toBeTruthy();
    expect(columns.props()).not.toHaveProperty('from');
  });
});

it('renders columns with gap size when requested', () => {
  GAP_SIZES.forEach((gapSize) => {
    const columns = shallow(<Columns gap={gapSize}/>);
    expect(columns.find('.columns.is-variable').exists()).toBeTruthy();
    expect(columns.find(`.columns.is-${gapSize}`).exists()).toBeTruthy();
  });
});

it('renders columns with gap size for the needed device when requested', () => {
  DEVICES.forEach( (device) => {
    GAP_SIZES.forEach((gapSize) => {
      const propName = `gap${capitalize(device)}`;
      const props = { [propName]: gapSize };
      const columns = shallow(<Columns {...props }/>);
      expect(columns.find('.columns.is-variable').exists()).toBeTruthy();
      expect(columns.find(`.columns.is-${gapSize}-${device}`).exists()).toBeTruthy();
      expect(columns.props()).not.toHaveProperty(propName);
    });
  });
});