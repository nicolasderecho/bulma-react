import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import HeadingElement from "../HeadingElement/HeadingElement";

const Subtitle = ({ className, ...props }) => {
  const classes = classNames(className, 'subtitle');
  return <HeadingElement className={classes} {...props} />;
};

Subtitle.displayName = 'Subtitle';

Subtitle.propTypes = {
  className: PropTypes.string
};

export default Subtitle;