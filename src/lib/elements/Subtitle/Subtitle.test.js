import React from 'react';
import { shallow } from 'enzyme';
import Subtitle from './Subtitle';

it('renders an element with class title', () => {
  const title = shallow(<Subtitle/>);
  expect(title.find('.subtitle').exists()).toBeTruthy();
});