import React from 'react';
import SelectWrapper from './SelectWrapper';
import {
    expectToRenderComponentWithChildren, expectToRenderComponentWithColor,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty, expectToRenderComponentWithSize, expectToRenderComponentWithState
} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";
import {COLOR_TYPES, SIZES, STATES} from "../../helpers/util";

it('renders a div with class select', () => {
    expectToRenderComponentWithElement(SelectWrapper, 'div', 'select');
});

it('renders the select wrapper\'s children', () => {
    expectToRenderComponentWithChildren(SelectWrapper);
});

it('renders the element requested by props', () => {
    const element = shallow(<SelectWrapper as={'span'}/>);
    expect(element.find('span.select').exists()).toBeTruthy();
});

it('renders a fullwidth wrapper when requested', () => {
   expectToRenderComponentWithProperty(SelectWrapper, 'fullwidth');
});

it('renders a multiple wrapper when requested', () => {
    expectToRenderComponentWithProperty(SelectWrapper, 'multiple');
});

it('adds the requested color by props', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(SelectWrapper, color) );
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(SelectWrapper, size) );
});

it('adds the requested state by props', () => {
    STATES.forEach((state) => expectToRenderComponentWithState(SelectWrapper, state) );
});