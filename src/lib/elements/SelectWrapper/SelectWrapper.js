import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    checkProperties,colorClassFor,
    htmlElementFor, stateClassFor,
    propsWithoutKeys, sizeClassFor,
    SIZES, COLOR_TYPES, STATES
} from "../../helpers/util";

const PROPERTIES_TO_CHECK = ['fullwidth', 'multiple'];

const SelectWrapper = ({ className, as, color, size, state, ...props }) => {
    const Element = htmlElementFor(as, 'div');
    const classes = classNames(className, 'select', checkProperties(props, PROPERTIES_TO_CHECK), colorClassFor(color), sizeClassFor(size), stateClassFor(state));
    const finalProps = propsWithoutKeys(props, PROPERTIES_TO_CHECK);
    return <Element className={classes} {...finalProps} />;
};

SelectWrapper.displayName = 'SelectWrapper';

SelectWrapper.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(SIZES),
    color: PropTypes.oneOf(COLOR_TYPES),
    state: PropTypes.oneOf(STATES)
};

export default SelectWrapper;