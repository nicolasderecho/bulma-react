import React from 'react';
import { shallow } from 'enzyme';
import Button from './Button';
import {COLOR_TYPES, SIZES, STATES} from "../../helpers/util";
import {
  expectToRenderComponentWithSize,
  expectToRenderComponentWithColor,
  expectToRenderComponentWithElement, expectToRenderComponentWithProperty, expectToRenderComponentWithState
} from "../../../testing/testHelpers";

const buttonClass = 'button';

it('renders an element with class button', () => {
  expectToRenderComponentWithElement(Button, 'button', buttonClass);
});

it('renders a button element by default', () => {
  const button = shallow(<Button/>);
  expect(button.find(`button.${buttonClass}`).exists()).toBeTruthy();
});

it('renders a link element when the \'as\' property is link', () => {
  const button = shallow(<Button as={'link'}/>);
  expect(button.find(`a.${buttonClass}`).exists()).toBeTruthy();
});

it('renders a link element when the \'as\' property is a', () => {
  const button = shallow(<Button as={'a'}/>);
  expect(button.find(`a.${buttonClass}`).exists()).toBeTruthy();
});

it('renders an outlined button', () => {
  expectToRenderComponentWithProperty(Button, 'outlined');
});

it('renders an inverted button', () => {
  expectToRenderComponentWithProperty(Button, 'inverted');
});

it('renders a loading button', () => {
  expectToRenderComponentWithProperty(Button, 'loading');
});

it('adds the requested color by props', () => {
  COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Button, color) );
});

it('adds the requested size by props', () => {
  SIZES.forEach((size) => expectToRenderComponentWithSize(Button, size) );
});

it('renders a rounded button when requested', () => {
  expectToRenderComponentWithProperty(Button, 'rounded');
});

it('renders an active button when requested', () => {
  expectToRenderComponentWithProperty(Button, 'active');
});

it('adds the requested state by props', () => {
  STATES.forEach((state) => expectToRenderComponentWithState(Button, state) );
});

it('renders a text button when requested', () => {
  expectToRenderComponentWithProperty(Button, 'text');
});

describe('when it\'s not an input button', () => {

  it('displays the button\'s children', () => {
    const content = <span>content</span>;
    const button = shallow(<Button as={'button'}>{content}</Button>);
    const buttonLink = shallow(<Button as={'link'}>{content}</Button>);
    expect(button.contains(content)).toBeTruthy();
    expect(buttonLink.contains(content)).toBeTruthy();
  });

  it('skips specific properties for input buttons', () => {
    const button = shallow(<Button as={'button'}><span>Common Button</span></Button>);
    const buttonLink = shallow(<Button as={'link'}><span>Link Button</span></Button>);
    const buttonProps = button.find('button').props();
    const buttonLinkProps = buttonLink.find('a').props();
    expect(buttonProps.type).not.toBeDefined();
    expect(buttonProps.value).not.toBeDefined();
    expect(buttonLinkProps.type).not.toBeDefined();
    expect(buttonLinkProps.value).not.toBeDefined();
  });

});

describe('when the props match for an input element', () => {

  it('renders an input element', () => {
    const button = shallow(<Button as={'input'} value={'my button'} />);
    expect(button.find(`input.${buttonClass}`).exists()).toBeTruthy();
  });

  it('sets the input type as submit by default', () => {
    const button = shallow(<Button as={'input'} value={'my button'} />);
    expect(button.find(`input.${buttonClass}`).props().type).toBe('submit');
  });

  it('doesn\'t allow to pass children', () => {
    const wrapper = () => shallow(<Button as={'input'}><span>something</span></Button>);
    expect(wrapper).toThrowError(/Button cannot receive children/);
  });

  it('requires an input value', () => {
    const wrapper = () => shallow(<Button as={'input'} />);
    expect(wrapper).toThrowError(/must have a value prop/);
  });
6
  it('doesn\'t allow invalid input types', () => {
    const wrapper = () => shallow(<Button as={'input'} type={'text'} value={'example'}/>);
    expect(wrapper).toThrow();
  });

});