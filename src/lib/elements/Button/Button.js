import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  activeClassFor, checkProperty,
  COLOR_TYPES,
  colorClassFor,
  htmlElementFor,
  invertedClassFor,
  linkClassFor, loadingClassFor,
  outlinedClassFor, propsWithoutKeys, roundedClassFor,
  sizeClassFor, SIZES, stateClassFor, STATES
} from "../../helpers/util";

const PROPS_TO_FILTER = ['outlined', 'inverted', 'link', 'loading', 'rounded', 'active', 'text'];

const Button = ({ className, as, type, value, color, size, state, ...props }) => {
  const Element    = htmlElementFor(as, 'button');
  const inputType  = type || 'submit';
  const classes    = classNames(className, 'button', outlinedClassFor(props), invertedClassFor(props), linkClassFor(props), colorClassFor(color), sizeClassFor(size), loadingClassFor(props), roundedClassFor(props), activeClassFor(props), checkProperty(props, 'text'), stateClassFor(state));
  const filteredProps = propsWithoutKeys(props, PROPS_TO_FILTER);
  const finalProps = as === 'input' ? { type: inputType, value: value, ...filteredProps } : filteredProps;
  return <Element className={classes} {...finalProps} />;
};

Button.displayName = 'Button';

Button.propTypes = {
  className: PropTypes.string,
  as: PropTypes.oneOf(['a', 'input', 'link', 'button', 'div', 'span']),
  children: (props, propName) => {
    if ( (props['as'] === 'input') && !!props[propName]) {
      return new Error('Button cannot receive children prop if it\'s an input. You should pass the content in \'value\' prop instead.');
    }
  },
  value: (props, propName) => {
    if ( (props['as'] === 'input') && (props[propName] === undefined || props[propName] === null) ) {
      return new Error(`Button input must have a value prop`);
    }
  },
  type: PropTypes.oneOf(['submit', 'reset']),
  color: PropTypes.oneOf(COLOR_TYPES),
  size: PropTypes.oneOf(SIZES),
  state: PropTypes.oneOf(STATES)
};

export default Button;