import React from 'react';
import { shallow } from 'enzyme';
import Title from './Title';

it('renders an element with class title', () => {
  const title = shallow(<Title/>);
  expect(title.find('.title').exists()).toBeTruthy();
});