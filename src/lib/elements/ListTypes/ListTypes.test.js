import React from 'react';
import { mount } from 'enzyme';
import { OrderedList, UnorderedList, DescriptionList } from './ListTypes';

const expectToRenderElement = (Component, element) => {
    const shallowComponent = mount(<Component />);
    expect(shallowComponent.find(element).exists()).toBeTruthy();
};

it('renders the proper list type', () => {
    expectToRenderElement(OrderedList, 'ol');
    expectToRenderElement(UnorderedList, 'ul');
    expectToRenderElement(DescriptionList, 'dl');
});