import React from 'react';
import List from "../List/List";
import ListItem from "../ListItem/ListItem";

const OrderedList = (props) => <List as={'ol'} {...props} />;
OrderedList.displayName = 'OrderedList';
OrderedList.Item = ListItem;

const UnorderedList = (props) => <List as={'ul'} {...props} />;
UnorderedList.displayName = 'UnorderedList';
UnorderedList.Item = ListItem;

const DescriptionList = (props) => <List as={'dl'} {...props} />;
DescriptionList.displayName = 'DescriptionList';
DescriptionList.Item = ListItem;

export { OrderedList, UnorderedList, DescriptionList };