import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {COLOR_TYPES, colorClassFor, htmlElementFor} from "../../helpers/util";

const Notification = ({ className, as, color, ...props }) => {
    const Element = htmlElementFor(as, 'div');
    const classes = classNames(className, 'notification', colorClassFor(color) );
    return <Element className={classes} {...props} />;
};

Notification.displayName = 'Notification';

Notification.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES)
};

export default Notification;