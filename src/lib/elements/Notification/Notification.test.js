import React from 'react';
import { shallow } from 'enzyme';
import Notification from './Notification';
import {COLOR_TYPES} from "../../helpers/util";
import {expectToRenderComponentWithColor} from "../../../testing/testHelpers";

it('renders a div with class notification', () => {
    const notification = shallow(<Notification/>);
    expect(notification.find('div.notification').exists()).toBeTruthy();
});

it('renders the element requested by props', () => {
    const notification = shallow(<Notification as={'span'}/>);
    expect(notification.find('span.notification').exists()).toBeTruthy();
});

it('passes classes to the notification', () => {
    const className = 'example';
    const notification = shallow(<Notification className={className}/>);
    expect(notification.find(`div.notification.${className}`).exists()).toBeTruthy();
});

it('renders the notification\'s children', () => {
    const child = <span>child element</span>;
    const notification = shallow(<Notification>{child}</Notification>);
    expect(notification.contains(child)).toBeTruthy();
});

it('renders the notification with the proper styles', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Notification, color) );
});