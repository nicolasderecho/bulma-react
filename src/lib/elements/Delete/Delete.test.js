import React from 'react';
import { shallow } from 'enzyme';
import Delete from './Delete';
import {expectToRenderComponentWithSize} from "../../../testing/testHelpers";
import {SIZES} from "../../helpers/util";

it('renders a link with class delete', () => {
    const deleteLink = shallow(<Delete/>);
    const anotherDeleteLink = shallow(<Delete as={'link'}/>);
    expect(deleteLink.find('a.delete').exists()).toBeTruthy();
    expect(anotherDeleteLink.find('a.delete').exists()).toBeTruthy();
});

it('passes classes to the component', () => {
    const className = 'example';
    const deleteComponent = shallow(<Delete className={className}/>);
    expect(deleteComponent.find(`a.${className}`).exists()).toBeTruthy();
});

it('renders the component\'s children', () => {
    const child = <span>child element</span>;
    const deleteComponent = shallow(<Delete>{child}</Delete>);
    expect(deleteComponent.contains(child)).toBeTruthy();
});

it('renders the element requested by props', () => {
    const deleteLink = shallow(<Delete as={'button'}/>);
    expect(deleteLink.find('button.delete').exists()).toBeTruthy();
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Delete, size) );
});