import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor, sizeClassFor} from "../../helpers/util";

const Delete = ({ className, as, size,...props }) => {
    const Element = htmlElementFor(as, 'a');
    const classes = classNames(className, 'delete', sizeClassFor(size));
    return <Element className={classes} {...props} />;
};

Delete.displayName = 'Delete';

Delete.propTypes = {
    className: PropTypes.string
};

export default Delete;