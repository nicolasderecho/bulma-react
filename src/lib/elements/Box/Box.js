import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Box = ({ className, ...props }) => {
    const classes = classNames(className, 'box');
    return <div className={classes} {...props} />;
};

Box.displayName = 'Box';

Box.propTypes = {
    className: PropTypes.string
};

export default Box;