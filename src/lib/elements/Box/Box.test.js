import Box from './Box';
import {expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithRequestedClass} from "../../../testing/testHelpers";

it('renders a div with class box', () => {
    expectToRenderComponentWithElement(Box, 'div', 'box' );
});

it('passes classes to the box', () => {
    expectToRenderComponentWithRequestedClass(Box, 'div.box');
});

it('renders the box\'s children', () => {
    expectToRenderComponentWithChildren(Box);
});