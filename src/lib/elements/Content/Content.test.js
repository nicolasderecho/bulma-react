import React from 'react';
import { shallow } from 'enzyme';
import Content from './Content';
import {expectToRenderComponentWithSize} from "../../../testing/testHelpers";
import {SIZES} from "../../helpers/util";

it('renders an element with class content', () => {
    const content = shallow(<Content/>);
    expect(content.find('.content').exists()).toBeTruthy();
});

it('passes classes to the content', () => {
    const className = 'example';
    const content = shallow(<Content className={className}/>);
    expect(content.find(`.content.${className}`).exists()).toBeTruthy();
});

it('renders a div by default', () => {
    const content = shallow(<Content/>);
    expect(content.find('div.content').exists()).toBeTruthy();
});

it('renders the requested element by props', () => {
    const content = shallow(<Content as={'p'}/>);
    expect(content.find('p.content').exists()).toBeTruthy();
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Content, size) );
});

it('renders the content\'s children', () => {
    const child = <span>element inside content</span>;
    const content = shallow(<Content>{child}</Content>);
    expect(content.contains(child)).toBeTruthy();
});