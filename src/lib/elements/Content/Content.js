import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor, sizeClassFor} from "../../helpers/util";

const Content = ({ className, as, size, ...props }) => {
    const Element = htmlElementFor(as, 'div');
    const classes = classNames(className, 'content', sizeClassFor(size));
    return <Element className={classes} {...props} />;
};

Content.displayName = 'Content';

Content.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default Content;