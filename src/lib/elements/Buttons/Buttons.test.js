import React from 'react';
import { shallow } from 'enzyme';
import Buttons from './Buttons';
import {expectToRenderComponentWithPluralSize} from "../../../testing/testHelpers";
import {SIZES, BUTTON_POSITIONS} from "../../helpers/util";

it('renders a div with class buttons', () => {
  const box = shallow(<Buttons/>);
  expect(box.find('div.buttons').exists()).toBeTruthy();
});

it('doesn\'t allow invalid sizes', () => {
  const wrapper = () => { shallow(<Buttons size={'amazing'} />) }
  expect(wrapper).toThrow();
});

it('renders a buttons group with the proper size', () => {
  SIZES.forEach( (size) => expectToRenderComponentWithPluralSize( Buttons, size) );
});

it('renders a buttons group with the proper position', () => {
  BUTTON_POSITIONS.forEach( (position) => {
    const element = shallow(<Buttons position={position}/>);
    expect(element.find(`.is-${position}`).exists()).toBeTruthy();
  });
});

it('renders a buttons group with class has-addons when the flag is enabled', () => {
  const buttonsWithAddons = shallow(<Buttons addons/>);
  const buttonsWithoutAddons = shallow(<Buttons addons={false}/>);
  expect(buttonsWithAddons.find('.has-addons').exists()).toBeTruthy();
  expect(buttonsWithoutAddons.find('.has-addons').exists()).toBeFalsy();
});