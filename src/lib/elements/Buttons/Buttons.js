import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {SIZES, pluralSizeClassFor, buttonPositionClassFor, BUTTON_POSITIONS, addonsClassFor, propsWithoutKeys} from "../../helpers/util";

const Buttons = ({ className, size, position, ...props }) => {
  const classes = classNames(className, 'buttons', pluralSizeClassFor(size), buttonPositionClassFor(position), addonsClassFor(props));
  const filteredProps = propsWithoutKeys(props, ['addons']);
  return <div className={classes} {...filteredProps} />;
};

Buttons.displayName = 'Buttons';

Buttons.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(SIZES),
  position: PropTypes.oneOf(BUTTON_POSITIONS)
};

export default Buttons;