import React from 'react';
import { shallow } from 'enzyme';
import TableHead from './TableHead';

it('renders a thead', () => {
  const thead = shallow(<TableHead/>);
  expect(thead.find('thead').exists()).toBeTruthy();
});

it('passes classes to the thead', () => {
  const className = 'example';
  const thead = shallow(<TableHead className={className}/>);
  expect(thead.find(`thead.${className}`).exists()).toBeTruthy();
});

it('renders the thead\'s children', () => {
  const child = <span>child element</span>;
  const thead = shallow(<TableHead>{child}</TableHead>);
  expect(thead.contains(child)).toBeTruthy();
});