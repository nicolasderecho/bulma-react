import React from 'react';
import { shallow } from 'enzyme';
import IconWrapper from './IconWrapper';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement, expectToRenderComponentWithRequestedClass,
    expectToRenderComponentWithSize,
    expectToRenderComponentWithTextClass
} from "../../../testing/testHelpers";
import {COLOR_TYPES, ICON_POSITIONS, SIZES} from "../../helpers/util";

it('renders an icon wrapper', () => {
    expectToRenderComponentWithElement(IconWrapper, 'span', 'icon');
});

it('passes classes to the element', () => {
    expectToRenderComponentWithRequestedClass(IconWrapper, 'span');
});

it('renders the element\'s children', () => {
    expectToRenderComponentWithChildren(IconWrapper);
});

it('renders the element requested by props', () => {
    const iconWrapper = shallow(<IconWrapper as={'div'}/>);
    expect(iconWrapper.find('div.icon').exists()).toBeTruthy();
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(IconWrapper, size) );
});

it('adds the text class requested by props', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithTextClass(IconWrapper, color) );
});

it('renders the component with the requested position', () => {
    ICON_POSITIONS.forEach((position) => {
        const element = shallow(<IconWrapper position={position}/>);
        expect(element.props()).not.toHaveProperty('position');
        expect(element.find(`.is-${position}`).exists()).toBeTruthy();
    });
});