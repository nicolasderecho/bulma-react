import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    htmlElementFor,
    sizeClassFor,
    textClassFor,
    ICON_POSITIONS, SIZES, iconPositionClassFor
} from "../../helpers/util";

const IconWrapper = ({ className, as, size, hasText, position, ...props }) => {
    const Element = htmlElementFor(as, 'span');
    const classes = classNames(className, 'icon', sizeClassFor(size), textClassFor(hasText), iconPositionClassFor(position));
    return <Element className={classes} {...props} />;
};

IconWrapper.displayName = 'IconWrapper';

IconWrapper.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string,
    size: PropTypes.oneOf(SIZES),
    position: PropTypes.oneOf(ICON_POSITIONS)
};

export default IconWrapper;