import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableCellHeader = ({ className, ...props }) => {
  return <th className={classNames(className)} {...props} />;
};

TableCellHeader.displayName = 'TableCellHeader';

TableCellHeader.propTypes = {
  className: PropTypes.string
};

export default TableCellHeader;