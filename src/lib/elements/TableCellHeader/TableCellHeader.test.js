import React from 'react';
import { shallow } from 'enzyme';
import TableCellHeader from './TableCellHeader';

it('renders a th', () => {
  const th = shallow(<TableCellHeader/>);
  expect(th.find('th').exists()).toBeTruthy();
});

it('passes classes to the th', () => {
  const className = 'example';
  const th = shallow(<TableCellHeader className={className}/>);
  expect(th.find(`th.${className}`).exists()).toBeTruthy();
});

it('renders the th\'s children', () => {
  const child = <span>child element</span>;
  const th = shallow(<TableCellHeader>{child}</TableCellHeader>);
  expect(th.contains(child)).toBeTruthy();
});