import React from 'react';
import PropTypes from 'prop-types';

const ListItem = (props) => {
    return <li {...props} />;
};

ListItem.displayName = 'ListItem';

ListItem.propTypes = {
    className: PropTypes.string
};

export default ListItem;