import React from 'react';
import { shallow } from 'enzyme';
import ListItem from './ListItem';

it('renders a li', () => {
    const item = shallow(<ListItem/>);
    expect(item.find('li').exists()).toBeTruthy();
});

it('passes classes to the box', () => {
    const className = 'example';
    const item = shallow(<ListItem className={className}/>);
    expect(item.find(`li.${className}`).exists()).toBeTruthy();
});

it('renders the box\'s children', () => {
    const child = <span>element inside li</span>;
    const item = shallow(<ListItem>{child}</ListItem>);
    expect(item.contains(child)).toBeTruthy();
});