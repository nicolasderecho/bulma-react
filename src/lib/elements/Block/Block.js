import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const Block = ({ className, as, ...props }) => {
    const Element = htmlElementFor(as, 'div');
    const classes = classNames(className, 'block');
    return <Element className={classes} {...props} />;
};

Block.displayName = 'Block';

Block.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string
};

export default Block;