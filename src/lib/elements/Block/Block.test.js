import Block from './Block';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithRequestedClass
} from "../../../testing/testHelpers";

it('renders a div with class block', () => {
    expectToRenderComponentWithElement(Block, 'div', 'block' );
});

it('passes classes to the block', () => {
    expectToRenderComponentWithRequestedClass(Block, 'div.block');
});

it('renders the block\'s children', () => {
    expectToRenderComponentWithChildren(Block);
});