import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import TableCell from "../TableCell/TableCell";
import TableCellHeader from "../TableCellHeader/TableCellHeader";
import {isEnabled, propsWithoutKeys} from "../../helpers/util";

const TableRow = ({ className, ...props }) => {
  const classes = classNames(className, {'is-selected': isEnabled(props, 'selected')});
  const finalProps = propsWithoutKeys(props, ['selected']);
  return <tr className={classes} {...finalProps} />;
};

TableRow.displayName = 'TableRow';

TableRow.propTypes = {
  className: PropTypes.string
};

TableRow.Cell         = TableCell;
TableRow.CellHeader   = TableCellHeader;

export default TableRow;