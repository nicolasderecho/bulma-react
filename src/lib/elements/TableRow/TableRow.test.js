import React from 'react';
import { shallow } from 'enzyme';
import TableRow from './TableRow';

it('renders a tr', () => {
  const tr = shallow(<TableRow/>);
  expect(tr.find('tr').exists()).toBeTruthy();
});

it('passes classes to the tr', () => {
  const className = 'example';
  const tr = shallow(<TableRow className={className}/>);
  expect(tr.find(`tr.${className}`).exists()).toBeTruthy();
});

it('renders the tr\'s children', () => {
  const child = <span>child element</span>;
  const tr = shallow(<TableRow>{child}</TableRow>);
  expect(tr.contains(child)).toBeTruthy();
});

it('renders a selected tr when requested', () => {
  const matchedTable = shallow(<TableRow selected />);
  const notMatchedTable = shallow(<TableRow selected={false} />);
  expect(matchedTable.props()).not.toHaveProperty('selected');
  expect(matchedTable.find('.is-selected').exists()).toBeTruthy();
  expect(notMatchedTable.find('.is-selected').exists()).toBeFalsy();
});