import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {isEnabled, isNotNull, isDefined, propsWithoutKeys} from "../../helpers/util";
import ListItem from "../ListItem/ListItem";

const elementFor = (attributes) => {
  if(isNotNull(attributes.as)) {
    return attributes.as;
  }
  else if (isEnabled(attributes, 'unordered')) {
    return 'ul';
  }
  else if(isEnabled(attributes, 'ordered')) {
    return 'ol';
  }
  else if(isEnabled(attributes, 'description')) {
    return 'dl';
  }
  else {
    return 'ul';
  }
};

const ALLOWED_MARKERS = ['lower-alpha', 'lower-roman', 'upper-alpha', 'upper-roman'];

const List = ({ className, marker, ...props }) => {
    const classes = classNames(className, { [`is-${marker}`]: isNotNull(marker) });
    const Element = elementFor(props);
    const finalProps = propsWithoutKeys(props, ['ordered', 'unordered', 'description', 'as']);
    return <Element className={classes} {...finalProps} />;
};

List.displayName = 'List';
List.Item = ListItem;

const moreThanOneExists = (attributes, key, ...extraKeys) => isDefined(attributes, key) && extraKeys.some((anotherKey) => isDefined(attributes, anotherKey));

const validateFieldsAreMutuallyExclusive = (attribues, key, extraKeys) => {
    if (moreThanOneExists.apply(moreThanOneExists, [attribues, key, ...extraKeys])) {
        return new Error('List only support one possible type (ordered, unordered or description). List also requires to choose between \'as\' prop or any of the list types. So if you introduced both props, you will have to remove one.');
    }
};

const mutuallyExclusiveKeysExcept = (keyToRemove) => {
    return ['as', 'description', 'ordered', 'unordered'].filter(key => key !== keyToRemove);
};

const mutuallyExclusiveValidation = (props, propName) => {
    return validateFieldsAreMutuallyExclusive(props, propName, mutuallyExclusiveKeysExcept(propName));
};

List.propTypes = {
    className: PropTypes.string,
    marker: PropTypes.oneOf(ALLOWED_MARKERS),
    ordered: mutuallyExclusiveValidation,
    unordered: mutuallyExclusiveValidation,
    description: mutuallyExclusiveValidation,
    as: PropTypes.oneOf(['ul', 'ol', 'dl'])
};

export default List;