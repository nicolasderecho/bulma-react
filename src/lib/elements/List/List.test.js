import React from 'react';
import { shallow } from 'enzyme';
import List from './List';

it('renders an unordered list by default', () => {
    const element = shallow(<List/>);
    expect(element.find('ul').exists()).toBeTruthy();
});

it('renders an unordered list when it\'s requested by props', () => {
    const element = shallow(<List unordered/>);
    expect(element.find('ul').exists()).toBeTruthy();
});

it('renders an ordered list when it\'s requested by props', () => {
    const element = shallow(<List ordered/>);
    expect(element.find('ol').exists()).toBeTruthy();
});

it('renders a description list when it\'s requested by props', () => {
    const element = shallow(<List description/>);
    expect(element.find('dl').exists()).toBeTruthy();
});

it('passes classes to the list', () => {
    const className = 'example';
    const element = shallow(<List className={className}/>);
    expect(element.find(`.${className}`).exists()).toBeTruthy();
});

const expectToRenderListWithMarker = (marker) => {
    const element = shallow(<List ordered marker={marker} />);
    expect(element.find(`ol.is-${marker}`).exists()).toBeTruthy();
};

it('allows to set different item markers when the list is ordered', () => {
    expectToRenderListWithMarker('lower-alpha');
    expectToRenderListWithMarker('lower-roman');
    expectToRenderListWithMarker('upper-alpha');
    expectToRenderListWithMarker('upper-roman');
});

it('doesn\'t allow invalid markers', () => {
   expect(() => shallow(<List ordered marker={'alpha-romeo'}/>)).toThrow();
});

it('doesn\'t allow multiple list types', () => {
    expect(() => shallow(<List ordered description unordered/>)).toThrow();
});

it('doesn\'t allow list type and \'as\' prop', () => {
    expect(() => shallow(<List ordered as/>)).toThrow();
});

it('doesn\'t allow an invalid \'as\' prop', () => {
    expect(() => shallow(<List as={'span'}/>)).toThrow();
});

it('renders the proper list requested by props', () => {
    const list = shallow(<List as={'ol'} />);
    expect(list.find('ol').exists()).toBeTruthy();
});

it('renders the box\'s children', () => {
    const child = <span>Child element</span>;
    const box = shallow(<List>{child}</List>);
    expect(box.contains(child)).toBeTruthy();
});