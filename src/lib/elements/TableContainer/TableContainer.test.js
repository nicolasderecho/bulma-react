import React from 'react';
import TableContainer from './TableContainer';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a div with class table-container', () => {
    expectToRenderComponentWithElement(TableContainer, 'div', 'table-container');
});

it('renders the TableContainer\'s children', () => {
    expectToRenderComponentWithChildren(TableContainer);
});

it('renders the element requested by props', () => {
    const element = shallow(<TableContainer as={'span'}/>);
    expect(element.find('span.table-container').exists()).toBeTruthy();
});
