import React from 'react';
import { shallow } from 'enzyme';
import ProgressBar from './ProgressBar';
import {
    expectToRenderComponentWithSize,
    expectToRenderComponentWithColor
} from "../../../testing/testHelpers";
import {COLOR_TYPES, SIZES} from "../../helpers/util";

it('renders a progress bar', () => {
    const progressBar = shallow(<ProgressBar/>);
    expect(progressBar.find('progress.progress').exists()).toBeTruthy();
});

it('passes classes to the progress bar', () => {
    const className = 'example';
    const progressBar = shallow(<ProgressBar className={className}/>);
    expect(progressBar.find(`progress.progress.${className}`).exists()).toBeTruthy();
});

it('renders the progress bar\'s children', () => {
    const child = <span>child element</span>;
    const progressBar = shallow(<ProgressBar>{child}</ProgressBar>);
    expect(progressBar.contains(child)).toBeTruthy();
});

it('renders the progress bar with the proper colors', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(ProgressBar, color) );
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(ProgressBar, size) );
});

it('renders an indeterminated progress bar(without value)', () => {
   const progressBar = shallow(<ProgressBar value={'10'} indeterminated />);
   expect(progressBar).not.toHaveProp('value');
});