import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {COLOR_TYPES, sizeClassFor, isEnabled, colorClassFor, propsWithoutKeys} from "../../helpers/util";

const generateFinalProps = (props) => {
    const keysToFilter = isEnabled(props, 'indeterminated') ? ['indeterminated', 'value'] : ['indeterminated'] ;
    return propsWithoutKeys(props, keysToFilter);
};

const ProgressBar = ({ className, color, size, ...props }) => {
    const classes = classNames(className, 'progress', sizeClassFor(size), colorClassFor(color));
    const finalProps = generateFinalProps(props);
    return <progress className={classes} {...finalProps} />;
};

ProgressBar.displayName = 'ProgressBar';

ProgressBar.propTypes = {
    className: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES),
    value: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
    max: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ])
};

export default ProgressBar;