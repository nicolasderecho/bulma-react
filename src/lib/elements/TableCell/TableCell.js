import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {COLOR_TYPES, colorClassFor} from "../../helpers/util";

const TableCell = ({ className, color, ...props }) => {
  const classes = classNames(className, colorClassFor(color));
  return <td className={classes} {...props} />;
};

TableCell.displayName = 'TableCell';

TableCell.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(COLOR_TYPES),
};

export default TableCell;