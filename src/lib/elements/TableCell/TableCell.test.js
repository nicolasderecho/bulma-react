import React from 'react';
import { shallow } from 'enzyme';
import TableCell from './TableCell';
import {COLOR_TYPES} from "../../helpers/util";
import {expectToRenderComponentWithColor} from "../../../testing/testHelpers";

it('renders a td', () => {
  const td = shallow(<TableCell/>);
  expect(td.find('td').exists()).toBeTruthy();
});

it('passes classes to the td', () => {
  const className = 'example';
  const td = shallow(<TableCell className={className}/>);
  expect(td.find(`td.${className}`).exists()).toBeTruthy();
});

it('renders the td\'s children', () => {
  const child = <span>child element</span>;
  const td = shallow(<TableCell>{child}</TableCell>);
  expect(td.contains(child)).toBeTruthy();
});

it('adds the requested color by props', () => {
  COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(TableCell, color) );
});