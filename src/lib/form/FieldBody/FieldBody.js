import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor, sizeClassFor, SIZES} from "../../helpers/util";

const FieldBody = ({ className,  as, size, ...props }) => {
    const classes = classNames(className, 'field-body', sizeClassFor(size));
    const Element = htmlElementFor(as, 'div');
    return <Element className={classes} {...props} />;
};

FieldBody.displayName = 'FieldBody';

FieldBody.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(SIZES)
};

export default FieldBody;