import React from 'react';
import FieldBody from './FieldBody';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithSize } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";
import {SIZES} from "../../helpers/util";

it('renders a div with class field-body', () => {
    expectToRenderComponentWithElement(FieldBody, 'div', 'field-body');
});

it('renders an specific dom element when requested', () => {
    const component = shallow(<FieldBody as={'span'}/>);
    expect(component.find('span.field-body').exists()).toBeTruthy();
});

it('renders the FieldBody\'s children', () => {
    expectToRenderComponentWithChildren(FieldBody);
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(FieldBody, size) );
});