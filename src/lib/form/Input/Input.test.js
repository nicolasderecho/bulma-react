import Input from './Input';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithColor,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty, expectToRenderComponentWithSize, expectToRenderComponentWithState
} from "../../../testing/testHelpers";
import {COLOR_TYPES, SIZES, STATES} from "../../helpers/util";

it('renders an input with class input', () => {
    expectToRenderComponentWithElement(Input, 'input', 'input');
});

it('renders the Input\'s children', () => {
    expectToRenderComponentWithChildren(Input);
});

it('renders the input with the proper styles', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Input, color) );
});

it('renders the component expanded when requested', () => {
    expectToRenderComponentWithProperty(Input, 'expanded');
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Input, size) );
});

it('renders a rounded element when requested', () => {
   expectToRenderComponentWithProperty(Input, 'rounded');
});

it('adds the requested state by props', () => {
    STATES.forEach((state) => expectToRenderComponentWithState(Input, state) );
});