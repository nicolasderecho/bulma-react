import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { checkProperty, colorClassFor, propsWithoutKeys, sizeClassFor, stateClassFor, COLOR_TYPES, SIZES, STATES } from "../../helpers/util";

const Input = ({ className, color, size, state, ...props }) => {
    const classes = classNames(className, 'input', colorClassFor(color), checkProperty(props, 'expanded'), sizeClassFor(size), checkProperty(props, 'rounded'), stateClassFor(state));
    const finalProps = propsWithoutKeys(props, ['expanded', 'rounded']);
    return <input className={classes} {...finalProps} />;
};

Input.displayName = 'Input';

Input.propTypes = {
    className: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES),
    size: PropTypes.oneOf(SIZES),
    state: PropTypes.oneOf(STATES)
};

export default Input;