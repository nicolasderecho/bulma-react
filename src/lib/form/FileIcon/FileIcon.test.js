import React from 'react';
import FileIcon from './FileIcon';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a span with class file-icon', () => {
  expectToRenderComponentWithElement(FileIcon, 'span', 'file-icon');
});

it('renders the FileIcon\'s children', () => {
  expectToRenderComponentWithChildren(FileIcon);
});