import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const FileIcon = ({ className, ...props }) => {
  const classes = classNames(className, 'file-icon');
  return <span className={classes} {...props} />;
};

FileIcon.displayName = 'FileIcon';

FileIcon.propTypes = {
  className: PropTypes.string
};

export default FileIcon;