import React from 'react';
import Radio from './Radio';
import {expectToRenderComponentWithChildren} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a radio wrapped inside a label', () => {
    const child = 'Yes';
    const component = shallow(<Radio name={'question'}>{child}</Radio>);
    expect(component.find('label.radio input.radio').exists()).toBeTruthy();
});

it('renders the Radio\'s children', () => {
    expectToRenderComponentWithChildren(Radio);
});

it('passes classes to the wrapper element', () => {
    const component = shallow(<Radio wrapperClassName={'wrapper'} />);
    expect(component.find('label.radio.wrapper').exists()).toBeTruthy();
    expect(component.props()).not.toHaveProperty('wrapperClassName');
});

