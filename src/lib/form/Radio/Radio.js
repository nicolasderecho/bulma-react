import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Radio = ({ className, children, wrapperClassName, ...props }) => {
    const classes = classNames(className, 'radio');
    const wrapperClasses = classNames(wrapperClassName, 'radio');
    return <label className={wrapperClasses}>
        <input type={'radio'} className={classes} {...props} />
        {children}
    </label>
};

Radio.displayName = 'Radio';

Radio.propTypes = {
    className: PropTypes.string
};

export default Radio;