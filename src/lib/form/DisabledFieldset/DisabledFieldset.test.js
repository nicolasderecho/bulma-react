import React from 'react';
import DisabledFieldset from './DisabledFieldset';
import { expectToRenderComponentWithChildren} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a disabled fieldset', () => {
    const component = shallow(<DisabledFieldset/>);
    expect(component.find(`fieldset`).exists()).toBeTruthy();
    expect(component.find('fieldset').props()["disabled"]).toBe(true);
});

it('renders the element\'s children', () => {
    expectToRenderComponentWithChildren(DisabledFieldset);
});
