import React from 'react';

const DisabledFieldset = (props) => {
    return <fieldset {...props} disabled/>;
};

DisabledFieldset.displayName = 'DisabledFieldset';

export default DisabledFieldset;