import React from 'react';
import FileCta from './FileCta';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a span with class file-cta', () => {
  expectToRenderComponentWithElement(FileCta, 'span', 'file-cta');
});

it('renders the FileCta\'s children', () => {
  expectToRenderComponentWithChildren(FileCta);
});