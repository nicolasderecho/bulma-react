import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const FileCta = ({ className, ...props }) => {
  const classes = classNames(className, 'file-cta');
  return <span className={classes} {...props} />;
};

FileCta.displayName = 'FileCta';

FileCta.propTypes = {
  className: PropTypes.string
};

export default FileCta;