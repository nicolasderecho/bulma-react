import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const FileName = ({ className, ...props }) => {
  const classes = classNames(className, 'file-name');
  return <span className={classes} {...props} />;
};

FileName.displayName = 'FileName';

FileName.propTypes = {
  className: PropTypes.string
};

export default FileName;