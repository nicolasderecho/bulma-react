import React from 'react';
import FileName from './FileName';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a span with class file-name', () => {
  expectToRenderComponentWithElement(FileName, 'span', 'file-name');
});

it('renders the FileName\'s children', () => {
  expectToRenderComponentWithChildren(FileName);
});