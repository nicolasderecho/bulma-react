import React from 'react';
import Field from './Field';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty, expectToRenderComponentWithAlignment
} from "../../../testing/testHelpers";
import {ALIGNMENTS} from "../../helpers/util";
import {shallow} from "enzyme/build";

it('renders a div with class field', () => {
    expectToRenderComponentWithElement(Field, 'div', 'field');
});

it('renders a div with class is-grouped when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Field, 'grouped');
});

it('renders a div with class is-grouped-centered when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Field, 'groupedCentered');
});

it('renders a div with class is-grouped-right when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Field, 'groupedRight');
});

it('renders a div with class is-grouped-multiline when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Field, 'groupedMultiline');
});

it('renders a div with class is-horizontal when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Field, 'horizontal');
});

it('renders a div with class has-addons when the flag is enabled', () => {
    expectToRenderComponentWithProperty(Field, 'addons', {prefix: 'has'});
});

it('renders the Field\'s children', () => {
    expectToRenderComponentWithChildren(Field);
});

it('renders the component with the requested alignment', () => {
    ALIGNMENTS.forEach((alignment) => expectToRenderComponentWithAlignment(Field, alignment, { prefix: 'has-addons' }) );
});