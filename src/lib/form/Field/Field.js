import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  ALIGNMENTS,
  addonsClassFor, alignmentClassFor,
  checkProperty,
  groupedClassFor,
  isEnabled,
  propsWithoutKeys
} from "../../helpers/util";
import FieldLabel from "../FieldLabel/FieldLabel";
import FieldBody from "../FieldBody/FieldBody";

const Field = ({ className, alignment, ...props }) => {
  const classes = classNames(className, 'field', groupedClassFor(props), addonsClassFor(props), {'is-grouped-centered': isEnabled(props, 'groupedCentered')}, {'is-grouped-right': isEnabled(props, 'groupedRight')}, {'is-grouped-multiline': isEnabled(props, 'groupedMultiline')}, checkProperty(props, 'horizontal'), alignmentClassFor(alignment, {prefix: 'has-addons'}));
  const finalProps = propsWithoutKeys(props, ['grouped', 'addons', 'groupedCentered', 'groupedRight', 'groupedMultiline', 'horizontal']);
  return <div className={classes} {...finalProps} />;
};

Field.displayName = 'Field';

Field.propTypes = {
  className: PropTypes.string,
  alignment: PropTypes.oneOf(ALIGNMENTS)
};

Field.Label = FieldLabel;
Field.Body  = FieldBody;

export default Field;