import React from 'react';
import Label from './Label';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithSize
} from "../../../testing/testHelpers";
import {SIZES} from "../../helpers/util";

it('renders a label with class label', () => {
    expectToRenderComponentWithElement(Label, 'label', 'label');
});

it('renders the Label\'s children', () => {
    expectToRenderComponentWithChildren(Label);
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Label, size) );
});