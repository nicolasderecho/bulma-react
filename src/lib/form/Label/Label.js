import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {sizeClassFor, SIZES} from "../../helpers/util";

const Label = ({ className, size, ...props }) => {
    const classes = classNames(className, 'label', sizeClassFor(size));
    return <label className={classes} {...props} />;
};

Label.displayName = 'Label';

Label.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(SIZES)
};

export default Label;