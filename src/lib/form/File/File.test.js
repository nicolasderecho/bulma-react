import React from 'react';
import File from './File';
import {
  expectToRenderComponentWithAlignment,
  expectToRenderComponentWithChildren, expectToRenderComponentWithColor,
  expectToRenderComponentWithElement, expectToRenderComponentWithProperty,
  expectToRenderComponentWithSize,
} from "../../../testing/testHelpers";
import {ALIGNMENTS, COLOR_TYPES, SIZES} from "../../helpers/util";

it('renders a div with class file', () => {
  expectToRenderComponentWithElement(File, 'div', 'file');
});

it('renders the File\'s children', () => {
  expectToRenderComponentWithChildren(File);
});

it('adds the requested size by props', () => {
  SIZES.forEach((size) => expectToRenderComponentWithSize(File, size) );
});

it('adds the requested color by props', () => {
  COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(File, color) );
});

it('adds the has-name class when requested', () => {
  expectToRenderComponentWithProperty(File, 'hasName', {prefix: ''});
});

it('adds the fullwidth class when requested', () => {
  expectToRenderComponentWithProperty(File, 'fullwidth');
});

it('adds the boxed class when requested', () => {
  expectToRenderComponentWithProperty(File, 'boxed');
});

it('renders the component with the requested alignment', () => {
  ALIGNMENTS.forEach((alignment) => expectToRenderComponentWithAlignment(File, alignment) );
});