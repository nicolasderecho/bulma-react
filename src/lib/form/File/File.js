import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import FileName from "../FileName/FileName";
import FileLabel from "../FileLabel/FileLabel";
import FileInput from "../FileInput/FileInput";
import FileCta from "../FileCta/FileCta";
import FileIcon from "../FileIcon/FileIcon";
import {
  checkProperties, checkProperty, colorClassFor, propsWithoutKeys, sizeClassFor,
  COLOR_TYPES, SIZES, ALIGNMENTS, alignmentClassFor
} from "../../helpers/util";

const EXTRA_PROPERTIES = ['fullwidth', 'boxed'];
const PROPERTIES_TO_EXCLUDE = EXTRA_PROPERTIES.concat('hasName');
const File = ({ className, size, color, alignment, ...props }) => {
  const classes = classNames(className, 'file', sizeClassFor(size), colorClassFor(color), checkProperty(props, 'hasName', {prefix: ''}), checkProperties(props, EXTRA_PROPERTIES), alignmentClassFor(alignment));
  const finalProps = propsWithoutKeys(props, PROPERTIES_TO_EXCLUDE);
  return <div className={classes} {...finalProps} />;
};

File.displayName = 'File';

File.Name  = FileName;
File.Label = FileLabel;
File.Input = FileInput;
File.Cta   = FileCta;
File.Icon  = FileIcon;

File.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(SIZES),
  color: PropTypes.oneOf(COLOR_TYPES),
  alignment: PropTypes.oneOf(ALIGNMENTS)
};

export default File;