import React from 'react';
import FileInput from './FileInput';
import { expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders an input with class file-input', () => {
  expectToRenderComponentWithElement(FileInput, 'input', 'file-input');
});