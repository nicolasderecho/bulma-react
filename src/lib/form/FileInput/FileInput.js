import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const FileInput = ({ className, ...props }) => {
  const classes = classNames(className, 'file-input');
  return <input className={classes} {...props} />;
};

FileInput.displayName = 'FileInput';

FileInput.propTypes = {
  className: PropTypes.string
};

export default FileInput;