import React from 'react';
import Help from './Help';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithColor, expectToRenderComponentWithElement } from "../../../testing/testHelpers";
import {COLOR_TYPES} from "../../helpers/util";

it('renders a div with class control', () => {
    expectToRenderComponentWithElement(Help, 'p', 'help');
});

it('renders the Help\'s children', () => {
    expectToRenderComponentWithChildren(Help);
});

it('renders the notification with the proper styles', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Help, color) );
});
