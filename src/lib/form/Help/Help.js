import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {COLOR_TYPES, colorClassFor} from "../../helpers/util";

const Help = ({ className, color, ...props }) => {
    const classes = classNames(className, 'help', colorClassFor(color));
    return <p className={classes} {...props} />;
};

Help.displayName = 'Help';

Help.propTypes = {
    className: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES)
};

export default Help;