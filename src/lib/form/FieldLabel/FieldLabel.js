import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor, sizeClassFor, SIZES} from "../../helpers/util";

const FieldLabel = ({ className,  as, size, ...props }) => {
    const classes = classNames(className, 'field-label', sizeClassFor(size));
    const Element = htmlElementFor(as, 'div');
    return <Element className={classes} {...props} />;
};

FieldLabel.displayName = 'FieldLabel';

FieldLabel.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(SIZES)
};

export default FieldLabel;