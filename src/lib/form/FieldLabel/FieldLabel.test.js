import React from 'react';
import FieldLabel from './FieldLabel';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithSize } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";
import {SIZES} from "../../helpers/util";

it('renders a div with class field-label', () => {
    expectToRenderComponentWithElement(FieldLabel, 'div', 'field-label');
});

it('renders an specific dom element when requested', () => {
    const component = shallow(<FieldLabel as={'span'}/>);
    expect(component.find('span.field-label').exists()).toBeTruthy();
});

it('renders the FieldLabel\'s children', () => {
    expectToRenderComponentWithChildren(FieldLabel);
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(FieldLabel, size) );
});