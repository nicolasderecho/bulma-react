import React from 'react';
import FileLabel from './FileLabel';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a label with class file-label', () => {
  expectToRenderComponentWithElement(FileLabel, 'label', 'file-label');
});

it('renders the FileLabel\'s children', () => {
  expectToRenderComponentWithChildren(FileLabel);
});

it('renders an specific dom element when requested', () => {
  const component = shallow(<FileLabel as={'span'}/>);
  expect(component.find('span.file-label').exists()).toBeTruthy();
});
