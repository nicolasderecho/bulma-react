import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const FileLabel = ({ className, as, ...props }) => {
  const Element = htmlElementFor(as, 'label');
  const classes = classNames(className, 'file-label');
  return <Element className={classes} {...props} />;
};

FileLabel.displayName = 'FileLabel';

FileLabel.propTypes = {
  className: PropTypes.string
};

export default FileLabel;