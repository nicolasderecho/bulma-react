import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Checkbox = ({ className, children, wrapperClassName, ...props }) => {
    const classes = classNames(className, 'checkbox');
    const wrapperClasses = classNames(wrapperClassName, 'checkbox');
    return <label className={wrapperClasses}>
        <input type={'checkbox'} className={classes} {...props} />
        {children}
    </label>
};

Checkbox.displayName = 'Checkbox';

Checkbox.propTypes = {
    className: PropTypes.string
};

export default Checkbox;