import React from 'react';
import Checkbox from './Checkbox';
import {expectToRenderComponentWithChildren, expectToRenderComponentWithElement} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a checkbox wrapped inside a label', () => {
    expectToRenderComponentWithElement(Checkbox, 'label.checkbox input', 'checkbox');
});

it('renders the Checkbox\'s children', () => {
    expectToRenderComponentWithChildren(Checkbox);
});

it('passes classes to the wrapper element', () => {
    const component = shallow(<Checkbox wrapperClassName={'wrapper'} />);
    expect(component.find('label.checkbox.wrapper').exists()).toBeTruthy();
    expect(component.props()).not.toHaveProperty('wrapperClassName');
});
