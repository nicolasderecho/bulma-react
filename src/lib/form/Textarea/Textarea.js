import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    checkProperty,
    COLOR_TYPES,
    colorClassFor, propsWithoutKeys,
    sizeClassFor,
    SIZES,
    stateClassFor,
    STATES
} from "../../helpers/util";

const Textarea = ({ className, color, size, state, ...props }) => {
    const classes = classNames(className, 'textarea', colorClassFor(color), sizeClassFor(size), stateClassFor(state), checkProperty(props, 'hasFixedSize', {prefix: ''}));
    const finalProps = propsWithoutKeys(props, ['hasFixedSize']);
    return <textarea className={classes} {...finalProps} />;
};

Textarea.displayName = 'Textarea';

Textarea.propTypes = {
    className: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES),
    size: PropTypes.oneOf(SIZES),
    state: PropTypes.oneOf(STATES)
};

export default Textarea;