import Textarea from './Textarea';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithColor,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty,
    expectToRenderComponentWithSize,
    expectToRenderComponentWithState
} from "../../../testing/testHelpers";
import {COLOR_TYPES, SIZES, STATES} from "../../helpers/util";

it('renders a textarea with class textarea', () => {
    expectToRenderComponentWithElement(Textarea, 'textarea', 'textarea');
});

it('renders the Textarea\'s children', () => {
    expectToRenderComponentWithChildren(Textarea);
});

it('renders the Textarea with the proper colors', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Textarea, color) );
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Textarea, size) );
});

it('adds the requested state by props', () => {
    STATES.forEach((state) => expectToRenderComponentWithState(Textarea, state) );
});

it('renders the has-fixed-size class when requested', () => {
    expectToRenderComponentWithProperty(Textarea, 'hasFixedSize', {prefix: ''});
});