import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperties, checkProperty, propsWithoutKeys} from "../../helpers/util";

const ICON_PROPERTIES = ['hasIconsLeft', 'hasIconsRight'];
const TOTAL_PROPERTIES = ICON_PROPERTIES.concat('expanded', 'loading');

const Control = ({ className, ...props }) => {
    const classes = classNames(className, 'control', checkProperty(props, 'expanded'), checkProperty(props, 'loading'), checkProperties(props, ICON_PROPERTIES, {prefix: ''}));
    const finalProps = propsWithoutKeys(props, TOTAL_PROPERTIES);
    return <div className={classes} {...finalProps} />;
};

Control.displayName = 'Control';

Control.propTypes = {
    className: PropTypes.string
};

export default Control;