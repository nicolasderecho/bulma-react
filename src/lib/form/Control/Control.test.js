import React from 'react';
import Control from './Control';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty
} from "../../../testing/testHelpers";

it('renders a div with class control', () => {
    expectToRenderComponentWithElement(Control, 'div', 'control');
});

it('renders the Control\'s children', () => {
    expectToRenderComponentWithChildren(Control);
});

it('renders the has-icons class when requested', () => {
    expectToRenderComponentWithProperty(Control, 'hasIconsLeft', {prefix: ''});
    expectToRenderComponentWithProperty(Control, 'hasIconsRight', {prefix: ''});
});

it('renders the component expanded when requested', () => {
    expectToRenderComponentWithProperty(Control, 'expanded');
});

it('renders a loading component when requested', () => {
    expectToRenderComponentWithProperty(Control, 'loading');
});