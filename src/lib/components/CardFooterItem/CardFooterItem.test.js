import React from 'react';
import CardFooterItem from './CardFooterItem';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a p with class card-footer-item', () => {
    expectToRenderComponentWithElement(CardFooterItem, 'p', 'card-footer-item');
});

it('renders the CardFooterItem\'s children', () => {
    expectToRenderComponentWithChildren(CardFooterItem);
});

it('renders the element requested by props', () => {
    const element = shallow(<CardFooterItem as={'a'} href={'#'}/>);
    expect(element.find('a.card-footer-item').exists()).toBeTruthy();
});