import React from 'react';
import CardHeaderIcon from './CardHeaderIcon';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a span with class card-header-icon', () => {
    expectToRenderComponentWithElement(CardHeaderIcon, 'span', 'card-header-icon');
});

it('renders the CardHeaderIcon\'s children', () => {
    expectToRenderComponentWithChildren(CardHeaderIcon);
});

it('renders the element requested by props', () => {
    const element = shallow(<CardHeaderIcon as={'a'} href={'#'}/>);
    expect(element.find('a.card-header-icon').exists()).toBeTruthy();
});