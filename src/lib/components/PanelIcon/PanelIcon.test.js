import React from 'react';
import PanelIcon from './PanelIcon';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a span with class panel-icon', () => {
    expectToRenderComponentWithElement(PanelIcon, 'span', 'panel-icon');
});

it('renders the PanelIcon\'s children', () => {
    expectToRenderComponentWithChildren(PanelIcon);
});