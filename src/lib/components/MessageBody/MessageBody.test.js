import React from 'react';
import MessageBody from './MessageBody';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class message-body', () => {
  expectToRenderComponentWithElement(MessageBody, 'div', 'message-body');
});

it('renders the MessageBody\'s children', () => {
  expectToRenderComponentWithChildren(MessageBody);
});