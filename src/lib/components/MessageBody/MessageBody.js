import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const MessageBody = ({ className, ...props }) => {
  const classes = classNames(className, 'message-body');
  return <div className={classes} {...props} />;
};

MessageBody.displayName = 'MessageBody';

MessageBody.propTypes = {
  className: PropTypes.string
};

export default MessageBody;