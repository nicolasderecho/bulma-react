import React from 'react';
import BreadcrumbItem from './BreadcrumbItem';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithProperty } from "../../../testing/testHelpers";

it('renders a li element', () => {
    expectToRenderComponentWithElement(BreadcrumbItem, 'li');
});

it('renders the BreadcrumbItem\'s children', () => {
    expectToRenderComponentWithChildren(BreadcrumbItem);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(BreadcrumbItem, 'active');
});