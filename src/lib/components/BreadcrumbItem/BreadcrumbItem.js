import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, propsWithoutKeys} from "../../helpers/util";

const BreadcrumbItem = ({ className, ...props }) => {
    const classes = classNames(className, checkProperty(props, 'active'));
    const finalProps = propsWithoutKeys(props, ['active']);
    return <li className={classes} {...finalProps} />;
};

BreadcrumbItem.displayName = 'BreadcrumbItem';

BreadcrumbItem.propTypes = {
    className: PropTypes.string
};

export default BreadcrumbItem;