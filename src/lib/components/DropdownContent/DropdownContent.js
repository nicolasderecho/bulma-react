import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const DropdownContent = ({ className, ...props }) => {
  const classes = classNames(className, 'dropdown-content');
  return <div className={classes} {...props} />;
};

DropdownContent.displayName = 'DropdownContent';

DropdownContent.propTypes = {
  className: PropTypes.string
};

export default DropdownContent;