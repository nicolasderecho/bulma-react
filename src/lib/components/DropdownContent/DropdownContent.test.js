import React from 'react';
import DropdownContent from './DropdownContent';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class dropdown-menu', () => {
  expectToRenderComponentWithElement(DropdownContent, 'div', 'dropdown-content');
});

it('renders the DropdownContent\'s children', () => {
  expectToRenderComponentWithChildren(DropdownContent);
});