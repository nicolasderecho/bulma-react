import React from 'react';
import Menu from './Menu';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders an aside with class menu', () => {
  expectToRenderComponentWithElement(Menu, 'aside', 'menu');
});

it('renders the Menu\'s children', () => {
  expectToRenderComponentWithChildren(Menu);
});