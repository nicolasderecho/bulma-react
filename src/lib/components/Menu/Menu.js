import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import MenuLabel from "../MenuLabel/MenuLabel";
import MenuList from "../MenuList/MenuList";

const Menu = ({ className, ...props }) => {
  const classes = classNames(className, 'menu');
  return <aside className={classes} {...props} />;
};

Menu.displayName = 'Menu';

Menu.Label = MenuLabel;
Menu.List  = MenuList;

Menu.propTypes = {
  className: PropTypes.string
};

export default Menu;