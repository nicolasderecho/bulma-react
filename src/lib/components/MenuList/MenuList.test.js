import React from 'react';
import MenuList from './MenuList';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders an ul with class menu-list', () => {
  expectToRenderComponentWithElement(MenuList, 'ul', 'menu-list');
});

it('renders the MenuList\'s children', () => {
  expectToRenderComponentWithChildren(MenuList);
});

it('renders the element requested by props', () => {
  const element = shallow(<MenuList as={'ol'} />);
  expect(element.find('ol.menu-list').exists()).toBeTruthy();
});