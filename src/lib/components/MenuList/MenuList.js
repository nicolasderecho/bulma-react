import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {htmlElementFor} from "../../helpers/util";

const MenuList = ({ className, as, ...props }) => {
  const Element = htmlElementFor(as, 'ul');
  const classes = classNames(className, 'menu-list');
  return <Element className={classes} {...props} />;
};

MenuList.displayName = 'MenuList';

MenuList.propTypes = {
  className: PropTypes.string
};

export default MenuList;