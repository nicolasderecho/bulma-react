import React from 'react';
import ModalClose from './ModalClose';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithSize } from "../../../testing/testHelpers";
import {SIZES} from "../../helpers/util";

it('renders a button with class modal-close', () => {
  expectToRenderComponentWithElement(ModalClose, 'button', 'modal-close');
});

it('renders the ModalClose\'s children', () => {
  expectToRenderComponentWithChildren(ModalClose);
});

it('adds the requested size by props', () => {
  SIZES.forEach((size) => expectToRenderComponentWithSize(ModalClose, size) );
});