import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {sizeClassFor, SIZES} from "../../helpers/util";

const ModalClose = ({ className, size, ...props }) => {
  const classes = classNames(className, 'modal-close', sizeClassFor(size));
  return <button className={classes} {...props} aria-label={'close'} />;
};

ModalClose.displayName = 'ModalClose';

ModalClose.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(SIZES)
};

export default ModalClose;