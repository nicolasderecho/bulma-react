import React from 'react';
import ModalCardBody from './ModalCardBody';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a section with class modal-card-body', () => {
  expectToRenderComponentWithElement(ModalCardBody, 'section', 'modal-card-body');
});

it('renders the ModalCardBody\'s children', () => {
  expectToRenderComponentWithChildren(ModalCardBody);
});