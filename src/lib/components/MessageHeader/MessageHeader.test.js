import React from 'react';
import MessageHeader from './MessageHeader';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class message-header', () => {
  expectToRenderComponentWithElement(MessageHeader, 'div', 'message-header');
});

it('renders the MessageHeader\'s children', () => {
  expectToRenderComponentWithChildren(MessageHeader);
});