import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const MessageHeader = ({ className, ...props }) => {
  const classes = classNames(className, 'message-header');
  return <div className={classes} {...props} />;
};

MessageHeader.displayName = 'MessageHeader';

MessageHeader.propTypes = {
  className: PropTypes.string
};

export default MessageHeader;