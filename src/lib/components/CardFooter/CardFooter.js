import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CardFooterItem from "../CardFooterItem/CardFooterItem";

const CardFooter = ({ className, ...props }) => {
    const classes = classNames(className, 'card-footer');
    return <footer className={classes} {...props} />;
};

CardFooter.displayName = 'CardFooter';

CardFooter.Item = CardFooterItem;

CardFooter.propTypes = {
    className: PropTypes.string
};

export default CardFooter;