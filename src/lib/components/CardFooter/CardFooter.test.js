import React from 'react';
import CardFooter from './CardFooter';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a footer with class card-footer', () => {
    expectToRenderComponentWithElement(CardFooter, 'footer', 'card-footer');
});

it('renders the CardFooter\'s children', () => {
    expectToRenderComponentWithChildren(CardFooter);
});