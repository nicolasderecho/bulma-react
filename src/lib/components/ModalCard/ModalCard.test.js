import React from 'react';
import ModalCard from './ModalCard';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class modal-card', () => {
  expectToRenderComponentWithElement(ModalCard, 'div', 'modal-card');
});

it('renders the ModalCard\'s children', () => {
  expectToRenderComponentWithChildren(ModalCard);
});