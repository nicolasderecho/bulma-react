import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ModalCardHead from "../ModalCardHead/ModalCardHead";
import ModalCardTitle from "../ModalCardTitle/ModalCardTitle";
import ModalCardBody from "../ModalCardBody/ModalCardBody";
import ModalCardFoot from "../ModalCardFoot/ModalCardFoot";

const ModalCard = ({ className, ...props }) => {
  const classes = classNames(className, 'modal-card');
  return <div className={classes} {...props} />;
};

ModalCard.displayName = 'ModalCard';

ModalCard.Head  = ModalCardHead;
ModalCard.Title = ModalCardTitle;
ModalCard.Body  = ModalCardBody;
ModalCard.Foot  = ModalCardFoot;

ModalCard.propTypes = {
  className: PropTypes.string
};

export default ModalCard;