import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, propsWithoutKeys} from "../../helpers/util";

const NavbarBurger = ({ className, ...props }) => {
    const classes = classNames(className, 'navbar-burger', 'burger', checkProperty(props, 'active'));
    const finalProps = propsWithoutKeys(props, ['active']);
    return <a className={classes} {...finalProps} role={'button'} aria-label={'menu'} aria-expanded={'false'}>
        <span aria-hidden={'true'} />
        <span aria-hidden={'true'} />
        <span aria-hidden={'true'} />
    </a>;
};

NavbarBurger.displayName = 'NavbarBurger';

NavbarBurger.propTypes = {
    className: PropTypes.string
};

export default NavbarBurger;