import React from 'react';
import NavbarBurger from './NavbarBurger';
import { expectToRenderComponentWithProperty } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders the html to display a burger icon on touch devices', () => {
    const element = shallow(<NavbarBurger />);
    expect(element.find('a.navbar-burger.burger').exists()).toBeTruthy();
    expect(element.find('a.navbar-burger.burger span')).toHaveLength(3);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(NavbarBurger, 'active');
});
