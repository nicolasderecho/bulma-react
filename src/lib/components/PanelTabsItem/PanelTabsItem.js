import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, propsWithoutKeys} from "../../helpers/util";

const PanelTabsItem = ({ className, ...props }) => {
    const classes = classNames(className, checkProperty(props, 'active'));
    const finalProps = propsWithoutKeys(props, ['active']);
    // eslint-disable-next-line
    return <a className={classes} {...finalProps} />;
};

PanelTabsItem.displayName = 'PanelTabsItem';

PanelTabsItem.propTypes = {
    className: PropTypes.string
};

export default PanelTabsItem;