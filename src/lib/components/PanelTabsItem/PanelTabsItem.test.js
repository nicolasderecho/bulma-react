import React from 'react';
import PanelTabsItem from './PanelTabsItem';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithProperty } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a panel tab item', () => {
    const component = shallow(<PanelTabsItem>My cool Tab</PanelTabsItem>);
    expect(component.find('a').exists()).toBeTruthy();
    expect(component.text() === 'My cool Tab').toBeTruthy();
});

it('renders the PanelTabsItem\'s children', () => {
    expectToRenderComponentWithChildren(PanelTabsItem);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(PanelTabsItem, 'active');
});