import React from 'react';
import DropdownMenu from './DropdownMenu';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class dropdown-menu', () => {
  expectToRenderComponentWithElement(DropdownMenu, 'div', 'dropdown-menu');
});

it('renders the DropdownMenu\'s children', () => {
  expectToRenderComponentWithChildren(DropdownMenu);
});
