import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const DropdownMenu = ({ className, ...props }) => {
  const classes = classNames(className, 'dropdown-menu');
  return <div className={classes} {...props} role={'menu'} />;
};

DropdownMenu.displayName = 'DropdownMenu';

DropdownMenu.propTypes = {
  className: PropTypes.string
};

export default DropdownMenu;