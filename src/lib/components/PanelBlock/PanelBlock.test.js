import React from 'react';
import PanelBlock from './PanelBlock';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty
} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a div with class panel-block', () => {
    expectToRenderComponentWithElement(PanelBlock, 'div', 'panel-block');
});

it('renders the PanelBlock\'s children', () => {
    expectToRenderComponentWithChildren(PanelBlock);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(PanelBlock, 'active');
});

it('renders a custom html element when requested', () => {
    const component = shallow(<PanelBlock as={'label'}/>);
    expect(component.find('label.panel-block').exists()).toBeTruthy();
});