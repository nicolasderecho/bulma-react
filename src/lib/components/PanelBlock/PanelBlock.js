import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, htmlElementFor, propsWithoutKeys} from "../../helpers/util";

const PanelBlock = ({ className, as, ...props }) => {
    const Element = htmlElementFor(as, 'div');
    const classes = classNames(className, 'panel-block', checkProperty(props, 'active'));
    const finalProps = propsWithoutKeys(props, ['active']);
    return <Element className={classes} {...finalProps} />;
};

PanelBlock.displayName = 'PanelBlock';

PanelBlock.propTypes = {
    className: PropTypes.string
};

export default PanelBlock;