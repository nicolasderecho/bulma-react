import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperties, COLOR_TYPES, colorClassFor, propsWithoutKeys} from "../../helpers/util";
import NavbarBrand from "../NavbarBrand/NavbarBrand";
import NavbarBurger from "../NavbarBurger/NavbarBurger";
import NavbarMenu from "../NavbarMenu/NavbarMenu";
import NavbarStart from "../NavbarStart/NavbarStart";
import NavbarEnd from "../NavbarEnd/NavbarEnd";
import NavbarItem from "../NavbarItem/NavbarItem";
import NavbarLink from "../NavbarLink/NavbarLink";
import NavbarDropdown from "../NavbarDropdown/NavbarDropdown";
import NavbarDivider from "../NavbarDivider/NavbarDivider";

const PROPERTIES_TO_CHECK = ['fixedTop', 'fixedBottom', 'transparent', 'spaced'];
const Navbar = ({ className, color, ...props }) => {
    const classes = classNames(className, 'navbar', checkProperties(props, PROPERTIES_TO_CHECK), colorClassFor(color));
    const finalProps = propsWithoutKeys(props, PROPERTIES_TO_CHECK);
    return <nav className={classes} {...finalProps} role={'navigation'} aria-label={'main navigation'} />;
};

Navbar.displayName = 'Navbar';

Navbar.Brand    = NavbarBrand;
Navbar.Burger   = NavbarBurger;
Navbar.Menu     = NavbarMenu;
Navbar.Start    = NavbarStart;
Navbar.End      = NavbarEnd;
Navbar.Item     = NavbarItem;
Navbar.Link     = NavbarLink;
Navbar.Dropdown = NavbarDropdown;
Navbar.Divider  = NavbarDivider;

Navbar.propTypes = {
    className: PropTypes.string,
    color: PropTypes.oneOf(COLOR_TYPES)
};

export default Navbar;