import React from 'react';
import Navbar from './Navbar';
import {
    expectToRenderComponentWithChildren, expectToRenderComponentWithColor,
    expectToRenderComponentWithElement,
    expectToRenderComponentWithProperty
} from "../../../testing/testHelpers";
import {COLOR_TYPES} from "../../helpers/util";

it('renders a nav with class navbar', () => {
    expectToRenderComponentWithElement(Navbar, 'nav', 'navbar');
});

it('renders the Navbar\'s children', () => {
    expectToRenderComponentWithChildren(Navbar);
});

it('adds the transparent class when requested', () => {
    expectToRenderComponentWithProperty(Navbar, 'transparent');
});

it('adds the fixed top class when requested', () => {
    expectToRenderComponentWithProperty(Navbar, 'fixedTop');
});

it('adds the fixed bottom class when requested', () => {
    expectToRenderComponentWithProperty(Navbar, 'fixedBottom');
});

it('adds the spaced class when requested', () => {
    expectToRenderComponentWithProperty(Navbar, 'spaced');
});

it('adds the requested color by props', () => {
    COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Navbar, color) );
});
