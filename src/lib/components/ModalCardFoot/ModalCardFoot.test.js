import React from 'react';
import ModalCardFoot from './ModalCardFoot';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a footer with class modal-card-foot', () => {
  expectToRenderComponentWithElement(ModalCardFoot, 'footer', 'modal-card-foot');
});

it('renders the ModalCardFoot\'s children', () => {
  expectToRenderComponentWithChildren(ModalCardFoot);
});