import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const ModalCardFoot = ({ className, ...props }) => {
  const classes = classNames(className, 'modal-card-foot');
  return <footer className={classes} {...props} />;
};

ModalCardFoot.displayName = 'ModalCardFoot';

ModalCardFoot.propTypes = {
  className: PropTypes.string
};

export default ModalCardFoot;