import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const CardImage = ({ className, ...props }) => {
    const classes = classNames(className, 'card-image');
    return <div className={classes} {...props} />;
};

CardImage.displayName = 'CardImage';

CardImage.propTypes = {
    className: PropTypes.string
};

export default CardImage;