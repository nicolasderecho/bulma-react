import React from 'react';
import CardImage from './CardImage';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class card-image', () => {
    expectToRenderComponentWithElement(CardImage, 'div', 'card-image');
});

it('renders the CardImage\'s children', () => {
    expectToRenderComponentWithChildren(CardImage);
});