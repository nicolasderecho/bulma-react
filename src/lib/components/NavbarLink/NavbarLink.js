import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, propsWithoutKeys} from "../../helpers/util";

const NavbarLink = ({ className, ...props }) => {
    const classes = classNames(className, 'navbar-link', checkProperty(props, 'arrowless'));
    const finalProps = propsWithoutKeys(props, ['arrowless']);
    // eslint-disable-next-line
    return <a className={classes} {...finalProps} />;
};

NavbarLink.displayName = 'NavbarLink';

NavbarLink.propTypes = {
    className: PropTypes.string
};

export default NavbarLink;