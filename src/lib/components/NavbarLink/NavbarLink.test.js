import React from 'react';
import NavbarLink from './NavbarLink';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement, expectToRenderComponentWithProperty
} from "../../../testing/testHelpers";

it('renders a link with class navbar-link', () => {
    expectToRenderComponentWithElement(NavbarLink, 'a', 'navbar-link');
});

it('renders the NavbarLink\'s children', () => {
    expectToRenderComponentWithChildren(NavbarLink);
});

it('adds the arrowless class when requested', () => {
    expectToRenderComponentWithProperty(NavbarLink, 'arrowless');
});
