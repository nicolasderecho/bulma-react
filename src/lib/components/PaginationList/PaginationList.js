import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const PaginationList = ({ className, ...props }) => {
    const classes = classNames(className, 'pagination-list');
    return <ul className={classes} {...props} />;
};

PaginationList.displayName = 'PaginationList';

PaginationList.propTypes = {
    className: PropTypes.string
};

export default PaginationList;