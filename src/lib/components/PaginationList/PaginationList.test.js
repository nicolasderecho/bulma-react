import React from 'react';
import PaginationList from './PaginationList';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a ul with class pagination-list', () => {
    expectToRenderComponentWithElement(PaginationList, 'ul', 'pagination-list');
});

it('renders the PaginationList\'s children', () => {
    expectToRenderComponentWithChildren(PaginationList);
});