import React from 'react';
import Pagination from './Pagination';
import { expectToRenderComponentWithAlignment, expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithProperty, expectToRenderComponentWithSize } from "../../../testing/testHelpers";
import {ALIGNMENTS, SIZES} from "../../helpers/util";

it('renders a nav with class pagination', () => {
    expectToRenderComponentWithElement(Pagination, 'nav', 'pagination');
});

it('renders the Pagination\'s children', () => {
    expectToRenderComponentWithChildren(Pagination);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(Pagination, 'rounded');
});

it('renders the component with the requested alignment', () => {
    ALIGNMENTS.forEach((alignment) => expectToRenderComponentWithAlignment(Pagination, alignment) );
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Pagination, size) );
});