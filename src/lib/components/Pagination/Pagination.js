import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {alignmentClassFor, ALIGNMENTS, checkProperty, propsWithoutKeys, sizeClassFor, SIZES} from "../../helpers/util";
import PaginationPrevious from "../PaginationPrevious/PaginationPrevious";
import PaginationNext from "../PaginationNext/PaginationNext";
import PaginationList from "../PaginationList/PaginationList";
import PaginationLink from "../PaginationLink/PaginationLink";
import PaginationEllipsis from "../PaginationEllipsis/PaginationEllipsis";

const Pagination = ({ className, size, alignment, ...props }) => {
    const classes = classNames(className, 'pagination', sizeClassFor(size), alignmentClassFor(alignment), checkProperty(props, 'rounded'));
    const finalProps = propsWithoutKeys(props, ['rounded']);
    return <nav className={classes} {...finalProps} role={'navigation'} aria-label={'pagination'}/>;
};

Pagination.displayName = 'Pagination';

Pagination.Previous = PaginationPrevious;
Pagination.Next     = PaginationNext;
Pagination.List     = PaginationList;
Pagination.Link     = PaginationLink;
Pagination.Ellipsis = PaginationEllipsis;

Pagination.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(SIZES),
    alignment: PropTypes.oneOf(ALIGNMENTS)
};

export default Pagination;