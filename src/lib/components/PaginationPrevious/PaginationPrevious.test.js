import React from 'react';
import PaginationPrevious from './PaginationPrevious';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a link with class pagination-previous', () => {
    expectToRenderComponentWithElement(PaginationPrevious, 'a', 'pagination-previous');
});

it('renders the PaginationPrevious\'s children', () => {
    expectToRenderComponentWithChildren(PaginationPrevious);
});