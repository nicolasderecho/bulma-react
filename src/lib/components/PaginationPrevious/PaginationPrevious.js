import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const PaginationPrevious = ({ className, ...props }) => {
    const classes = classNames(className, 'pagination-previous');
    // eslint-disable-next-line
    return <a className={classes} {...props} />;
};

PaginationPrevious.displayName = 'PaginationPrevious';

PaginationPrevious.propTypes = {
    className: PropTypes.string
};

export default PaginationPrevious;