import React from 'react';
import Tabs from './Tabs';
import { expectToRenderComponentWithAlignment, expectToRenderComponentWithElement, expectToRenderComponentWithProperty, expectToRenderComponentWithSize } from "../../../testing/testHelpers";
import {ALIGNMENTS, SIZES} from "../../helpers/util";
import {shallow} from "enzyme/build";

it('renders a div with class tabs', () => {
    expectToRenderComponentWithElement(Tabs, 'div', 'tabs');
});

it('renders the Tabs\'s children inside a ul element', () => {
    const child = <li>Child Element</li>;
    const component = shallow(<Tabs>{child}</Tabs>);
    const ulElement = component.find('ul');
    expect(ulElement.exists()).toBeTruthy();
    expect(ulElement.contains(child)).toBeTruthy();
});

it('adds a class to the ul child when requested', () => {
    const component = shallow(<Tabs ulClassName={'my-custom-ul'}/>);
    const ulElement = component.find('ul.my-custom-ul');
    expect(ulElement.exists()).toBeTruthy();
});

it('adds the boxed class when requested', () => {
    expectToRenderComponentWithProperty(Tabs, 'boxed');
});

it('adds the toggle class when requested', () => {
    expectToRenderComponentWithProperty(Tabs, 'toggle');
});

it('adds the rounded class when requested', () => {
    expectToRenderComponentWithProperty(Tabs, 'rounded');
});

it('adds the toggle-rounded class when requested', () => {
    expectToRenderComponentWithProperty(Tabs, 'toggleRounded');
});

it('adds the fullwidth class when requested', () => {
    expectToRenderComponentWithProperty(Tabs, 'fullwidth');
});

it('renders the component with the requested alignment', () => {
    ALIGNMENTS.forEach((alignment) => expectToRenderComponentWithAlignment(Tabs, alignment) );
});

it('adds the requested size by props', () => {
    SIZES.forEach((size) => expectToRenderComponentWithSize(Tabs, size) );
});