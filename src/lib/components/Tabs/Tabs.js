import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ALIGNMENTS, SIZES, sizeClassFor, alignmentClassFor, checkProperties, propsWithoutKeys } from "../../helpers/util";
import TabsItem from "../TabsItem/TabsItem";

const PROPERTIES_TO_CHECK = ['boxed', 'rounded', 'fullwidth', 'toggle', 'toggleRounded'];
const Tabs = ({ className, size, alignment, ulClassName, children, ...props }) => {
    const classes = classNames(className, 'tabs', sizeClassFor(size), alignmentClassFor(alignment), checkProperties(props, PROPERTIES_TO_CHECK));
    const ulClasses = classNames(ulClassName);
    const finalProps = propsWithoutKeys(props, PROPERTIES_TO_CHECK);
    return <div className={classes} {...finalProps}><ul className={ulClasses}>{children}</ul></div>;
};

Tabs.displayName = 'Tabs';

Tabs.Item = TabsItem;

Tabs.propTypes = {
    className: PropTypes.string,
    ulClassName: PropTypes.string,
    alignment: PropTypes.oneOf(ALIGNMENTS),
    size: PropTypes.oneOf(SIZES)
};

export default Tabs;