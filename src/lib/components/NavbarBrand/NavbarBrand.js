import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const NavbarBrand = ({ className, ...props }) => {
    const classes = classNames(className, 'navbar-brand');
    return <div className={classes} {...props} />;
};

NavbarBrand.displayName = 'NavbarBrand';

NavbarBrand.propTypes = {
    className: PropTypes.string
};

export default NavbarBrand;