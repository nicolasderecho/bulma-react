import React from 'react';
import NavbarBrand from './NavbarBrand';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement
} from "../../../testing/testHelpers";

it('renders a div with class navbar-brand', () => {
    expectToRenderComponentWithElement(NavbarBrand, 'div', 'navbar-brand');
});

it('renders the NavbarBrand\'s children', () => {
    expectToRenderComponentWithChildren(NavbarBrand);
});
