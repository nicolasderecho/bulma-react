import React from 'react';
import NavbarStart from './NavbarStart';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement
} from "../../../testing/testHelpers";

it('renders a div with class navbar-start', () => {
    expectToRenderComponentWithElement(NavbarStart, 'div', 'navbar-start');
});

it('renders the NavbarStart\'s children', () => {
    expectToRenderComponentWithChildren(NavbarStart);
});
