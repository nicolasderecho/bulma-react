import React from 'react';
import ModalContent from './ModalContent';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class modal-content', () => {
  expectToRenderComponentWithElement(ModalContent, 'div', 'modal-content');
});

it('renders the ModalContent\'s children', () => {
  expectToRenderComponentWithChildren(ModalContent);
});