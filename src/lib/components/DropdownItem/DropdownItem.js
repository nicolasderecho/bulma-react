import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, htmlElementFor, propsWithoutKeys} from "../../helpers/util";

const DropdownItem = ({ className, as, ...props }) => {
  const Element = htmlElementFor(as, 'a');
  const classes = classNames(className, 'dropdown-item', checkProperty(props, 'active'));
  const finalProps = propsWithoutKeys(props, ['active']);
  return <Element className={classes} {...finalProps} />;
};

DropdownItem.displayName = 'DropdownItem';

DropdownItem.propTypes = {
  className: PropTypes.string
};

export default DropdownItem;