import React from 'react';
import DropdownItem from './DropdownItem';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithProperty } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a link with class dropdown-item', () => {
  expectToRenderComponentWithElement(DropdownItem, 'a', 'dropdown-item');
});

it('renders the DropdownItem\'s children', () => {
  expectToRenderComponentWithChildren(DropdownItem);
});

it('renders the element requested by props', () => {
  const element = shallow(<DropdownItem as={'div'} />);
  expect(element.find('div.dropdown-item').exists()).toBeTruthy();
});

it('adds the active class when requested', () => {
  expectToRenderComponentWithProperty(DropdownItem, 'active');
});