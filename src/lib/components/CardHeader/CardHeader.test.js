import React from 'react';
import CardHeader from './CardHeader';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a header with class card-header', () => {
    expectToRenderComponentWithElement(CardHeader, 'header', 'card-header');
});

it('renders the CardHeader\'s children', () => {
    expectToRenderComponentWithChildren(CardHeader);
});