import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CardHeaderTitle from "../CardHeaderTitle/CardHeaderTitle";
import CardHeaderIcon from "../CardHeaderIcon/CardHeaderIcon";

const CardHeader = ({ className, ...props }) => {
    const classes = classNames(className, 'card-header');
    return <header className={classes} {...props} />;
};

CardHeader.displayName = 'CardHeader';

CardHeader.Title = CardHeaderTitle;
CardHeader.Icon  = CardHeaderIcon;

CardHeader.propTypes = {
    className: PropTypes.string
};

export default CardHeader;