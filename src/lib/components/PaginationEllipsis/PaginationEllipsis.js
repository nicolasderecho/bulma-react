import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const PaginationEllipsis = ({ className, text, ...props }) => {
    const ellipsisText = text || '...';
    const classes = classNames(className, 'pagination-ellipsis');
    return <li><span className={classes} {...props}>{ellipsisText}</span></li>;
};

PaginationEllipsis.displayName = 'PaginationEllipsis';

PaginationEllipsis.propTypes = {
    className: PropTypes.string,
    text: PropTypes.string
};

export default PaginationEllipsis;