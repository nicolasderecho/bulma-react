import React from 'react';
import PaginationEllipsis from './PaginationEllipsis';
import { expectToRenderComponentWithElement } from "../../../testing/testHelpers";
import {mount} from "enzyme/build";

it('renders a span with class pagination-ellipsis', () => {
    expectToRenderComponentWithElement(PaginationEllipsis, 'span', 'pagination-ellipsis');
});

it('renders a custom text when requested', () => {
    const component = mount(<PaginationEllipsis text={'pepe'}/>).find('li');
    expect(component.find('span').text() === 'pepe').toBeTruthy();
});