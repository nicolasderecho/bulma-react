import React from 'react';
import DropdownTrigger from './DropdownTrigger';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class dropdown-trigger', () => {
  expectToRenderComponentWithElement(DropdownTrigger, 'div', 'dropdown-trigger');
});

it('renders the DropdownTrigger\'s children', () => {
  expectToRenderComponentWithChildren(DropdownTrigger);
});
