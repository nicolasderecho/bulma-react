import React from 'react';
import Dropdown from './Dropdown';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithProperty } from "../../../testing/testHelpers";

it('renders a div with class dropdown', () => {
    expectToRenderComponentWithElement(Dropdown, 'div', 'dropdown');
});

it('renders the Dropdown\'s children', () => {
    expectToRenderComponentWithChildren(Dropdown);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(Dropdown, 'active');
});

it('adds the \'right\' class when requested', () => {
    expectToRenderComponentWithProperty(Dropdown, 'right');
});

it('adds the up class when requested', () => {
    expectToRenderComponentWithProperty(Dropdown, 'up');
});

it('adds the hoverable class when requested', () => {
    expectToRenderComponentWithProperty(Dropdown, 'hoverable');
});