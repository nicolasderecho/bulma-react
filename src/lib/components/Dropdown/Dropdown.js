import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperties, propsWithoutKeys} from "../../helpers/util";
import DropdownTrigger from "../DropdownTrigger/DropdownTrigger";
import DropdownMenu from "../DropdownMenu/DropdownMenu";
import DropdownContent from "../DropdownContent/DropdownContent";
import DropdownDivider from "../DropdownDivider/DropdownDivider";
import DropdownItem from "../DropdownItem/DropdownItem";

const PROPERTIES_TO_CHECK = ['active', 'up', 'right', 'hoverable'];
const Dropdown = ({ className, ...props }) => {
    const classes = classNames(className, 'dropdown', checkProperties(props, PROPERTIES_TO_CHECK));
    const finalProps = propsWithoutKeys(props, PROPERTIES_TO_CHECK);
    return <div className={classes} {...finalProps} />;
};

Dropdown.displayName = 'Dropdown';

Dropdown.Trigger = DropdownTrigger;
Dropdown.Menu    = DropdownMenu;
Dropdown.Content = DropdownContent;
Dropdown.Divider = DropdownDivider;
Dropdown.Item    = DropdownItem;

Dropdown.propTypes = {
    className: PropTypes.string
};

export default Dropdown;