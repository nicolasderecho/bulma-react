import React from 'react';
import PaginationNext from './PaginationNext';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a link with class pagination-next', () => {
    expectToRenderComponentWithElement(PaginationNext, 'a', 'pagination-next');
});

it('renders the PaginationNext\'s children', () => {
    expectToRenderComponentWithChildren(PaginationNext);
});