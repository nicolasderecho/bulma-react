import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const PaginationNext = ({ className, ...props }) => {
    const classes = classNames(className, 'pagination-next');
    // eslint-disable-next-line
    return <a className={classes} {...props} />;
};

PaginationNext.displayName = 'PaginationNext';

PaginationNext.propTypes = {
    className: PropTypes.string
};

export default PaginationNext;