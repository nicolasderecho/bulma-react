import React from 'react';
import MenuLabel from './MenuLabel';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a p with class menu-label', () => {
  expectToRenderComponentWithElement(MenuLabel, 'p', 'menu-label');
});

it('renders the MenuLabel\'s children', () => {
  expectToRenderComponentWithChildren(MenuLabel);
});