import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const MenuLabel = ({ className, ...props }) => {
  const classes = classNames(className, 'menu-label');
  return <p className={classes} {...props} />;
};

MenuLabel.displayName = 'MenuLabel';

MenuLabel.propTypes = {
  className: PropTypes.string
};

export default MenuLabel;