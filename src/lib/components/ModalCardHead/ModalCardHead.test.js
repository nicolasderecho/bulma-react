import React from 'react';
import ModalCardHead from './ModalCardHead';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a header with class modal-card-head', () => {
  expectToRenderComponentWithElement(ModalCardHead, 'header', 'modal-card-head');
});

it('renders the ModalCardHead\'s children', () => {
  expectToRenderComponentWithChildren(ModalCardHead);
});