import React from 'react';
import NavbarEnd from './NavbarEnd';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement
} from "../../../testing/testHelpers";

it('renders a div with class navbar-end', () => {
    expectToRenderComponentWithElement(NavbarEnd, 'div', 'navbar-end');
});

it('renders the NavbarEnd\'s children', () => {
    expectToRenderComponentWithChildren(NavbarEnd);
});
