import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const NavbarEnd = ({ className, ...props }) => {
    const classes = classNames(className, 'navbar-end');
    return <div className={classes} {...props} />;
};

NavbarEnd.displayName = 'NavbarEnd';

NavbarEnd.propTypes = {
    className: PropTypes.string
};

export default NavbarEnd;