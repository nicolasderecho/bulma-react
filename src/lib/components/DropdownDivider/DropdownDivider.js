import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const DropdownDivider = ({ className, ...props }) => {
  const classes = classNames(className, 'dropdown-divider');
  return <hr className={classes} {...props} />;
};

DropdownDivider.displayName = 'DropdownDivider';

DropdownDivider.propTypes = {
  className: PropTypes.string
};

export default DropdownDivider;