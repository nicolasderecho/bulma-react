import React from 'react';
import DropdownDivider from './DropdownDivider';
import { expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a hr with class dropdown-divider', () => {
  expectToRenderComponentWithElement(DropdownDivider, 'hr', 'dropdown-divider');
});