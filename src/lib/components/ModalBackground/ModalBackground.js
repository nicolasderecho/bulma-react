import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const ModalBackground = ({ className, ...props }) => {
  const classes = classNames(className, 'modal-background');
  return <div className={classes} {...props} />;
};

ModalBackground.displayName = 'ModalBackground';

ModalBackground.propTypes = {
  className: PropTypes.string
};

export default ModalBackground;