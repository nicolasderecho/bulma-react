import React from 'react';
import ModalBackground from './ModalBackground';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class modal-background', () => {
  expectToRenderComponentWithElement(ModalBackground, 'div', 'modal-background');
});

it('renders the ModalBackground\'s children', () => {
  expectToRenderComponentWithChildren(ModalBackground);
});
