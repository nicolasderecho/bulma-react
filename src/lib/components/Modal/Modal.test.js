import React from 'react';
import Modal from './Modal';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithProperty } from "../../../testing/testHelpers";
import {mount} from "enzyme/build";

it('renders a div with class modal', () => {
  expectToRenderComponentWithElement(Modal, 'div', 'modal');
});

it('renders the Modal\'s children', () => {
  expectToRenderComponentWithChildren(Modal);
});

it('adds the active class when requested', () => {
  expectToRenderComponentWithProperty(Modal, 'active');
});

it('closes on Escape by default', () => {
  let displayModal = true;
  const closeModal = () => displayModal = false;
  const component = mount(<Modal active={displayModal} closeFunction={closeModal}/>);
  expect(component.find('.modal.is-active').exists()).toBeTruthy();
  document.dispatchEvent(new KeyboardEvent('keydown', { key: 'Escape' }));
  expect(displayModal).toBeFalsy();
});

it('doesn\'t close on Escape when requested', () => {
  let displayModal = true;
  const closeModal = () => displayModal = false;
  const component = mount(<Modal active={displayModal} closeFunction={closeModal} closeOnEscape={false} />);
  expect(component.find('.modal.is-active').exists()).toBeTruthy();
  document.dispatchEvent(new KeyboardEvent('keydown', { key: 'Escape' }));
  expect(displayModal).toBeTruthy();
});

describe('clipped modal', () => {

  beforeEach(() => {
    document.querySelector('html').classList.remove('is-clipped');
  });

  it('adds is-clipped class to html when the modal is active', () => {
    expect(document.querySelector('html.is-clipped')).toBeNull();
    mount(<Modal active clipped />);
    expect(document.querySelector('html.is-clipped')).not.toBeNull();
  });

  it('doesn\'t add is-clipped class to html when the modal is not active', () => {
    expect(document.querySelector('html.is-clipped')).toBeNull();
    mount(<Modal active={false} clipped />);
    expect(document.querySelector('html.is-clipped')).toBeNull();
  });

  it('removes the clipped class when the modal is clipped and it\'s hiding', () => {
    const component = mount(<Modal active={true} clipped={true}/>);
    component.setProps({active: false, clipped: true});
    expect(document.querySelector('html.is-clipped')).toBeNull();
  });

  it('removes the clipped class when the modal is clipped and it\'s unmounting', () => {
    const component = mount(<Modal active={true} clipped={true}/>);
    component.unmount();
    expect(document.querySelector('html.is-clipped')).toBeNull();
  });

  it('doesn\'t remove the clipped class when the modal is not clipped', () => {
    document.querySelector('html').classList.add('is-clipped'); //added by another component
    const component = mount(<Modal active={true} clipped={false}/>);
    component.setProps({active: false, clipped: false});
    expect(document.querySelector('html.is-clipped')).not.toBeNull();
  });

});

describe('callbacks', () => {

  describe('when modal is being updated', () => {
    it('calls the onOpen callback when it receives one', () => {
      const callbacks = { onOpen: () => {} };
      const spy = jest.spyOn(callbacks, 'onOpen');
      const firstModal = mount(<Modal active={false} onOpen={callbacks.onOpen}/>);
      const secondModal = mount(<Modal active={false} onOpen={callbacks.onOpen}/>);
      firstModal.setProps({active: true, onOpen: callbacks.onOpen});
      secondModal.setProps({active: false, onOpen: callbacks.onOpen});
      expect(spy).toHaveBeenCalledTimes(1);
      spy.mockRestore();
    });

    it('calls the onClose callback when it receives one', () => {
      const callbacks = { onClose: () => {} };
      const spy = jest.spyOn(callbacks, 'onClose');
      const firstModal  = mount(<Modal active={true} onClose={callbacks.onClose}/>);
      const secondModal = mount(<Modal active={true} onClose={callbacks.onClose}/>);
      firstModal.setProps({active: true, onClose: callbacks.onClose});
      secondModal.setProps({active: false, onClose: callbacks.onClose});
      expect(spy).toHaveBeenCalledTimes(1);
      spy.mockRestore();
    });
  });

  describe('when the modal has been just mounted', () => {
    it('calls the onOpen callback when the modal starts opened', () => {
      const callbacks = { onOpen: () => {} };
      const spy = jest.spyOn(callbacks, 'onOpen');
      mount(<Modal active={true} onOpen={callbacks.onOpen}/>);
      mount(<Modal active={false} onOpen={callbacks.onOpen}/>);
      expect(spy).toHaveBeenCalledTimes(1);
      spy.mockRestore();
    });

    it('calls the onMount callback when it receives one', () => {
      const callbacks = { onMount: () => {} };
      const spy = jest.spyOn(callbacks, 'onMount');
      mount(<Modal active={false} onMount={callbacks.onMount}/>);
      expect(spy).toHaveBeenCalledTimes(1);
      spy.mockRestore();
    });
  });

  it('calls the onUnmount callback when it\'s being unmounted', () => {
    const callbacks = { onUnmount: () => {} };
    const spy = jest.spyOn(callbacks, 'onUnmount');
    const component = mount(<Modal active={false} onUnmount={callbacks.onUnmount}/>);
    component.unmount();
    expect(spy).toHaveBeenCalledTimes(1);
    spy.mockRestore();
  });

});