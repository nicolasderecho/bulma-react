import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, isEnabled, propsWithoutKeys, getValue} from "../../helpers/util";
import ModalBackground from "../ModalBackground/ModalBackground";
import ModalContent from "../ModalContent/ModalContent";
import ModalClose from "../ModalClose/ModalClose";
import ModalCard from "../ModalCard/ModalCard";

class Modal extends React.Component {

  static isEscape(event) {
    const key = event.key || event.keyCode;
    return key === 'Escape' || key === 'Esc' || key === 27;
  }

  constructor(props) {
    super(props);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  hasClippedProp() {
    return isEnabled(this.props, 'clipped');
  }

  isActive() {
    return isEnabled(this.props, 'active');
  }

  addClippedClassToHtml() {
    document.querySelector('html').classList.add('is-clipped');
  }

  removeClippedClassToHtml() {
    document.querySelector('html').classList.remove('is-clipped');
  }

  mustCloseOnEscape(){
    return getValue(this.props, 'closeOnEscape', true);
  }

  onKeyDown(event) {
    if(this.props.closeFunction && Modal.isEscape(event) && this.isActive() && this.mustCloseOnEscape()) {
      this.props.closeFunction();
    }
  }

  maybeAddClippedClass() {
    if(this.hasClippedProp() && this.isActive()) {
      this.addClippedClassToHtml();
    }
  }

  maybeRemoveClippedClass() {
    if(this.hasClippedProp()) {
      this.removeClippedClassToHtml();
    }
  }

  modalDidOpen(prevActive, currentActive) {
    return !prevActive && !!currentActive;
  }

  modalDidClose(prevActive, currentActive) {
    return !!prevActive && !currentActive;
  }

  onModalOpen() {
    this.maybeAddClippedClass();
    if(this.props.onOpen) {
      this.props.onOpen();
    }
  }

  onModalClose() {
    this.maybeRemoveClippedClass();
    if(this.props.onClose) {
      this.props.onClose();
    }
  }

  checkModalOpened(prevActive, currentActive){
    if(this.modalDidOpen(prevActive, currentActive)){
      this.onModalOpen();
    }
  }

  checkModalClosed(prevActive, currentActive) {
    if(this.modalDidClose(prevActive, currentActive)) {
      this.onModalClose();
    }
  }

  componentDidMount() {
    document.addEventListener('keydown', this.onKeyDown);
    this.checkModalOpened(false, this.props.active);
    if(this.props.onMount) {
      this.props.onMount();
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.checkModalOpened(prevProps.active, this.props.active);
    this.checkModalClosed(prevProps.active, this.props.active);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyDown);
    this.maybeRemoveClippedClass();
    if(this.props.onUnmount) {
      this.props.onUnmount();
    }
  }

  render() {
    const classes = classNames(this.props.className, 'modal', checkProperty(this.props, 'active'));
    const finalProps = propsWithoutKeys(this.props, ['active', 'closeFunction', 'clipped', 'className', 'onOpen', 'onMount', 'onUnmount', 'closeOnEscape', 'closeOnDocumentClick']);
    return <div className={classes} {...finalProps} />;
  }
}

Modal.displayName = 'Modal';

Modal.Background  = ModalBackground;
Modal.Content     = ModalContent;
Modal.Close       = ModalClose;
Modal.Card        = ModalCard;

Modal.propTypes = {
  className: PropTypes.string,
  closeFunction: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  onMount: PropTypes.func,
  onUnmount: PropTypes.func
};

export default Modal;