import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const PanelHeading = ({ className, ...props }) => {
    const classes = classNames(className, 'panel-heading');
    return <p className={classes} {...props} />;
};

PanelHeading.displayName = 'PanelHeading';

PanelHeading.propTypes = {
    className: PropTypes.string
};

export default PanelHeading;