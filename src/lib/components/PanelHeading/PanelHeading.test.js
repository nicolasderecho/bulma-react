import React from 'react';
import PanelHeading from './PanelHeading';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a paragraph with class panel-heading', () => {
    expectToRenderComponentWithElement(PanelHeading, 'p', 'panel-heading');
});

it('renders the PanelHeading\'s children', () => {
    expectToRenderComponentWithChildren(PanelHeading);
});