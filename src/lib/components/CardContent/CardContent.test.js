import React from 'react';
import CardContent from './CardContent';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class card-content', () => {
    expectToRenderComponentWithElement(CardContent, 'div', 'card-content');
});

it('renders the CardContent\'s children', () => {
    expectToRenderComponentWithChildren(CardContent);
});