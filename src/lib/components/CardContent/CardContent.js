import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const CardContent = ({ className, ...props }) => {
    const classes = classNames(className, 'card-content');
    return <div className={classes} {...props} />;
};

CardContent.displayName = 'CardContent';

CardContent.propTypes = {
    className: PropTypes.string
};

export default CardContent;