import React from 'react';
import PaginationLink from './PaginationLink';
import {expectToRenderComponentWithChildren, expectToRenderComponentWithProperty} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a link inside a li with class pagination-link', () => {
    const component = shallow(<PaginationLink/>);
    expect(component.find('li a.pagination-link').exists()).toBeTruthy();
});

it('renders a custom html element when requested', () => {
    const component = shallow(<PaginationLink as={'span'}/>);
    expect(component.find('li span.pagination-link').exists()).toBeTruthy();
});

it('renders the PaginationLink\'s children', () => {
    expectToRenderComponentWithChildren(PaginationLink);
});

it('adds the current class when requested', () => {
    expectToRenderComponentWithProperty(PaginationLink, 'current');
});
