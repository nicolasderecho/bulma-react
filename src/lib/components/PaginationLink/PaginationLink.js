import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, htmlElementFor, propsWithoutKeys} from "../../helpers/util";

const PaginationLink = ({ className, as, ...props }) => {
    const Element = htmlElementFor(as, 'a');
    const classes = classNames(className, 'pagination-link', checkProperty(props, 'current'));
    const finalProps = propsWithoutKeys(props, ['current']);
    return <li><Element className={classes} {...finalProps} /></li>;
};

PaginationLink.displayName = 'PaginationLink';

PaginationLink.propTypes = {
    className: PropTypes.string
};

export default PaginationLink;