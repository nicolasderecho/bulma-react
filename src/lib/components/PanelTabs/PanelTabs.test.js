import React from 'react';
import PanelTabs from './PanelTabs';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a paragraph with class panel-tabs', () => {
    expectToRenderComponentWithElement(PanelTabs, 'p', 'panel-tabs');
});

it('renders the PanelTabs\'s children', () => {
    expectToRenderComponentWithChildren(PanelTabs);
});