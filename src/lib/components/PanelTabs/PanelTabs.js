import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import PanelTabsItem from "../PanelTabsItem/PanelTabsItem";

const PanelTabs = ({ className, ...props }) => {
    const classes = classNames(className, 'panel-tabs');
    return <p className={classes} {...props} />;
};

PanelTabs.displayName = 'PanelTabs';

PanelTabs.Item = PanelTabsItem;

PanelTabs.propTypes = {
    className: PropTypes.string
};

export default PanelTabs;