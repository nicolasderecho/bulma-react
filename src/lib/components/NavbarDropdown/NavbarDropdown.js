import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, propsWithoutKeys} from "../../helpers/util";

const NavbarDropdown = ({ className, ...props }) => {
    const classes = classNames(className, 'navbar-dropdown', checkProperty(props, 'right'));
    const finalProps = propsWithoutKeys(props, ['right']);
    return <div className={classes} {...finalProps} />;
};

NavbarDropdown.displayName = 'NavbarDropdown';

NavbarDropdown.propTypes = {
    className: PropTypes.string
};

export default NavbarDropdown;