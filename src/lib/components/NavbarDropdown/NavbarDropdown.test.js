import React from 'react';
import NavbarDropdown from './NavbarDropdown';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement, expectToRenderComponentWithProperty
} from "../../../testing/testHelpers";

it('renders a div with class navbar-dropdown', () => {
    expectToRenderComponentWithElement(NavbarDropdown, 'div', 'navbar-dropdown');
});

it('renders the NavbarDropdown\'s children', () => {
    expectToRenderComponentWithChildren(NavbarDropdown);
});

it('adds the right class when requested', () => {
    expectToRenderComponentWithProperty(NavbarDropdown, 'right');
});