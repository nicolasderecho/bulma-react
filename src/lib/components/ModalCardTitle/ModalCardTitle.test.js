import React from 'react';
import ModalCardTitle from './ModalCardTitle';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a p with class modal-card-title', () => {
  expectToRenderComponentWithElement(ModalCardTitle, 'p', 'modal-card-title');
});

it('renders the ModalCardTitle\'s children', () => {
  expectToRenderComponentWithChildren(ModalCardTitle);
});