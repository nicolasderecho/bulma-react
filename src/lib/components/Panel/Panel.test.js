import React from 'react';
import Panel from './Panel';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a nav with class panel', () => {
    expectToRenderComponentWithElement(Panel, 'nav', 'panel');
});

it('renders the Panel\'s children', () => {
    expectToRenderComponentWithChildren(Panel);
});