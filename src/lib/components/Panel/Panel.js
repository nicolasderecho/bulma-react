import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import PanelHeading from "../PanelHeading/PanelHeading";
import PanelBlock from "../PanelBlock/PanelBlock";
import PanelIcon from "../PanelIcon/PanelIcon";
import PanelTabs from "../PanelTabs/PanelTabs";

const Panel = ({ className, ...props }) => {
    const classes = classNames(className, 'panel');
    return <nav className={classes} {...props} />;
};

Panel.displayName = 'Panel';

Panel.Heading = PanelHeading;
Panel.Block   = PanelBlock;
Panel.Icon    = PanelIcon;
Panel.Tabs    = PanelTabs;

Panel.propTypes = {
    className: PropTypes.string
};

export default Panel;