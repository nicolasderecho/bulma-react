import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, propsWithoutKeys} from "../../helpers/util";

const TabsItem = ({ className, children, ...props }) => {
    const classes = classNames(className, checkProperty(props, 'active'));
    const finalProps = propsWithoutKeys(props, ['active']);
    // eslint-disable-next-line
    return <li className={classes} {...finalProps} ><a>{children}</a></li>;
};

TabsItem.displayName = 'TabsItem';

TabsItem.propTypes = {
    className: PropTypes.string
};

export default TabsItem;