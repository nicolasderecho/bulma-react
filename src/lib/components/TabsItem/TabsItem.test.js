import React from 'react';
import TabsItem from './TabsItem';
import { expectToRenderComponentWithProperty } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders the TabsItem\'s children inside a li element with a link', () => {
    const component = shallow(<TabsItem>Some Item</TabsItem>);
    const liElement = component.find('li');
    const aElement  = component.find('li a');
    expect(liElement.exists()).toBeTruthy();
    expect(aElement.exists()).toBeTruthy();
    expect(aElement.text() === 'Some Item').toBeTruthy();
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(TabsItem, 'active');
});