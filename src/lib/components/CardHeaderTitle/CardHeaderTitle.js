import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, htmlElementFor, propsWithoutKeys} from "../../helpers/util";

const CardHeaderTitle = ({ className, as, ...props }) => {
    const Element = htmlElementFor(as, 'p');
    const classes = classNames(className, 'card-header-title', checkProperty(props, 'centered'));
    const finalProps = propsWithoutKeys(props, ['centered']);
    return <Element className={classes} {...finalProps} />;
};

CardHeaderTitle.displayName = 'CardHeaderTitle';

CardHeaderTitle.propTypes = {
    className: PropTypes.string
};

export default CardHeaderTitle;