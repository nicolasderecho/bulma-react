import React from 'react';
import CardHeaderTitle from './CardHeaderTitle';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement, expectToRenderComponentWithProperty } from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";

it('renders a paragraph with class card-header-title', () => {
    expectToRenderComponentWithElement(CardHeaderTitle, 'p', 'card-header-title');
});

it('renders the CardHeaderTitle\'s children', () => {
    expectToRenderComponentWithChildren(CardHeaderTitle);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(CardHeaderTitle, 'centered');
});

it('renders the element requested by props', () => {
    const element = shallow(<CardHeaderTitle as={'a'} href={'#'}/>);
    expect(element.find('a.card-header-title').exists()).toBeTruthy();
});