import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperty, propsWithoutKeys} from "../../helpers/util";

const NavbarMenu = ({ className, ...props }) => {
    const classes = classNames(className, 'navbar-menu', checkProperty(props, 'active'));
    const finalProps = propsWithoutKeys(props, ['active']);
    return <div className={classes} {...finalProps} />;
};

NavbarMenu.displayName = 'NavbarMenu';

NavbarMenu.propTypes = {
    className: PropTypes.string
};

export default NavbarMenu;