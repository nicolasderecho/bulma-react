import React from 'react';
import NavbarMenu from './NavbarMenu';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement, expectToRenderComponentWithProperty
} from "../../../testing/testHelpers";

it('renders a div with class navbar-menu', () => {
    expectToRenderComponentWithElement(NavbarMenu, 'div', 'navbar-menu');
});

it('renders the NavbarMenu\'s children', () => {
    expectToRenderComponentWithChildren(NavbarMenu);
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(NavbarMenu, 'active');
});