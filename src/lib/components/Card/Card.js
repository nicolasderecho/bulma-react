import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CardImage from "../CardImage/CardImage";
import CardContent from "../CardContent/CardContent";
import CardFooter from "../CardFooter/CardFooter";
import CardHeader from "../CardHeader/CardHeader";

const Card = ({ className, ...props }) => {
    const classes = classNames(className, 'card');
    return <div className={classes} {...props} />;
};

Card.displayName = 'Card';

Card.Image   = CardImage;
Card.Content = CardContent;
Card.Footer  = CardFooter;
Card.Header  = CardHeader;

Card.propTypes = {
    className: PropTypes.string
};

export default Card;