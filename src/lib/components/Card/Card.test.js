import React from 'react';
import Card from './Card';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a div with class card', () => {
    expectToRenderComponentWithElement(Card, 'div', 'card');
});

it('renders the Card\'s children', () => {
    expectToRenderComponentWithChildren(Card);
});