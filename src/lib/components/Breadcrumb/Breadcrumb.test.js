import React from 'react';
import Breadcrumb from './Breadcrumb';
import {
  expectToRenderComponentWithAlignment,
  expectToRenderComponentWithElement,
  expectToRenderComponentWithSize,
  expectToRenderComponentWithSeparator
} from "../../../testing/testHelpers";
import {shallow} from "enzyme/build";
import {ALIGNMENTS, SEPARATORS, SIZES} from "../../helpers/util";

it('renders a nav with class breadcrumb', () => {
  expectToRenderComponentWithElement(Breadcrumb, 'nav', 'breadcrumb');
});

it('renders the Breadcrumb\'s children inside a ul element', () => {
  const child = <li>Child Element</li>;
  const component = shallow(<Breadcrumb>{child}</Breadcrumb>);
  const ulElement = component.find('ul');
  expect(ulElement.exists()).toBeTruthy();
  expect(ulElement.contains(child)).toBeTruthy();
});

it('adds a class to the ul child when requested', () => {
  const component = shallow(<Breadcrumb ulClassName={'my-custom-ul'}/>);
  const ulElement = component.find('ul.my-custom-ul');
  expect(ulElement.exists()).toBeTruthy();
});

it('renders the component with the requested alignment', () => {
  ALIGNMENTS.forEach((alignment) => expectToRenderComponentWithAlignment(Breadcrumb, alignment) );
});

it('adds the requested size by props', () => {
  SIZES.forEach((size) => expectToRenderComponentWithSize(Breadcrumb, size) );
});

it('adds the requested separator by props', () => {
  SEPARATORS.forEach((separator) => expectToRenderComponentWithSeparator(Breadcrumb, separator) );
});