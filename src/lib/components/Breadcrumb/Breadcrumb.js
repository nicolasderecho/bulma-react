import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {alignmentClassFor, ALIGNMENTS, separatorClassFor, SEPARATORS, sizeClassFor, SIZES} from "../../helpers/util";
import BreadcrumbItem from "../BreadcrumbItem/BreadcrumbItem";

const Breadcrumb = ({ className, children, ulClassName, alignment, size, separator, ...props }) => {
  const classes = classNames(className, 'breadcrumb', alignmentClassFor(alignment), sizeClassFor(size), separatorClassFor(separator));
  const ulClasses = classNames(ulClassName);
  return <nav aria-label={'breadcrumbs'} className={classes} {...props}>
    <ul className={ulClasses}>{children}</ul>
  </nav>;
};

Breadcrumb.displayName = 'Breadcrumb';

Breadcrumb.Item = BreadcrumbItem;

Breadcrumb.propTypes = {
  className: PropTypes.string,
  ulClassName: PropTypes.string,
  alignment: PropTypes.oneOf(ALIGNMENTS),
  size: PropTypes.oneOf(SIZES),
  separator: PropTypes.oneOf(SEPARATORS)
};

export default Breadcrumb;