import React from 'react';
import Message from './Message';
import { expectToRenderComponentWithChildren, expectToRenderComponentWithColor, expectToRenderComponentWithElement, expectToRenderComponentWithSize } from "../../../testing/testHelpers";
import {COLOR_TYPES, SIZES} from "../../helpers/util";

it('renders an article with class message', () => {
  expectToRenderComponentWithElement(Message, 'article', 'message');
});

it('renders the Message\'s children', () => {
  expectToRenderComponentWithChildren(Message);
});

it('adds the requested color by props', () => {
  COLOR_TYPES.forEach((color) => expectToRenderComponentWithColor(Message, color) );
});

it('adds the requested size by props', () => {
  SIZES.forEach((size) => expectToRenderComponentWithSize(Message, size) );
});