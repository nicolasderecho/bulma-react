import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {COLOR_TYPES, SIZES, colorClassFor, sizeClassFor } from "../../helpers/util";
import MessageHeader from "../MessageHeader/MessageHeader";
import MessageBody from "../MessageBody/MessageBody";

const Message = ({ className, color, size, ...props }) => {
  const classes = classNames(className, 'message', colorClassFor(color), sizeClassFor(size));
  return <article className={classes} {...props} />;
};

Message.displayName = 'Message';

Message.Header = MessageHeader;
Message.Body   = MessageBody;

Message.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(SIZES),
  color: PropTypes.oneOf(COLOR_TYPES)
};

export default Message;