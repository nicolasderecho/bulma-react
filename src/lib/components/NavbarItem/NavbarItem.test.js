import React from 'react';
import NavbarItem from './NavbarItem';
import {
    expectToRenderComponentWithChildren,
    expectToRenderComponentWithElement, expectToRenderComponentWithProperty
} from "../../../testing/testHelpers";
import {shallow, mount} from "enzyme/build";

it('renders a link with class navbar-item', () => {
    expectToRenderComponentWithElement(NavbarItem, 'a', 'navbar-item');
});

it('renders the NavbarItem\'s children', () => {
    expectToRenderComponentWithChildren(NavbarItem);
});

it('renders a custom component when requested', () => {
    const Component = () => <div className={'component'}>test</div>;
    const component = mount(<NavbarItem component={Component}/>);
    expect(component.find('div.component').exists()).toBeTruthy();
});

it('renders a custom html element when requested', () => {
    const component = shallow(<NavbarItem as={'div'}/>);
    expect(component.find('div.navbar-item').exists()).toBeTruthy();
});

it('adds the dropdown class when requested', () => {
    expectToRenderComponentWithProperty(NavbarItem, 'dropdown', {prefix: 'has'});
});

it('adds the dropdown-up class when requested', () => {
    expectToRenderComponentWithProperty(NavbarItem, 'dropdownUp', {prefix: 'has'});
});

it('adds the expanded class when requested', () => {
    expectToRenderComponentWithProperty(NavbarItem, 'expanded');
});

it('adds the hoverable class when requested', () => {
    expectToRenderComponentWithProperty(NavbarItem, 'hoverable');
});

it('adds the active class when requested', () => {
    expectToRenderComponentWithProperty(NavbarItem, 'active');
});

it('adds the tab class when requested', () => {
    expectToRenderComponentWithProperty(NavbarItem, 'tab');
});