import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {checkProperties, htmlElementFor, propsWithoutKeys} from "../../helpers/util";

const PROPERTIES_TO_CHECK = ['expanded', 'tab', 'active', 'hoverable'];
const DROPDOWN_PROPERTIES = ['dropdown', 'dropdownUp'];
const PROPERTIES_TO_EXCLUDE = PROPERTIES_TO_CHECK.concat(DROPDOWN_PROPERTIES);
const NavbarItem = ({ className, as, component, ...props }) => {
    const Element = component || htmlElementFor(as, 'a');
    const classes = classNames(className, 'navbar-item', checkProperties(props, DROPDOWN_PROPERTIES, {prefix: 'has'}), checkProperties(props, PROPERTIES_TO_CHECK));
    const finalProps = propsWithoutKeys(props, PROPERTIES_TO_EXCLUDE);
    return <Element className={classes} {...finalProps} />;
};

NavbarItem.displayName = 'NavbarItem';

NavbarItem.propTypes = {
    className: PropTypes.string
};

export default NavbarItem;