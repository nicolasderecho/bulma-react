import React from 'react';
import NavbarDivider from './NavbarDivider';
import { expectToRenderComponentWithElement } from "../../../testing/testHelpers";

it('renders a hr with class navbar-divider', () => {
    expectToRenderComponentWithElement(NavbarDivider, 'hr', 'navbar-divider');
});