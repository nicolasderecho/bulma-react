import React from "react";
import ReactDOM from "react-dom";
import 'bulma';
import Home from "./examples/Home";

ReactDOM.render(<Home/>, document.getElementById("root"));